#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype

/**
 * Bitmaps
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.bitmap;

import
    std.math, std.string, std.traits, std.range, std.stdio, std.bitmanip;
import
    cairo.cairo;
import
    imageformats, imageformats.bmp;
import
    iz.memory, iz.streams, iz.properties;
import
    kheops.types, kheops.canvas, kheops.colors;

alias BmpPixel = ubyte[4];

/**
 * Used as template parameter to define the element type of a Bitmap.byLine.
 */
enum BitmapPixelKind
{
    U8U8U8U8,
    U32
}

/**
 * Input range that allows to read and write each line of a Bitmap.
 */
struct BitmapLines
{
    private Bitmap _bmp;
    private void* _line;
    private size_t _index;

    ///
    this(Bitmap bmp)
    {
        _bmp = bmp;
        _line = cast(void*)_bmp._data.memory;
    }

    ///
    bool empty()
    {
        return _line >= (_bmp._data.memory + _bmp._datasz);
    }

    /**
     * Returns a pointer to the first pixel of line.
     */
    uint* front()
    {
        return cast(uint*)_line;
    }

    /**
     * Goes to the next Line.
     */
    void popFront()
    {
        _line += _bmp._size.w * 4;
        ++_index;
    }

    /**
     * Returns the count of pixels by line.
     */
    size_t width() {return _bmp._size.w;}

    /**
     * Returns the index of the current line.
     */
     size_t index() {return _index;}

    /**
     * Sets a particular pixel of the current line.
     */
    void opIndexAssign(T)(auto ref T t, size_t i)
    in
    {
        assert(i < _bmp._size.w);
    }
    do
    {
        auto ptr = _line + i * 4;
        static if (isImplicitlyConvertible!(T,uint))
        {
            *cast(uint*) ptr = t;
        }
        else static if (is(T == Rgba))
        {
            *cast(uint*) ptr = t.color;
        }
        else static if (isArray!T && is(ElementType!T == float))
        {
            static assert(0);
        }
        else static assert(0, "BitmapLines.opIndexAssign, unsupported type: " ~ T.stringof);
    }
}

/**
 *
 */
class Bitmap: PropertyPublisher
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

protected:

    @NoGc cairo_surface_t* _surf;
    MemoryStream _data;
    size_t _datasz;
    SizeI32 _size;
    Event _onChanged;
    ptrdiff_t _changedCount;

    enum doChanged =
    q{
        if (_changedCount > 0)
            return;
        if (_onChanged)
            _onChanged(null);
        _changedCount = 0;
    };

    void updateSize()
    {
        _datasz = _size.w * _size.h * 4;
        _data.size = _datasz;
        if (_surf)
            cairo_surface_destroy(_surf);
        _surf = cairo_image_surface_create_for_data(cast(char*) _data.memory,
            CairoFormat.ARGB32, _size.w, _size.h, _size.w * 4);

        mixin(doChanged);
    }

public:

    /**
     * Internal: used when a brush fillKind == bitmap.
     */
    cairo_surface_t* surface() {return _surf;}

    /**
     * Creates a new empty Bitmap.
     */
    this()
    {
        _data = construct!MemoryStream;
        updateSize;
        collectPublications!Bitmap;
    }

    /**
     * Creates a new Bitmap with a custom size.
     */
    this(int w, int h)
    {
        _data = construct!MemoryStream;
        size(w,h);
        collectPublications!Bitmap;
    }

    ~this()
    {
        destruct(_data);
        if (_surf)
            cairo_surface_destroy(_surf);
        callInheritedDtor;
    }

    /**
     * Copies the data from another Bitmap.
     */
    void copyFrom(Bitmap value)
    {
        size(value.width, value.height);
        value._data.position = 0;
        _data.loadFromStream(value._data);
    }

    /**
     * Sets or gets the bitmap width.
     */
    @Set void width(int value)
    {
        if (_size.w == value)
            return;
        _size.w = value;
        updateSize;
    }

    /// ditto
    @Get int width() {return _size.w;}

    /**
     * Sets or gets the bitmap height.
     */
    @Set void height(int value)
    {
        if (_size.h == value)
            return;
        _size.h = value;
        updateSize;
    }

    /// ditto
    @Get int height() {return _size.h;}

    /**
     * Sets or gets the bitmap data.
     * Only for serialization.
     */
    @Set void data(Stream str)
    {
        _data.loadFromStream(str);
        _data.position = 0;
    }

    /// ditto
    @Get Stream data()
    {
        _data.position = 0;
        return _data;
    }

    /**
     * Sets both width and height.
     */
    void size(int w, int h)
    {
        if (_size.w == w && _size.h == h)
            return;
        _size = SizeI32(w,h);
        updateSize;
    }

    /**
     * Returns a uint* (ARGB) input range that allows to read and write each
     * pixel of a bitmpa, line per line.
     */
    BitmapLines byLine()
    {
        return BitmapLines(this);
    }

    /**
     * Clears the data, preserves the size.
     */
    void clear()
    {
        (cast(ubyte*)_data.memory())[0.._datasz] = 0;
    }

    /**
     * Returns the value of the pixel indexed by line and column.
     */
    auto opIndex(BitmapPixelKind kind = BitmapPixelKind.U32)(size_t line, size_t column)
    {
        auto ptr = _data.memory + (line * _size.w + column) * 4;
        static if (kind = BitmapPixelKind.U32)
            return *cast(uint*) ptr;
        else
            return *cast(BmpPixel*) ptr;
    }

    /**
     * Sets the value of the pixel indexed by line and column.
     */
    void opIndexAssign(T)(auto ref T t, size_t line, size_t column)
    in
    {
        assert(line < _size.h);
        assert(column < _size.w);
    }
    do
    {
        auto ptr = _data.memory + (line * _size.w + column) * 4;
        static if (isImplicitlyConvertible!(T,uint))
        {
            *cast(uint*) ptr = t;
        }
        else static if (is(T == Rgba))
        {
            *cast(uint*) ptr = t.color;
        }
        else static if (isArray!T && is(ElementType!T == float))
        {
            static assert(0);
        }
        else static assert(0, "Bitmap.opIndexAssign, unsupported type: " ~ T.stringof);
    }

    /**
     * Loads the bitmap from a PNG file.
     */
    void loadFromPng(const(char)[] filename)
    {
        auto s = cairo_image_surface_create_from_png(filename.toStringz);
        scope(exit) cairo_surface_destroy(s);

        if (const status = cairo_surface_status(s))
        {
            size(0,0);
            throw new Exception("Invalid PNG file: " ~ cairo_status_to_string(status).fromStringz.idup);
        }

        auto w = cairo_image_surface_get_height(s);
        auto h = cairo_image_surface_get_height(s);
        auto d = cairo_image_surface_get_data(s);

        size(w, h);
        _data.write(cast(void*) d, _datasz);
    }

    /**
     * Saves the bitmap to a PNG file.
     */
    void saveToPng(const(char)[] filename)
    {
        if (const status = cairo_surface_write_to_png(_surf, filename.toStringz))
        {
            throw new Exception("Exception while saving PNG file: " ~
                cairo_status_to_string(status).fromStringz.idup);
        }
    }

    /**
     * Loads the bitmap from a BMP file.
     */
    void loadFromBmp(const(char)[] filename)
    {
        try
        {
            IFImage img = read_bmp(filename, ColFmt.RGBA);
            size(cast(int) img.w, cast(int) img.h);
            writeArray!false(_data, img.pixels);
        }
        catch (Exception e)
        {
            size(0,0);
            throw e;
        }
    }

    /**
     * Saves the bitmap to a BMP file.
     */
    void saveToBmp(const(char)[] filename)
    {
        import std.array: Appender;

        scope ubyte[4] pix;
        scope Appender!(ubyte[]) pixs;

        _data.position = 0;
        pixs.reserve(_size.w * _size.h * 4);

        foreach(immutable i; 0.. _size.w * _size.h)
        {
            _data.read(pix.ptr, 4);
            pixs ~= [pix[2],pix[1],pix[0],pix[3]];
        }
        write_bmp(filename, _size.w, _size.h, pixs.data, ColFmt.RGBA);
    }

    /**
     * Signal fired when the bitmap has changed.
     *
     * It usually indicates that a repaint is needed.
     */
    @Set void onChanged(Event value) {_onChanged = value;}

    /// ditto
    @Get Event onChanged(){return _onChanged;}

    /**
     * Manually triggers and forces the onChanged event.
     */
    void changed() {_changedCount = 0; mixin(doChanged);}

    /**
     * Prevents the onChanged event to be fired while several properties
     * are modified. Must be followed by a call to endChange.
     */
    void beginChange() {++_changedCount;}

    /// ditto
    void endChange()
    {
        --_changedCount;
        mixin(doChanged);
    }
}

/**
 * A Bitmap descendant that allows vectorial drawing.
 */
class DrawableBitmap: Bitmap
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

protected:

    @NoGc cairo_t* _cr;
    Canvas _canvas;

    override void updateSize()
    {
        super.updateSize;
        if (_cr)
            cairo_destroy(_cr);
        _cr = cairo_create(_surf);
        _canvas.setContext(_cr);
    }

public:

    ///
    this()
    {
        super();
    }

    /// Constructs an instance with and defines the dimension.
    this(int w, int h)
    {
        super(w,h);
    }

    ~this()
    {
        if (_cr)
            cairo_destroy(_cr);
        destruct(_canvas);
        callInheritedDtor;
    }

    /**
     * Returns the canvas, ready for Cairo drawings.
     */
    ref Canvas canvas() {return _canvas;}
}

unittest
{
    DrawableBitmap bmp = new DrawableBitmap(8, 8);
    bmp.canvas.rectangle(0,0,8,8);
    bmp.canvas.fillBrush.color = ColorConstant.skyblue;
    bmp.canvas.fill;
    bmp.canvas.stroke!true;
    bmp.saveToPng("kbitmap.png");
    bmp.saveToBmp("kbitmap.bmp");

    DrawableBitmap bmp2 = new DrawableBitmap(0, 0);
    bmp2.copyFrom(bmp);

    bmp2[0,0] = ColorConstant.aquamarine;
    bmp2[1,0] = ColorConstant.aquamarine;
    bmp2[2,0] = ColorConstant.aquamarine;
    bmp2[3,0] = ColorConstant.aquamarine;
    bmp2[4,0] = ColorConstant.aquamarine;
    bmp2[5,0] = ColorConstant.aquamarine;
    bmp2[6,0] = ColorConstant.aquamarine;
    bmp2[7,0] = ColorConstant.aquamarine;

    auto l = bmp2.byLine;
    l.popFront;l.popFront;
    l.popFront;l.popFront;
    foreach(immutable i; 0 .. l.width)
        l[i] = ColorConstant.red;

    bmp2.saveToPng("kbitmap2.png");
    bmp2.saveToBmp("kbitmap2.bmp");

    bmp2.loadFromBmp("kbitmap2.bmp");
    bmp2.saveToBmp("kbitmap3.bmp");
}

unittest
{
    static assert(!MustAddGcRange!Bitmap);
    static assert(!MustAddGcRange!DrawableBitmap);
}
