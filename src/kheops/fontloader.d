#!runnable-flags: -L-lfreetype -L-lcairo

/**
 * Bridge betwwen the operating system (or lib freetype) to load a font
 * as a cairo font.
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.fontloader;

import
    freetype.def, freetype.ft;
import
    cairo.cairo, cairo.ft;

private __gshared cairo_font_face_t*[string] _loadedFonts;
private __gshared cairo_font_face_t* _lastFont;
private __gshared string _lastFontName;

private __gshared string[] _fontList;

static this()
{
    updateFontList;
}

version(linux)
    static immutable string[] defaultFontPaths = ["/usr/share/fonts", "/usr/local/share/fonts"];
else
    static immutable string[] defaultFontPaths = [];

/**
 * Returns the font list.
 */
const(string[]) fontList()
{
    return _fontList;
}

/**
 * Updates the list of the fonts.
 *
 * Params:
 *      paths = The paths were fonts are installed.
 */
void updateFontList(const string[] paths = defaultFontPaths)
{
    import std.file: dirEntries, SpanMode, DirEntry;
    import std.algorithm.sorting: s = sort;
    import std.array: array, Appender;
    import std.file: exists;

    Appender!(string[]) app;
    app.reserve(64 * 64);

    foreach(path; paths)
    {
        if (path.exists)
            foreach(e; dirEntries(path, "*.ttf", SpanMode.depth))
                app ~= e.name;
    }
    _fontList = app.data.s.array.dup;
}

/**
 * Returns: If the font name exists, returns the fully qualified font name,
 * as found in fontList(), otherwise returns an empty string.
 */
string qualifiedFontName(const(char)[] name)
{
    import std.algorithm.searching: find;
    import std.range: empty, front;
    import std.path: baseName, withDefaultExtension;
    import std.array: array;

    const(char)[] n = name.withDefaultExtension("ttf").array;
    auto r = _fontList.find!(a => a.baseName == n);
    if (r.empty)
        return "";
    else
        return r.front();
}

/**
 * Returns a Cairo font matching to a name or a filename.
 */
cairo_font_face_t* getCairoFont(string name)
{
    name = qualifiedFontName(name);

    if (name.length == 0)
        return null;

    if (name == _lastFontName)
        return _lastFont;

    if (auto f = name in _loadedFonts)
    {
        _lastFontName = name;
        _lastFont = *f;
        return *f;
    }
    else
    {
        if (auto f = createFont(name))
        {
            cairo_font_face_reference(f);
            _loadedFonts[name] = f;
            _lastFontName = name;
            _lastFont = f;
            return f;
        }
        else return null;
    }
}

version(linux)
{
    private __gshared FT_Face[] _loadedFt;
    private __gshared FT_Library ftHandle;

    static this()
    {
        if (auto error = FT_Init_FreeType(&ftHandle))
            throw new Error("library freetype2 can't be initialized");
    }

    static ~this()
    {
        if (ftHandle)
        {
            foreach(immutable i; 0 .. _loadedFt.length)
                FT_Done_Face(_loadedFt[i]);
            FT_Done_FreeType(ftHandle);
            //FT_Done_Library(ftHandle);
        }
    }

    private cairo_font_face_t* createFont(string name)
    {
        import std.utf: toUTFz;

        _loadedFt.length += 1;
        FT_New_Face(ftHandle, name.toUTFz!(char*), 0, &_loadedFt[$-1]);
        return cairo_ft_font_face_create_for_ft_face(_loadedFt[$-1], 0);
    }
}

version(Win32)
{
    static assert(0);
}

