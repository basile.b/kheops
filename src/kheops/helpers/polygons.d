/**
 * Helpers to manipulate polygons
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.helpers.polygons;

import
    std.random, std.math;
import
    iz.math;
import
    kheops.types;

/**
 * Returns a random a polygon.
 *
 * Params:
 *      rect = The polygon area.
 *      numPoints = The number of points.
 *      fuzz = The fuzz factor, from 0.0 (straight polygon) to 1.0 (fuzzed).
 */
Points randomizePolygon(const Rect rect, int numPoints, double fuzz = 0.8f)
{
    const double cx = rect.width * 0.5;
    const double cy = rect.height * 0.5;
    const double mw = (rect.width - rect.left) * 0.5;
    const double mh = (rect.height - rect.top) * 0.5;
    const double ai = Pi!2 / numPoints;

    Points points;
    points.length = numPoints;

    foreach(immutable i; 0 .. numPoints)
    {
        const double alpha = ai * i;
        const double c = cos(alpha);
        const double s = sin(alpha);
        if (fuzz != 0.0)
        {
            points[i].x = cx + c * mw * (1.0f - uniform(0, fuzz));
            points[i].y = cy + s * mh * (1.0f - uniform(0, fuzz));
        }
        else
        {
            points[i].x = cx + c * mw;
            points[i].y = cy + s * mh;
        }
    }

    return points;
}

/**
 * Returns a star.
 *
 * Params:
 *      rect = The star area.
 *      numPeaks = The number of peaks.
 *      radius = the radius of the inner circle.
 */
Points star(const Rect rect, int numPeaks, double radius)
{
    const uint ni = numPeaks * 3;
    const double cx = rect.width * 0.5;
    const double cy = rect.height * 0.5;
    const double mw = (rect.width - rect.left) * 0.5;
    const double mh = (rect.height - rect.top) * 0.5;
    const double ai = Pi!2 / ni;

    Points points;
    points.length = ni;

    foreach(immutable i; 0 .. ni)
    {
        double alpha = ai * i;
        if (i % 3 == 1)
        {
            points[i].x = cx + cos(alpha) * mw;
            points[i].y = cy + sin(alpha) * mh;
        }
        else
        {
            points[i].x = cx + cos(alpha) * radius;
            points[i].y = cy + sin(alpha) * radius;
        }
    }

    return points;
}

/**
 * Tests if a point is at the left, on, or to the right of a vector.
 *
 * Params:
 *      p0 = The first point used to define the vector.
 *      p1 = The second point used to define the vector.
 *      p = The point to test.
 * Returns:
 *      A positive value if p is at the left, a negative value if p is at the
 *      right or 0 if p is on the vector.
 *
 * Copyright:
 *      Copyright 2000 softSurfer, 2012 Dan Sunday
 */
pragma(inline, true)
package double isLeft(Point p0, Point p1, Point p) pure @nogc @safe
{
    return (p1.x - p0.x) * (p.y - p0.y) - (p.x - p0.x) * (p1.y - p0.y);
}

/**
 * Point in polygon.
 *
 * Tests if a point is located in a polygon, using the winding number algorithm.
 *
 * Params:
 *      x = The x coordinate of the point to test.
 *      y = The y coordinate of the point to test.
 *
 * Returns:
 *      true if the point is inside the polygon.
 *
 * Copyright:
 *      Copyright 2000 softSurfer, 2012 Dan Sunday
 */
bool pip(Points points, double x, double y) pure @nogc
{
    size_t wn;

    foreach (immutable i; 0..points.length)
    {

        Point* next;
        if (i + 1 != points.length)
            next = points.ptr + i + 1;
        else
            next = points.ptr;

        if (points[i].y <= y)
        {
            if (next.y  > y &&
                isLeft(points[i], *next, Point(x,y)) > 0)
                ++wn;
        }
        else
        {
            if (next.y  <= y &&
                isLeft(points[i], *next, Point(x,y)) < 0)
                --wn;
        }
    }
    return wn != 0;
}

/**
 * Returns: The extents of a polygon
 */
Margin extents(P)(auto ref P points)
in
{
    assert(points.length == 0 ||points.length > 2);
}
do
{
    Margin result;
    result.left = 8000;
    result.right = -8000;
    result.top = 8000;
    result.bottom = -8000;

    foreach (pt; points)
    {
        if (pt.x < result.left)
            result.left = pt.x;
        if (pt.x > result.right)
            result.right = pt.x;
        if (pt.y < result.top)
            result.top = pt.y;
        if (pt.y > result.bottom)
            result.bottom = pt.y;
    }

    return result;
}

/**
 * Aligns a polygon, in place.
 *
 * Params:
 *      pa = The alignment.
 *      polygon = An array of points.
 *      rect = the target rectangle.
 */
void alignTo(P, T : Rect)(ref P polygon, PathAlignment pa, const auto ref T rect)
{
    const Margin ex = extents(polygon);

    const double ew = ex.right - ex.left;
    const double eh = ex.bottom - ex.top;
    const double cx = ew * 0.5 + ex.left;
    const double cy = eh * 0.5 + ex.top;
    const double ox = cx - (rect.width * 0.5 + rect.left);
    const double oy = cy - (rect.height * 0.5 + rect.top);
    const double sx = rect.width / ew;
    const double sy = rect.height / eh;

    foreach(ref pt; polygon)
    {
        with(PathAlignment) final switch (pa)
        {
        case center:
            pt.x -= ox;
            pt.y -= oy;
            break;
        case leftCenter:
            pt.x -= ex.left;
            pt.y -= oy + rect.left;
            break;
        case leftTop:
            pt.x -= ex.left;
            pt.y -= ex.top;
            break;
        case rightCenter:
            pt.x += (rect.left + rect.width) - ex.right;
            pt.y -= oy + rect.left;
            break;
        case centerTop:
            pt.x -= ox + rect.top;
            pt.y -= ex.top;
            break;
        case centerBottom:
            pt.x -= ox + rect.top;
            pt.y += (rect.top + rect.height) - ex.bottom;
            break;
        case fitUnscaled:
            pt.x -= ex.left;
            pt.y -= ex.top;
            pt.x *= sx;
            pt.y *= sy;
            break;
        case none:
        }
    }
}

