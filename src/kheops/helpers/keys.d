/**
 * Helpers to translate a shortcuts to strings.
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.helpers.keys;

import
    std.conv, std.stdio, std.uni;
import
    iz.strings, iz.containers, iz.memory;
import
    kheops.types;


/**
 * Serializable shortcut.
 */
struct Shortcut
{

private:

    Array!char _text;
    KeyModifiers _km;
    dchar _key;

public:

    /**
     * Constructs the shortcut from a representation.
     */
    this(string value)
    {
        _text = value;
        _km = shortcutToKeyModifiers(value);
        _key = shortcutToKey(value).toUpper;
    }

    /**
     * Constructs the shortcut from the key and its modifiers.
     */
    this(K)(dchar key, auto ref K km)
    if (is(typeof({KeyModifiers k = km;})))
    {
        key = key.toUpper;
        _km = km;
        _key = key;
        _text = shortcutToString(key, _km);
    }

    ~this()
    {
        destruct(_text);
    }

    /// TextStruct trait, see iz.rtti.
    void loadFromText(const(char)[] value)
    {
        _km = shortcutToKeyModifiers(value.dup);
        _key = shortcutToKey(value.dup);
        _text = shortcutToString(_key, _km);
    }

    /// TextStruct trait, see iz.rtti.
    const(char)[] saveToText()
    {
        return _text[];
    }

    /// Returns the key modifiers.
    ref const(KeyModifiers) keyModifiers() return {return _km;}

    /// Returns the key.
    dchar key() {return _key;}

    /// Returns the representation.
    alias text = saveToText;
}
///
unittest
{
    Shortcut s0 = Shortcut("CTRL + S");
    assert(s0.key == 'S');
    assert(s0.keyModifiers == [KeyModifier.ctrl]);
    Shortcut s1 = Shortcut('s', KeyModifiers(KeyModifier.alt));
    assert(s1.text == "Alt+S");
    s1.loadFromText("CTRL + S");
    assert(s1.text == "Ctrl+S", s1.text);
    assert(s1.saveToText == "Ctrl+S", s1.text);
    Shortcut s2 = Shortcut('w', KeyModifiers(KeyModifier.ctrl));
    assert(s2.saveToText == "Ctrl+W", s1.text);
}

unittest
{
    import iz.rtti;
    const(Rtti)* sti = getRtti!Shortcut;
    assert(sti.type == RtType._struct);
    assert(sti.structInfo.type == StructType._text);
}

unittest
{
    Shortcut s;
    s = Shortcut('a', [KeyModifier.ctrl]);
    assert(s.saveToText == "Ctrl+A");
}

/**
 * Indicates wether a dchar is a virtual key.
 */
bool isVirtualKey(K)(K key)
{
    import std.traits;
    bool result;
    foreach(m; EnumMembers!VirtualKey)
        if (m == key)
    {
        result = true;
        break;
    }
    return result;
}
///
unittest
{
    assert(isVirtualKey(VirtualKey.VK_0));
    assert(isVirtualKey(VirtualKey.min));
    assert(isVirtualKey(VirtualKey.max));
    assert(!isVirtualKey(VirtualKey.max) + 1);
}

/**
 * Returns the VirtualKey representation of a dchar.
 *
 * Params:
 *      key = the dchar to represent as a VirtualKey.
 * Returns:
 *      An empty string if the input character is not a VirtualKey, otherwise
 *      its member name (without "VK_") in the VirtualKey enum.
 */
string toVirtualKeyString(dchar key)
{
    string result = void;
    VirtualKey vk = cast(VirtualKey) key;
    try result = to!string(vk);
    catch (ConvException ce)
        return "";
    if (result.length < 4)
        return "";
    if (result[0..3] != "VK_")
        return "";
    return result[3..$];
}
///
unittest
{
    dchar k0 = cast(dchar) VirtualKey.VK_F1;
    assert(k0.toVirtualKeyString == "F1");
    dchar k1 = cast(dchar) VirtualKey.VK_ALT_L;
    assert(k1.toVirtualKeyString == "ALT_L");
    assert('B'.toVirtualKeyString == "");
}

/**
 * Makes the string representation of a shortcut.
 *
 * Params:
 *      key = A dchar.
 *      km = key modifiers.
 * Returns:
 *      A string with each element is separated by a plus. The key element is
 *      always at the last position.
 */
string shortcutToString(dchar key, KeyModifiers km)
{
    string result;
    key = key.toUpper;
    with(KeyModifier)
    {
        if ((key & VirtualKey.VK_ALT_L) >= VirtualKey.VK_ALT_L)
            km += KeyModifier.alt;
        if ((key & VirtualKey.VK_ALT_R) >= VirtualKey.VK_ALT_R)
            km += KeyModifier.alt;
        if ((key & VirtualKey.VK_CTRL_L)  >= VirtualKey.VK_CTRL_L)
            km += KeyModifier.ctrl;
        if ((key & VirtualKey.VK_CTRL_R)  >= VirtualKey.VK_CTRL_R)
            km += KeyModifier.ctrl;
        if ((key & VirtualKey.VK_SHIFT_L) >= VirtualKey.VK_SHIFT_L)
            km += KeyModifier.shift;
        if ((key & VirtualKey.VK_SHIFT_R) >= VirtualKey.VK_SHIFT_R)
            km += KeyModifier.shift;

        if (alt in km)
            result ~= "Alt+";
        if (ctrl in km)
            result ~= "Ctrl+";
        if (shift in km)
            result ~= "Shift+";
        if (meta in km)
            result ~= "Meta+";

        result ~= toVirtualKeyString(key);
        if (!result.length || result[$-1] == '+')
            result ~= key;
    }
    return result;
}
///
unittest
{
    with(KeyModifier)
    {
        KeyModifiers km;
        km = [ctrl];
        assert(shortcutToString('a', km) == "Ctrl+A");
        km = [ctrl,shift];
        assert(shortcutToString('V', km) == "Ctrl+Shift+V");
        km = [alt,ctrl,shift];
        assert(shortcutToString('Ü', km) == "Alt+Ctrl+Shift+Ü");
        km = [alt,shift];
        assert(shortcutToString(VirtualKey.VK_F1, km) == "Alt+Shift+F1");
    }
}

/**
 * Returns the key component from a shortcut representation.
 *
 * Params:
 *      shortcut: A string that has to match the format of shortcutToString()
 */
dchar shortcutToKey(string shortcut)
{
    dchar result;
    char[] e;
    auto elems = shortcut.bySeparated('+');
    while(!elems.empty)
    {
        e = elems.front;
        elems.popFront;
    }
    return to!dchar(e).toUpper;
}
///
unittest
{
    assert(shortcutToKey("CTRL+A") == 'A');
    assert(shortcutToKey("Ctrl+a") == 'A');
    assert(shortcutToKey("Ctrl+Ü") == 'Ü');
    assert(shortcutToKey("Ctrl  + Ü") == 'Ü');
    assert(shortcutToKey("Ctrl \t\v + \t\tÜ") == 'Ü');
    assert(shortcutToKey("Ü") == 'Ü');
}

/**
 * Returns the modifiers from a shortcut representation.
 *
 * Params:
 *      shortcut: A string that has to match the format of shortcutToString()
 * Returns:
 *      A KeyModifiers set.
 */
KeyModifiers shortcutToKeyModifiers(string shortcut)
{
    KeyModifiers km;
    foreach(m; shortcut.bySeparated('+')) switch(m)
    {
        default: break;
        case "Alt", "ALT", "alt": km += KeyModifier.alt; break;
        case "Ctrl", "CTRL", "ctrl": km += KeyModifier.ctrl; break;
        case "Shift", "SHIFT", "shift": km += KeyModifier.shift; break;
        case "Meta", "META", "meta": km += KeyModifier.meta; break;
    }
    return km;
}
///
unittest
{
    with(KeyModifier)
    {
        assert(shortcutToKeyModifiers("Ctrl+meta") == [ctrl,meta]);
        assert(shortcutToKeyModifiers("Ctrl+Shift") == [ctrl,shift]);
        assert(shortcutToKeyModifiers("Alt+Shift") == [alt,shift]);
        assert(shortcutToKeyModifiers("Alt+Ctrl") == [alt,ctrl]);
        assert(shortcutToKeyModifiers("sfsf+sdf+ezf") == 0);
    }
}

unittest
{
    import iz.memory;
    static assert(!MustAddGcRange!Shortcut);
}

