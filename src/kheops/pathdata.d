#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype
module kheops.pathdata;

import
    std.stdio;
import
    cairo.cairo;
import
    iz.memory, iz.enumset, iz.containers;
import
    kheops.types;

///
enum PathPointKind: ubyte
{
    move,
    line,
    horzLine,
    vertLine,
    curve,
    close,
}

/// Front type of a PathRange.
struct PathPoint(T)
{
    /// The point kind
    PathPointKind kind;
    /// either 0,1,2 or 3 points, depending on kind.
    PointOf!T[3] points;
}

/// Path point iterator
struct PathPoints
{

private:

    size_t _index;
    size_t _ptx = 1;
    cairo_path_data_t* _front;
    cairo_path_t* _path;

public:

    /// Initializes with a cairo path.
    this(cairo_path_t* pathData) @nogc
    {
        _path = pathData;
        _front = _path.data;
    }

    /// Indicates if the range is consumed.
    bool empty() pure nothrow @safe @nogc
    {
        return _index == _path.num_data;
    }

    /// Advances the range.
    void popFront() pure nothrow @nogc
    {
        if (_ptx >= (_path.data + _index).header.length)
        {
            _index += (_path.data + _index).header.length + 1;
            _front = _path.data + _index;
            _ptx = 1;
        }
        else ++_ptx;
    }

    /// Returns: The type of the point.
    PathPointKind frontKind() @nogc
    {
        final switch(_front.header.type)
        {
        case CairoPathDataType.ClosePath: return PathPointKind.close;
        case CairoPathDataType.CurveTo: return PathPointKind.curve;
        case CairoPathDataType.LineTo: return PathPointKind.line;
        case CairoPathDataType.MoveTo: return PathPointKind.move;
        }
    }

    /// Returns: The index od the point for the current type.
    size_t frontIndex() @nogc
    {
        return _ptx - 1;
    }

    /// Returns: true if the current point is in the path, i.e not a control point.
    bool isFrontInPath() @nogc
    {
        bool result = true;
        if (_front.header.type == CairoPathDataType.CurveTo && _ptx < 3)
            result = false;
        return result;
    }

    /// Returns: the current point.
    Point* front() @nogc
    {
        return cast(typeof(return)) (_front + _ptx);
    }
}

/// Path data iterator
struct PathRange
{

private:

    size_t _index;
    cairo_path_data_t* _front;
    cairo_path_t* _path;

public:

    @disable this(this);

    this(cairo_path_t* pathData) pure nothrow @safe @nogc
    {
        _path = pathData;
        _front = _path.data;
    }

    bool empty() pure nothrow @safe @nogc
    {
        return _index == _path.num_data;
    }

    void popFront() pure nothrow @nogc
    {
        _index += (_path.data + _index).header.length + 1;
        _front = _path.data + _index;
    }

    const(PathPoint!double) front() pure nothrow @nogc
    {
        PathPoint!double result;
        cairo_path_data_t* _point = _front + 1;

        import iz.sugar;

        final switch(_front.header.type)
        {
        case CairoPathDataType.ClosePath:
            result = PathPoint!double(PathPointKind.close,
               bruteCast!(Point[3])(_point.point));
            break;
        case CairoPathDataType.MoveTo:
            result = PathPoint!double(PathPointKind.move,
                bruteCast!(Point[3])(_point.point));
            break;
        case CairoPathDataType.LineTo:
            result = PathPoint!double(PathPointKind.line,
                bruteCast!(Point[3])(_point.point));
            break;
        case CairoPathDataType.CurveTo:
            result = PathPoint!double(PathPointKind.curve,
                bruteCast!(Point[3])(_point.point));
            break;

        }
        return result;
    }
}

/**
 * Encapsulates a cairo path that can be build manually or using the
 * path data from a SVG document.
 */
class PathData
{

private:

    @NoGc Array!char _svgData;
    @NoGc Array!cairo_path_data_t _points;

    @NoGc cairo_path_t _path;
    Event _onChanged;
    bool _updateExtents;
    Margin _extents;

    void doChanged()
    {
        _updateExtents = true;
        if (_onChanged)
            _onChanged(this);
    }

    void updateCairoPath()
    {
        _path.data = cast(cairo_path_data_t*) _points.ptr();
        _path.num_data = cast(int) _points.length();
        _path.status = CairoStatus.Success;
    }

public:

    ~this()
    {
        destruct(_points);
    }

    /// Returns: An iterator for accurate drawing and with non modifiable points.
    PathRange opIndex()
    {
        updateCairoPath;
        return PathRange(&_path);
    }

    /// ditto
    alias range = opIndex;

    /// Returns: An iterator that can be used to modify the points.
    PathPoints pointsIterator()
    {
        updateCairoPath;
        _updateExtents = true;
        return PathPoints(&_path);
    }

    /// Adds a line
    void lineTo(P : Point)(auto ref const(P) value)
    {
        _points.length = _points.length + 2;
        _points[$-2].header.type = CairoPathDataType.LineTo;
        _points[$-2].header.length = 1;
        _points[$-1].point.x = value.x;
        _points[$-1].point.y = value.y;
    }

    /// Moves to a position or creates a new path
    void moveTo(P : Point)(auto ref const(P) value)
    {
        _points.length = _points.length + 2;
        _points[$-2].header.type = CairoPathDataType.MoveTo;
        _points[$-2].header.length = 1;
        _points[$-1].point.x = value.x;
        _points[$-1].point.y = value.y;
    }

    /// Adds a curve.
    void curveTo(P : Point)(auto ref const(P) p0, auto ref const(P) p1,
        auto ref const(P) p2)
    {
        _points.length = _points.length + 4;
        _points[$-4].header.type = CairoPathDataType.CurveTo;
        _points[$-4].header.length = 3;
        _points[$-3].point.x = p0.x;
        _points[$-3].point.y = p0.y;
        _points[$-2].point.x = p1.x;
        _points[$-2].point.y = p1.y;
        _points[$-1].point.x = p2.x;
        _points[$-1].point.y = p2.y;
    }

    /// Closes a path.
    void closePath()
    {
        _points.length = _points.length + 1;
        _points[$-1].header.type = CairoPathDataType.ClosePath;
        _points[$-1].header.length = 0;
        updateCairoPath;
    }

    /// Removes the current paths.
    void clearPath()
    {
        _points.length = 0;
        updateCairoPath;
        doChanged;
    }

    /// Reads again the previous SVG data, discards any possible transformation.
    void reloadSvgData()
    {
        loadFromSvgData(_svgData[]);
    }

    /// Builds the path using the string that contains the SVG path data.
    void loadFromSvgData(const(char)[] value)
    {
        import std.conv: to;
        import std.ascii: isDigit;

        size_t index, ptCount;
        size_t tokStart;
        PathPointKind kind;
        Point[3] points;
        Point currPoint;
        bool absolute;

        void addPoint()
        {
            final switch(kind)
            {
            case PathPointKind.close:
                closePath;
                ptCount = 0;
                break;
            case PathPointKind.curve:
                if (!absolute)
                {
                    points[0] += currPoint;
                    points[1] += currPoint;
                    points[2] += currPoint;
                }
                if (ptCount == 6)
                {
                    curveTo(points[0], points[1], points[2]);
                    ptCount = 0;
                }
                currPoint = points[2];
                break;
            case PathPointKind.horzLine:
                points[0].y = currPoint.y;
                goto case PathPointKind.line;
            case PathPointKind.vertLine:
                points[0].y = points[0].x;
                points[0].x = currPoint.x;
                goto case PathPointKind.line;
            case PathPointKind.line:
                if (!absolute)
                    points[0] += currPoint;
                lineTo(points[0]);
                currPoint = points[0];
                ptCount = 0;
                break;
            case PathPointKind.move:
                if (!absolute)
                    points[0] += currPoint;
                moveTo(points[0]);
                currPoint = points[0];
                // implicit lineTo after move
                kind = PathPointKind.line;
                ptCount = 0;
                break;
            }
        }

        void parseFloat()
        {
            double* v = cast(double*) &points;
            // reminder: go to nth point
            v += ptCount;
            *v = to!double(value[tokStart..index]);
            ++ptCount;
            assert(ptCount <= 6);
        }

        while (index < value.length)
        {
            switch (value[index])
            {
            // Commands
            case 'm':
                absolute = false;
                kind = PathPointKind.move;
                break;
            case 'M':
                absolute = true;
                kind = PathPointKind.move;
                break;
            case 'c':
                absolute = false;
                kind = PathPointKind.curve;
                break;
            case 'C':
                absolute = true;
                kind = PathPointKind.curve;
                break;
            case 'l' :
                absolute = false;
                kind = PathPointKind.line;
                break;
            case 'L':
                absolute = true;
                kind = PathPointKind.line;
                break;
            case 'h' :
                absolute = false;
                kind = PathPointKind.horzLine;
                break;
            case 'H':
                absolute = true;
                kind = PathPointKind.horzLine;
                break;
            case 'v' :
                absolute = false;
                kind = PathPointKind.vertLine;
                break;
            case 'V':
                absolute = true;
                kind = PathPointKind.vertLine;
                break;
            case 'z': case 'Z':
                kind = PathPointKind.close;
                break;
            // Y after X  "X,Y"
            case ',':
                parseFloat;
                tokStart = index + 1;
                break;
            // white after point "X,Y " or "X Y " or "X " ...
            case ' ', '\x0A', '\x0D', '\x09':
                if (value[index-1].isDigit)
                {
                    parseFloat;
                    switch (ptCount)
                    {
                    case 1:
                        if (kind == PathPointKind.horzLine || kind == PathPointKind.vertLine)
                        addPoint;
                        break;
                    case 2:
                        if (kind == PathPointKind.move || kind == PathPointKind.line)
                        addPoint;
                        break;
                    case 6:
                        if (kind == PathPointKind.curve)
                        addPoint;
                        break;
                    default:
                        break;
                    }
                }
                tokStart = index + 1;
                break;
            // point
            case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
            case '.', '-', 'e', 'E':
                break;
            // TODO-cpaths: handle the other SVG commands
            case 'S', 's', 'Q', 'q', 'T', 't', 'A', 'a':
                clearPath;
                throw new Exception("Unhandled SVG command: "
                    ~ value[index .. index + 1].idup);
            default:
                clearPath;
                throw new Exception("Unexpected character while parsing SVG data: "
                    ~ value[index .. index + 1].idup);
            }
            ++index;
        }

        _svgData = value;

        doChanged;
    }

    /// currently not working (see primitives.Path comments)
    cairo_path_t* cairoPath(){return &_path;}

    /// Events triggered when paths are cleared or added.
    ref Event onChanged(){return _onChanged;}

    /// Returns: The string containing the SVG path data.
    ref Array!char svgData(){return _svgData;}

    /// Returns: The path extents.
    Margin extents(bool forceUpdate = false)
    {
        if (_updateExtents || forceUpdate)
        {
            _extents.left = 8000.0;
            _extents.top = 8000.0;
            _extents.right = -8000.0;
            _extents.bottom = -8000.0;

            Point curr;

            void processPoint(T)(auto ref T pt)
            {
                if (pt.x < _extents.left)
                    _extents.left = pt.x;
                if (pt.x > _extents.right)
                    _extents.right = pt.x;

                if (pt.y < _extents.top)
                    _extents.top = pt.y;
                if (pt.y > _extents.bottom)
                    _extents.bottom = pt.y;
            }

            void processCurve(T)(T pts)
            {
                import iz.math: hypot;
                const size_t numSample = 1 + cast(size_t) hypot(pts[2].x - curr.x, pts[2].y - curr.y);
                const double inc = 1.0f / numSample;
                foreach (immutable i; 0..numSample)
                {
                    version(D_InlineAsm_X86_64)
                        processPoint(bezierCubicSSE2(i * inc, curr, pts[0], pts[1], pts[2]));
                    else
                        processPoint(bezierCubic(i * inc, curr, pts[0], pts[1], pts[2]));

                }
                curr = pts[2];
            }

            foreach (ppts; opIndex())
            {
                with (PathPointKind) final switch (ppts.kind)
                {
                case move, line, horzLine, vertLine:
                    processPoint(ppts.points[0]);
                    curr = ppts.points[0];
                    break;
                case curve:
                    processCurve(ppts.points);
                    break;
                case close:
                }
            }
        }
        _updateExtents = false;
        return _extents;
    }

    /// Returns: True if the point given by x and y is in the path.
    bool pointInPath(const double x, const double y)
    {
        Points poly;
        Point curr;

        void addPoint(T)(auto ref T pt)
        {
            poly ~= pt;
        }

        void addCurve(T)(T pts)
        {
            import iz.math: hypot;
            const size_t numSample = 1 + cast(size_t) hypot(pts[2].x - curr.x, pts[2].y - curr.y);
            const double inc = 1.0 / numSample;
            foreach (immutable i; 0..numSample)
            {
                version(D_InlineAsm_X86_64)
                    addPoint(bezierCubicSSE2(i * inc, curr, pts[0], pts[1], pts[2]));
                else
                    addPoint(bezierCubic(i * inc, curr, pts[0], pts[1], pts[2]));
            }
            curr = pts[2];
        }

        foreach (ppts; opIndex())
        {
            with (PathPointKind) final switch (ppts.kind)
            {
            case move, line, horzLine, vertLine:
                addPoint(ppts.points[0]);
                curr = ppts.points[0];
                break;
            case curve:
                addCurve(ppts.points);
                break;
            case close:
            }
        }

        import kheops.helpers.polygons: pip;
        return pip(poly, x, y);
    }

    /// Positions the path in a rectangle.
    void alignTo(T : Rect)(PathAlignment pa, auto ref T rect)
    {
        const Margin ex = extents(true);

        const double ew = ex.right - ex.left;
        const double eh = ex.bottom - ex.top;
        const double cx = ew * 0.5 + ex.left;
        const double cy = eh * 0.5 + ex.top;
        const double ox = cx - (rect.width * 0.5 + rect.left);
        const double oy = cy - (rect.height * 0.5 + rect.top);
        const double sx = rect.width / ew;
        const double sy = rect.height / eh;

        foreach(pt; pointsIterator())
        {
            with(PathAlignment) final switch (pa)
            {
            case center:
                pt.x -= ox;
                pt.y -= oy;
                break;
            case leftCenter:
                pt.x -= ex.left;
                pt.y -= oy + rect.left;
                break;
            case leftTop:
                pt.x -= ex.left;
                pt.y -= ex.top;
                break;
            case rightCenter:
                pt.x += (rect.left + rect.width) - ex.right;
                pt.y -= oy + rect.left;
                break;
            case centerTop:
                pt.x -= ox + rect.top;
                pt.y -= ex.top;
                break;
            case centerBottom:
                pt.x -= ox + rect.top;
                pt.y += (rect.top + rect.height) - ex.bottom;
                break;
            case fitUnscaled:
                pt.x -= ex.left;
                pt.y -= ex.top;
                pt.x *= sx;
                pt.y *= sy;
                break;
            case none:
            }
        }
    }
}

private Point bezierCubic(P = Point)(
    const auto ref double t,
    const auto ref P p1,
    const auto ref P c1,
    const auto ref P c2,
    const auto ref P p2)
{
    Point result;
    auto tm1 = 1-t;
    result.x = (tm1^^3)*p1.x + 3*t*(tm1^^2)*c1.x + 3*(t*t)*tm1*c2.x + t^^3*p2.x;
    result.y = (tm1^^3)*p1.y + 3*t*(tm1^^2)*c1.y + 3*(t*t)*tm1*c2.y + t^^3*p2.y;
    return result;
}

version(D_InlineAsm_X86_64)
private Point bezierCubicSSE2(P = Point)(
    double t,
    const ref P p1,
    const ref P c1,
    const ref P c2,
    const ref P p2)
{
    asm pure nothrow
    {
        naked;

        // t
        movddup XMM3, XMM0;

        // gen 1 to get 1 - t (tm1)
        pcmpeqw XMM4, XMM4;
        psllq   XMM4, 54;
        psrlq   XMM4, 2;
        subpd   XMM4, XMM3;

        // t^^2
        movaps  XMM5, XMM3;
        mulpd   XMM5, XMM3;

        // tm1^^2
        movaps  XMM6, XMM4;
        mulpd   XMM6, XMM4;

        // gen 1.5 to get 3
        pcmpeqw XMM2, XMM2;
        psllq   XMM2, 53;
        psrlq   XMM2, 2;
        addpd   XMM2, XMM2;

        // result = (tm1^^3)* p1
        movlpd XMM0, qword ptr [RCX];
        movhpd XMM0, qword ptr [RCX + 8];
        mulpd  XMM0, XMM4;
        mulpd  XMM0, XMM6;

        // result += 3*t*(tm1^^2)*c1
        movlpd XMM7, qword ptr [RDX];
        movhpd XMM7, qword ptr [RDX + 8];
        mulpd  XMM7, XMM6;
        mulpd  XMM7, XMM3;
        mulpd  XMM7, XMM2;
        addpd  XMM0, XMM7;

        // result += 3*(t^^2)*tm1*c2
        movlpd XMM6, qword ptr [RSI];
        movhpd XMM6, qword ptr [RSI + 8];
        mulpd  XMM6, XMM4;
        mulpd  XMM6, XMM5;
        mulpd  XMM6, XMM2;
        addpd  XMM0, XMM6;

        // result += t^^3*p2
        movlpd XMM4, qword ptr [RDI];
        movhpd XMM4, qword ptr [RDI + 8];
        mulpd  XMM4, XMM3;
        mulpd  XMM4, XMM5;
        addpd  XMM0, XMM4;

        // a point is not fully stored in the 128 bits of a SSE reg...
        movhlps XMM1, XMM0;

        ret;
    }
}

unittest
{
    string path0 = q{M 266,254 L 432.34529,456.37218
        218.19295,504.8595 197.9899,307.87976 z};
    PathData dt = construct!PathData;
    dt.loadFromSvgData(path0);
    scope(exit) destruct(dt);
}

unittest
{
    string path0 = q{M 1.0 2.0 L 2.0 2.0
        C 3.0 3.0 4.0 4.0 5.0 5.0 z};
    PathData dt = construct!PathData;
    dt.loadFromSvgData(path0);
    scope(exit) destruct(dt);

    size_t i;
    PathPoints pts = dt.pointsIterator();
    assert(!pts.empty());
    foreach(pt; pts)
        ++i;
    assert(i == 5);
}

unittest
{
    string path0 = q{M 1.0 2.0 L 2.01 0 z};
    PathData dt = construct!PathData;
    dt.loadFromSvgData(path0);
    scope(exit) destruct(dt);
}

unittest
{
    static assert(!MustAddGcRange!PathData);
}

