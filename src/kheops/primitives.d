#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype

/**
 * Primitves used to compose a control.
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.primitives;

import
    std.math, std.stdio;
import
    cairo.cairo, cairo.ft;
import
    iz.memory, iz.properties, iz.math, iz.containers;
import
    kheops.brush, kheops.types, kheops.control, kheops.canvas, kheops.colors,
    kheops.font, kheops.pen, kheops.bitmap, kheops.pathdata;

static this()
{
    classesRepository.registerFactoryClasses!(Rectangle, RectangleEn, Polygon, Path,
        Ellipse, Circle, Pie, Text);
    classesRepository.registerFactoryClasses!(TextEn, Line, Image);
}


/**
 * Primitive is the base class for a control used to compose the visual
 * of a final control.
 *
 * It implements a caching system that allows to re-draw complex paths
 * with a single call to Cairo. The visual properties (stroke, fill, pen and font)
 * are defined internally with the owned brushes, pen and font.
 * The pen width is used to set automatically the control padding.
 *
 * Primitives by default don't get the user input.
 */
class Primitive: Control
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Brush _stroke;
    Brush _fill;
    Pen _pen;

protected:

    /// Must always be set to true to invalidate the cache after a modification of the size, the shape, etc.
    bool _changed = true;
    @NoGc cairo_path_t* _path;
    bool _vecHitTest;

    void propertyChanged(Object notifier)
    {
        _needRepaint = true;
        _changed = true;
        if (_pen.width > 1.0f)
            paddingAll((_pen.width - 1) * 0.5);
        else
            paddingAll(0.0f);
    }

    /**
     * Tries to restore the cached path.
     *
     * In most of the case this method has not be used directly. It's a sub
     * routine of drawOrRestorePath();
     *
     * Returns: True if the path has been restored and false otherwise.
     * In this case, drawPath must be called.
     */
    final bool tryRestorePath(ref Canvas canvas)
    {
        if (_changed || !_path || !_path.num_data)
            return false;
        canvas.restorePath(_path);
        return true;
    }

    /**
     * Virtual method called by drawOrRestorePath() when the cache is
     * not available or if it's invalidated, for example when the size of
     * the prmitive changes.
     */
    void drawPath(ref Canvas canvas) {}

    /**
     * Puts the path on the canvas, either from the cache or from drawPath().
     */
    final void drawOrRestorePath(ref Canvas canvas)
    {
        if (!tryRestorePath(canvas))
        {
            drawPath(canvas);
            if (_path)
                cairo_path_destroy(_path);
            _path = canvas.savePath;
            _changed = false;
        }
    }

    /**
     * Virtual method called when the property `vectorialHitTest` is set to true.
     *
     * This function should tell wether or not a point is in the shape,
     * rather than in the clip rect.
     */
    bool procVectorialHitTest(double x, double y)
    {
        return super.isPointInside(x, y);
    }

public:

    ///
    this()
    {
        _stroke = construct!Brush;
        _fill   = construct!Brush;
        _pen    = construct!Pen;

        _stroke.color = defaultStrokeColor;
        _fill.color = defaultFillColor;

        _fill.onChanged  = &propertyChanged;
        _stroke.onChanged= &propertyChanged;
        _pen.onChanged   = &propertyChanged;

        wantMouse = false;
        wantKeys = false;
        _focusable = false;

        collectPublications!Primitive;
    }

    ~this()
    {
        destructEach(_stroke, _fill, _pen);
        if (_path)
            cairo_path_destroy(_path);
        callInheritedDtor;
    }

    final override bool isPointInside(double x, double y)
    {
        final switch (_vecHitTest)
        {
            case false: return super.isPointInside(x, y);
            case true: return procVectorialHitTest(x, y);
        }
    }

    /**
     * Sets or gets the brush used to fill the primitive.
     *
     * Its `onChanged` event must not be overwritten.
     */
    @Set void fill(Brush value)
    {
        if (value)
            _fill.copyFrom(value);
    }

    /// ditto
    @Get Brush fill() {return _fill;}

    /**
     * Sets or gets the brush used to stroke the primitive.
     *
     * Its `onChanged` event must not be overwritten.
     */
    @Set void stroke(Brush value)
    {
        if (value)
            _stroke.copyFrom(value);
    }

    /// ditto
    @Get Brush stroke() {return _stroke;}

    /**
     * Sets or gets the brush used to stroke the primitive.
     *
     * Its `onChanged` event must not be overwritten.
     */
    @Set void pen(Pen value)
    {
        if (value)
            _pen.copyFrom(value);
    }

    /// ditto
    @Get Pen pen() {return _pen;}

    @Set override void width(double value)
    {
        super.width(value);
        _changed = true;
    }

    @Get override double width() {return super.width;}

    @Set override void height(double value)
    {
        super.height(value);
        _changed = true;
    }

    @Get override double height() {return super.height;}

    /**
     *  Sets or gets if `isPointInside` is based on the shape.
     */
    @Set void vectorialHitTest(bool value)
    {
        _vecHitTest = value;
    }

    /// ditto
    @Get bool vectorialHitTest() {return _vecHitTest;}
}

/**
 * A primitive that displays a basic rectangle.
 */
class Rectangle: Primitive
{
    mixin inheritedDtor;

protected:

    override void drawPath(ref Canvas canvas)
    {
        canvas.pen = _pen;
        canvas.rectangle(drawRectList[0..$]);
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;
    }
}

/**
 * A primitive that displays an enhanced rectangle.
 *
 * The aspect of the corners can be tweaked.
 */
class RectangleEn: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    double _radius = 0.0f;
    CornerKind _cTopLeft    = CornerKind.round;
    CornerKind _cTopRight   = CornerKind.round;
    CornerKind _cBotLeft    = CornerKind.round;
    CornerKind _cBotRight   = CornerKind.round;

protected:

    override void drawPath(ref Canvas canvas)
    {
        with (CornerKind)
        {
            final switch(_cTopRight)
            {
                case none:
                    canvas.beginPath(drawRect.left, drawRect.top);
                    canvas.line(drawRect.width, drawRect.top);
                    break;
                case line:
                    canvas.beginPath(drawRect.left + _radius, drawRect.top);
                    canvas.line(drawRect.right() - _radius, drawRect.top);
                    canvas.line(drawRect.right(), drawRect.top + _radius);
                    break;
                case round:
                    canvas.arc( drawRect.left + drawRect.width - _radius,
                                drawRect.top + _radius,
                                _radius, -Pi!(1,2), 0.0f);
            }
            final switch(_cBotRight)
            {
                case none:
                    canvas.line(drawRect.width, drawRect.height);
                    break;
                case line:
                    canvas.line(drawRect.right(), drawRect.bottom() - _radius);
                    canvas.line(drawRect.right() - _radius, drawRect.bottom());
                    break;
                case round:
                    canvas.arc( drawRect.left + drawRect.width - _radius,
                                drawRect.top + drawRect.height - _radius,
                                _radius, 0.0f, Pi!(1,2));
            }
            final switch(_cBotLeft)
            {
                case none:
                    canvas.line(drawRect.left, drawRect.height);
                    break;
                case line:
                    canvas.line(drawRect.left + _radius, drawRect.bottom());
                    canvas.line(drawRect.left, drawRect.bottom() - _radius);
                    break;
                case round:
                    canvas.arc( drawRect.left + _radius,
                                drawRect.bottom() - _radius,
                                _radius, Pi!(1,2), Pi!1);
            }
            final switch(_cTopLeft)
            {
                case none:
                    canvas.line(drawRect.left, drawRect.top);
                    break;
                case line:
                    canvas.line(drawRect.left, _radius + drawRect.top);
                    canvas.line(drawRect.left + _radius, drawRect.top);
                    break;
                case round:
                    canvas.arc( drawRect.left + _radius, drawRect.top + _radius,
                                _radius, Pi!1, Pi!(3,2));
            }
        }
        canvas.closePath;
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;
    }

public:

    ///
    this()
    {
        collectPublications!RectangleEn;
    }

    /**
     * Sets the type of all the corners.
     */
    void cornerAll(CornerKind value)
    {
        cornerBottomLeft = value;
        cornerBottomRight = value;
        cornerTopLeft = value;
        cornerTopRight = value;
    }

    /**
     * Sets or gets the top left corner type.
     */
     @Set void cornerTopLeft(CornerKind value)
     {
        if (_cTopLeft == value)
            return;
        _cTopLeft = value;
        _changed = true;
        _needRepaint = true;
     }

     /// ditto
     @Get CornerKind cornerTopLeft() {return _cTopLeft;}

    /**
     * Sets or gets the top right corner type.
     */
     @Set void cornerTopRight(CornerKind value)
     {
        if (_cTopRight == value)
            return;
        _cTopRight = value;
        _changed = true;
        _needRepaint = true;
     }

     /// ditto
     @Get CornerKind cornerTopRight() {return _cTopRight;}

    /**
     * Sets or gets the bottom left corner type.
     */
     @Set void cornerBottomLeft(CornerKind value)
     {
        if (_cBotLeft == value)
            return;
        _cBotLeft = value;
        _changed = true;
        _needRepaint = true;
     }

     /// ditto
     @Get CornerKind cornerBottomLeft() {return _cBotLeft;}

    /**
     * Sets or gets the bottom right corner type.
     */
     @Set void cornerBottomRight(CornerKind value)
     {
        if (_cBotRight == value)
            return;
        _cBotRight = value;
        _changed = true;
        _needRepaint = true;
     }

     /// ditto
     @Get CornerKind cornerBottomRight() {return _cBotRight;}

    /**
     * Sets or gets the corners radius.
     */
    @Set void radius(double value)
    {
        if (value == _radius)
            return;
        _radius = value;
        _changed = true;
        _needRepaint = true;
    }

    /// ditto
    @Get double radius() {return _radius;}

    @Set override void width(double value)
    {
        super.width(value);
        _changed = true;
    }

    @Get override double width() {return super.width;}


    @Set override void height(double value)
    {
        super.height(value);
        _changed = true;
    }

    @Get override double height() {return super.height;}
}

/**
 * A primitive that displays a polygon.
 */
class Polygon: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Array!(Point) _pts;
    PathAlignment _pathAlignment;

protected:

    override void propertyChanged(Object notifier)
    {
        super.propertyChanged(notifier);
        import kheops.helpers.polygons: alignTo;
        _pts.alignTo(_pathAlignment, drawRect);
    }

    override bool procVectorialHitTest(double x, double y)
    {
        import kheops.helpers.polygons: pip;
        return pip(_pts[], x - left, y - top);
    }

    override void drawPath(ref Canvas canvas)
    {
        canvas.polygon(_pts[]);
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;

        version (all)
        {
            canvas.rectangle(sizeRectList[0..$]);
            canvas.strokeBrush.color = ColorConstant.red;
            canvas.stroke!true;
        }
    }

public:

    ///
    this()
    {
        collectPublications!Polygon;
    }

    ~this()
    {
        destruct(_pts);
        callInheritedDtor;
    }

    /**
     * Sets or gets the polygon points.
     */
    @Set void points(Points value)
    {
        if (_pts == value)
            return;
        _pts = value;
        _changed = true;
        _needRepaint = true;
        propertyChanged(null);
    }

    /// ditto
    @Get Points points() {return _pts[];}

    /**
     * Sets or gets the polygon positioning in the primitive.
     */
    @Set void polygonPosition(PathAlignment value)
    {
        if (_pathAlignment == value)
            return;
        _pathAlignment = value;
        propertyChanged(null);
    }

    /// ditto
    @Get PathAlignment polygonPosition() {return _pathAlignment;}
}

/**
 * A primitive that displays an arbitrary vectorial path.
 */
class Path: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    @NoGc PathData _data;
    PathAlignment _pathAlignment;

protected:

    override void propertyChanged(Object notifier)
    {
        super.propertyChanged(notifier);
        _data.alignTo(_pathAlignment, drawRect);
    }

    override bool procVectorialHitTest(double x, double y)
    {
        return _data.pointInPath(x - parentedRect.left, y - parentedRect.top);
    }

public:
    ///
    this()
    {
        _data = construct!PathData;
        collectPublications!Path;
        _data.onChanged = &propertyChanged;
    }

    ~this()
    {
        destruct(_data);
        callInheritedDtor;
    }

    override void drawPath(ref Canvas canvas)
    {
        canvas.drawPath(_data);
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);

        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;

        version (all)
        {
            canvas.rectangle(sizeRectList[0..$]);
            canvas.strokeBrush.color = ColorConstant.red;
            canvas.stroke!true;
        }
    }

    //TODO-cbugfix: problems when the path that's build manually is used
    /*override void paint(ref Canvas canvas)
    {
        _path = _data.cairoPath;
        if (!_path || _path.num_data == 0)
            return;

        canvas.restorePath(_path);
        canvas.closePath;

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;
    }*/

    ///
    PathData data(){return _data;}

    /**
     * Sets or gets the SVG path data.
     *
     * The string passed must be conform with https://www.w3.org/TR/SVG/paths.html#PathData.
     */
    @Set void svgData(char[] value)
    {
        _data.loadFromSvgData(value);
    }

    /// ditto
    @Get char[] svgData() {return _data.svgData[];}

    /**
     * Sets or gets the path positioning in the primitive.
     */
    @Set void pathPosition(PathAlignment value)
    {
        if (_pathAlignment == value)
            return;
        _pathAlignment = value;
        propertyChanged(_data);
    }

    /// ditto
    @Get PathAlignment pathPosition() {return _pathAlignment;}
}

package class CustomCircle: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    double _radius;
    double _cx, _cy;
    EllipseKind _kind;

    void update()
    {
        _changed = true;
        _needRepaint = true;

        _cx = drawRect.left + drawRect.width * 0.5;
        _cy = drawRect.top + drawRect.height * 0.5;

        if (_kind == EllipseKind.scaled)
        {
            _radius = (drawRect.width > drawRect.height) ?
                drawRect.height * 0.5 : drawRect.width * 0.5;
        }
    }

protected:

    override bool procVectorialHitTest(double x, double y)
    {
        x -= left + _cx;
        y -= top + _cy;
        return x*x + y*y <= _radius * _radius;
    }

    override void drawPath(ref Canvas canvas)
    {
        canvas.circle(_cx, _cy, _radius);
    }

    override void propertyChanged(Object notifier)
    {
        super.propertyChanged(notifier);
        update();
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;
    }

public:

    ///
    this()
    {
        collectPublications!CustomCircle;
    }

    /**
     * Sets or gets the ellipse kind.
     */
    @Set void kind(EllipseKind value)
    {
        if (_kind == value)
            return;
        _kind = value;
        update;
    }

    /// ditto
    @Get EllipseKind kind() {return _kind;}

    /**
     * Sets or gets the x radius, applied when kind is not scaled.
     */
    @Set void radius(double value)
    {
        if (_radius == value)
            return;
        _radius = value;
        update;
    }

    /// ditto
    @Get double radius() {return _radius;}


    @Set override void width(double value)
    {
        super.width(value);
        update;
    }

    @Get override double width() {return super.width;}


    @Set override void height(double value)
    {
        super.height(value);
        update;
    }

    @Get override double height() {return super.height;}
}

/**
 * A primitive that displays a circle, either scaled or with a fixed size.
 */
class Circle: CustomCircle
{
    mixin inheritedDtor;
}

/**
 * A primitive that displays an ellispse, either scaled or with a fixed size.
 */
class Ellipse: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    double _radius;
    double _cx, _cy;
    double _wr, _hr;
    double _rx, _ry;
    EllipseKind _kind;

    void update()
    {
        _changed = true;
        _needRepaint = true;
        _cx = drawRect.left + drawRect.width * 0.5;
        _cy = drawRect.top + drawRect.height * 0.5;

        with (EllipseKind) final switch (_kind)
        {
            case scaled:
                if (drawRect.width > drawRect.height)
                {
                    _radius = drawRect.height * 0.5;
                    if (drawRect.height != 0)
                        _wr = drawRect.width / drawRect.height;
                    _hr = 1.0f;
                }
                else
                {
                    _radius = drawRect.width * 0.5;
                    _wr = 1.0f;
                    if (drawRect.width != 0)
                        _hr = drawRect.height / drawRect.width;

                }
                break;
            case strict:
                _radius = _rx;
                _wr = 1.0f;
                _hr = _ry / _rx;
        }
    }

protected:

    override void propertyChanged(Object notifier)
    {
        super.propertyChanged(notifier);
        update();
    }

    override void drawPath(ref Canvas canvas)
    {
        canvas.ellipse(_cx, _cy, _radius, _wr, _hr);
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;
    }

public:

    ///
    this()
    {
        collectPublications!Ellipse;
    }

    /**
     * Sets or gets the ellipse kind.
     */
    @Set void kind(EllipseKind value)
    {
        if (_kind == value)
            return;
        _kind = value;
        update;
    }

    /// ditto
    @Get EllipseKind kind() {return _kind;}

    /**
     * Sets or gets the x radius, applied when kind is not scaled.
     */
    @Set void radiusX(double value)
    {
        if (_rx == value)
            return;
        _rx = value;
        if (_rx == 0.0f)
            _rx = 0.0001;
        update;
    }

    /// ditto
    @Get double radiusX() {return _rx;}

    /**
     * Sets or gets the y radius, applied when kind is not scaled.
     */
    @Set void radiusY(double value)
    {
        if (_ry == value)
            return;
        _ry = value;
        update;
    }

    /// ditto
    @Get double radiusY() {return _ry;}


    @Set override void width(double value)
    {
        super.width(value);
        update;
    }

    @Get override double width() {return super.width;}


    @Set override void height(double value)
    {
        super.height(value);
        update;
    }

    @Get override double height() {return super.height;}
}

/**
 * A primitive that displays a pie, either scaled or with a fixed size.
 */
class Pie: CustomCircle
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    double _aBeg, _aEnd;

protected:

    override void drawPath(ref Canvas canvas)
    {
        canvas.pie(_cx, _cy, _radius, (Pi!2 / 360) * _aBeg, (Pi!2 / 360) * _aEnd);
    }

    override bool procVectorialHitTest(double x, double y)
    {
        import std.math: atan2;
        auto angle = atan2(y-top-_cy,x-left-_cx);
        if (angle < 0) angle = angle + Pi!2 ;
        angle *= 360 / Pi!2;
        return (angle >= _aBeg) & (angle <= _aEnd) & super.procVectorialHitTest(x,y);
    }

public:

    ///
    this()
    {
        collectPublications!Pie;
    }

    /**
     * Sets or gets the angle where the pie begins.
     */
    @Set void angleBegin(double value)
    {
        if (_aBeg == value)
            return;
        _aBeg = value.wrap(360);
        update;
    }

    /// ditto
    @Get double angleBegin() {return _aBeg;}

    /**
     * Sets or gets the angle where the pie ends.
     */
    @Set void angleEnd(double value)
    {
        if (_aEnd == value)
            return;
        _aEnd = value.wrap(360);
        update;
    }

    /// ditto
    @Get double angleEnd() {return _aEnd;}
}

/**
 * A primitive that displays a line
 */
class Line: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    LineKind _kind;

protected:

    override void drawPath(ref Canvas canvas)
    {
        with(LineKind) final switch(_kind)
        {
            case topToTop:
                canvas.beginPath(0.0f, 0.0f);
                canvas.line(sizeRect.width, 0.0f);
                break;
            case bottomToBottom:
                canvas.beginPath(0.0f, sizeRect.height);
                canvas.line(sizeRect.width, sizeRect.height);
                break;
            case leftToLeft:
                canvas.beginPath(0.0f, 0.0f);
                canvas.line(0.0f, sizeRect.height);
                break;
            case righToRight:
                canvas.beginPath(sizeRect.width, 0.0f);
                canvas.line(sizeRect.width, sizeRect.height);
                break;
            case topLeftToBottomRight:
                canvas.beginPath(0.0f, 0.0f);
                canvas.line(sizeRect.width, sizeRect.height);
                break;
            case BottomLeftToTopRight:
                canvas.beginPath(0.0f, sizeRect.height);
                canvas.line(sizeRect.width, 0.0f);
                break;
        }
        canvas.closePath;
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.strokeBrush = _stroke;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultPen;
    }

public:

    ///
    this()
    {
        collectPublications!Line;
    }

    /**
     * Sets or gets the line kind.
     */
    @Set void kind(LineKind value)
    {
        if (_kind == value)
            return;
        _kind = value;
        _changed = true;
        _needRepaint = true;
    }

    /// ditto
    @Get LineKind kind() {return _kind;}
}

package class CustomText: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:


    Font _font;
    Justify _justify;

protected:

    TextExtent _textSize;

    override void propertyChanged(Object)
    {
        _changed = true;
        _needRepaint = true;

        /*textInfo.setFont(_font);
        textInfo.setPen(_pen);
        auto sz = textInfo(caption);

        _y = (sizeRect.height - _font.size) * 0.5 + _font.size;

        final switch(_justify)
        {
            case Justify.left:
                _x = 0;
                break;
            case Justify.center:
                _x = (sizeRect.width - sz.width) * 0.5;
                break;
            case Justify.right:
                _x = sizeRect.width - sz.width;
                break;
        }*/
    }

public:

    ///
    this()
    {
        _font = construct!Font;
        _font.onChanged = &propertyChanged;
        collectPublications!CustomText;
    }

    ~this()
    {
        destruct(_font);
        callInheritedDtor;
    }

    /**
     * Sets or gets the font used to draw the text.
     *
     * Its `onChanged` event must not be overwritten.
     */
    @Set void font(Font value)
    {
        if (value)
            _font.copyFrom(value);
    }

    @Get Font font() {return _font;}

    /**
     * Sets or gets the text justification.
     */
    @Set void justify(Justify value)
    {
        if (_justify == value)
            return;
        _justify = value;
        _changed = true;
    }

    /// ditto
    @Get justify() {return _justify;}

    @Set override void width(double value)
    {
        super.width(value);
        _changed = true;
    }

    @Get override double width() {return super.width;}

    @Set override void height(double value)
    {
        super.height(value);
        _changed = true;
    }

    @Get override double height() {return super.height;}

    /**
     * Sets or gets the text.
     */
    @Set override void caption(string value)
    {
        super.caption(value);
        _changed = true;
        _needRepaint = true;
    }

    /// ditto
    @Get override string caption() {return super.caption;}

    /// Returns the text size
    final ref const(TextExtent) textSize(){return _textSize;}

    /// Returns the text width
    final double textWidth() {return _textSize.width;}

    /// Returns the text height
    final double textheight() {return _textSize.height;}
}

/**
 * A primitive that displays a simple text, without fill.
 * Designed for static texts such as controls captions or labels.
 *
 * This primitive doesn't cache a vectorial path but rather uses the
 * font API that draws glyphs.
 */
class Text: CustomText
{

    mixin inheritedDtor;

public:

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        canvas.font = _font;
        canvas.pen = _pen;

        const _textSize = canvas.textSize(caption);

        double x = void;
        const double y = sizeRect.height - (sizeRect.height - _textSize.height) * 0.5;

        final switch(_justify)
        {
            case Justify.left:
                x = 0;
                break;
            case Justify.center:
                x = (sizeRect.width - _textSize.width) * 0.5;
                break;
            case Justify.right:
                x = sizeRect.width - _textSize.width;
                break;
        }


        canvas.text(x, y, caption);
        canvas.restoreDefaultFont;

        canvas.strokeBrush = _stroke;
        canvas.stroke!true;
        canvas.restoreDefaultPen;
        canvas.restoreDefaultStrokeBrush;
    }
}

/**
 * A primitive that displays a text with a fill and a stroke.
 *
 * Contrary to Text, the string displayed is a real (and slower to render)
 * vectorial path.
 */
class TextEn: CustomText
{
    mixin inheritedDtor;

protected:

    override void drawPath(ref Canvas canvas)
    {
        canvas.font = _font;
        _textSize = canvas.textSize(caption);
        double x = void;
        const double y = sizeRect.height - (sizeRect.height - _textSize.height) * 0.5;
        final switch(_justify)
        {
            case Justify.left:
                x = 0;
                break;
            case Justify.center:
                x = (sizeRect.width - _textSize.width) * 0.5;
                break;
            case Justify.right:
                x = sizeRect.width - _textSize.width;
                break;
        }
        canvas.drawText(x, y, caption);
        canvas.restoreDefaultFont;
    }

public:

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultPen;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultStrokeBrush;
    }
}

class Image: Primitive
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Stretch _stretch;

protected:

    @SetGet Bitmap _bmp;

    override void drawPath(ref Canvas canvas)
    {
        canvas.rectangle(sizeRectList[0..$]);
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        drawOrRestorePath(canvas);

        canvas.pen = _pen;
        canvas.fillBrush = _fill;
        canvas.strokeBrush = _stroke;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultStrokeBrush;
        canvas.restoreDefaultFillBrush;
        canvas.restoreDefaultPen;

        canvas.drawImage(sizeRect, _bmp, _stretch);
    }

public:

    ///
    this()
    {
        _bmp = construct!Bitmap;
        _bmp.onChanged = &propertyChanged;
        collectPublications!Image;
    }

    ~this()
    {
        destruct(_bmp);
        callInheritedDtor;
    }

    Bitmap bitmap() {return _bmp;}

    @Set void stretch(Stretch value)
    {
        _stretch = value;
        _needRepaint = true;
    }

    @Get Stretch stretch() {return _stretch;}

    @Set override void width(double value)
    {
        super.width(value);
        _changed = true;
    }

    @Get override double width() {return super.width;}


    @Set override void height(double value)
    {
        super.height(value);
        _changed = true;
    }

    @Get override double height() {return super.height;}
}

unittest
{
    static assert(!MustAddGcRange!Circle);
    static assert(!MustAddGcRange!CustomCircle);
    static assert(!MustAddGcRange!CustomText);
    static assert(!MustAddGcRange!Ellipse);
    static assert(!MustAddGcRange!Image);
    static assert(!MustAddGcRange!Line);
    static assert(!MustAddGcRange!Path);
    static assert(!MustAddGcRange!Pie);
    static assert(!MustAddGcRange!Polygon);
    static assert(!MustAddGcRange!Primitive);
    static assert(!MustAddGcRange!Rectangle);
    static assert(!MustAddGcRange!RectangleEn);
    static assert(!MustAddGcRange!Text);
    static assert(!MustAddGcRange!TextEn);
}
