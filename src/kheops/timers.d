/**
 * Timers
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.timers;

import
    core.time, core.thread, std.datetime, std.stdio;
import
    iz.properties, iz.memory, iz.containers;
import
    kheops.types;
version(Posix) import
    core.sys.posix.time, core.sys.posix.signal;

/**
 * CustomTimer defines the interface of a timer.
 * Not to be used directly.
 */
class CustomTimer: PropertyPublisher
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

protected:

    __gshared Event _onTimer;
    __gshared uint _interval;

public:

    ///
    this()
    {
        collectPublications!CustomTimer;
    }

    /**
     * Starts the timer.
     */
    void start() {}

    /**
     * Stops the timer.
     */
    void stop() {}

    /**
     * Indicates if the timer is started.
     */
    bool started() {return false;}

    /**
     * Sets or gets the interval, in milliseconds, between two ticks.
     */
    @Set void interval(uint value)
    {
        _interval = (value > 5) ? value : 5;
        _interval = (_interval < 20000) ? _interval : 20000;
    }

    /// ditto.
    @Get uint interval() {return _interval;}

    /**
     * Sets or gets the event called for each tick.
     */
    @Set void onTimer(Event value) {_onTimer = value; }

    /// ditto.
    @Get Event onTimer() {return _onTimer;}
}

/**
 * ThreadTimer is a simple timer that uses a thread and does not use
 * the operating system API.
 *
 * Not reliable in stressed environments.
 */
class ThreadTimer: CustomTimer
{
    mixin inheritedDtor;

private:

    __gshared bool _stop;
    __gshared ulong _t1, _t2;
    __gshared Thread _thread;

    final void execute()
    {
        while (true)
        {
            if (!_t1) _t1 = (MonoTime.currTime - MonoTime.zero).total!"msecs";
            Thread.sleep(dur!"msecs"(5));
            _t2 = (MonoTime.currTime - MonoTime.zero).total!"msecs";
            if (_t2 - _t1 > _interval)
            {
                _t1 = 0;
                if (_onTimer) _onTimer(this);
            }
            if (_stop) break;
        }
    }

public:

    ~this()
    {
        stop();
        if (_thread)
            destruct(_thread);
        callInheritedDtor;
    }

    final override void start()
    {
        stop();
        _t1 = 0;
        _stop = false;
        if (_thread) destruct(_thread);
        _thread = construct!Thread(&execute);
        _thread.start;
    }

    final override void stop()
    {
        if (_thread)
        {
            _stop = true;
            _thread.join;
        }
    }

    final override bool started()
    {
        return _thread !is null && _thread.isRunning;
    }
}

/**
 * IdleTimer is a timer that only processes when the OsWindow event loop is idle.
 */
class IdleTimer: CustomTimer
{
    mixin inheritedDtor;

private:

    bool _listen;
    size_t _ix;
    ulong _t1;

    __gshared static Array!IdleTimer _instances;
    __gshared static ulong _tick;

    shared static ~this()
    {
        foreach(IdleTimer it; _instances)
            destruct(it);
        destruct(_instances);
    }

package:

    static idleTick(ulong tick)
    {
        foreach(IdleTimer it; _instances)
        {
            if (!it._listen)
                return;
            if (tick - it._t1 >= it.interval)
            {
                it._t1 = tick;
                if (it._onTimer)
                    it._onTimer(it);
            }
        }
        Thread.sleep(dur!"msecs"(1));
    }

public:

    /**
     * Default constructor.
     *
     * IdleTimer instances that remains on program termination are
     * automatically destructed.
     */
    this()
    {
        _instances ~= this;
        _ix = _instances.length - 1;
    }

    ~this()
    {
        import std.algorithm.mutation: remove;
        import std.algorithm.searching: countUntil;
        _instances = _instances[0.._ix] ~ _instances[_ix+1..$];
        callInheritedDtor;
    }

    final override void start()
    {
        _listen = true;
        _t1 = _tick;
    }

    final override void stop()
    {
        _listen = false;
    }

    final override bool started() {return _listen;}
}

unittest
{
    static assert(!MustAddGcRange!CustomTimer);
    static assert(!MustAddGcRange!IdleTimer);
    static assert(!MustAddGcRange!ThreadTimer);
}

