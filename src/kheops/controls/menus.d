/**
 * Menus
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.controls.menus;

import
    std.stdio;
import
    iz.memory, iz.properties;
import
    kheops.control, kheops.types, kheops.primitives, kheops.colors;

static this()
{
    registerFactoryClasses!(MenuWindow)(classesRepository);
}

class MenuWindow: CustomMenuWindow
{

    mixin PropertyPublisherImpl;

private:

    RectangleEn _background;

protected:

public:

    this()
    {
        _background = addControl!RectangleEn(Rect(0,0,1,1), Alignment.client);
        _background.fill.color = ColorConstant.beige;
        _background.siblingIndex = 0;

        collectPublications!MenuWindow;
    }

}
