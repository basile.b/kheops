#!for-unittests: -L-lX11 -L-lcairo -L-lfreetype

/**
 * Brush
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.brush;

import
    std.math, std.stdio;
import
    iz.types, iz.memory, iz.properties, iz.streams;
import
    kheops.colors, kheops.types, kheops.gradient, kheops.bitmap;


/**
 *
 */
class Brush: PropertyPublisher
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    ptrdiff_t _changedCount;
    Rgba _rgba;
    FillKind _fillkind = FillKind.uniform;
    Gradient _gradient;
    GradientKind _gradientKind;
    Event _onChanged;
    @SetGet Bitmap _bitmap;
    FillAdjustment _fillAdjust;

    enum doChanged =
    q{
        if (_changedCount > 0)
            return;
        if (_onChanged)
            _onChanged(this);
        _changedCount = 0;
    };

    void internalStuffChanged(Object notifier)
    {
        mixin(doChanged);
    }

public:

    ///
    this()
    {
        _bitmap = construct!Bitmap(0,0);
        _gradient = construct!Gradient;
        _gradient.onChanged = &internalStuffChanged;
        collectPublications!Brush;
    }

    ~this()
    {
        destructEach(_bitmap, _rgba, _gradient);
        callInheritedDtor;
    }

    /**
     * Copies the data from another brush.
     */
    void copyFrom(Brush value)
    {
        beginChange;
        _rgba = value.rgba;
        _fillkind = value._fillkind;
        _gradient.copyFrom(value._gradient);
        _bitmap.copyFrom(value._bitmap);
        endChange;
    }

    /**
     * Sets or gets the brush fill kind.
     */
    @Set void fillKind(FillKind value)
    {
        if (_fillkind == value) return;
        _fillkind = value;
        mixin(doChanged);
    }

    /// ditto
    @Get FillKind fillKind() {return _fillkind;}

    /**
     * Sets or gets the color applied when fillKind is uniform.
     *
     * If the value returned by the getter is modified then changed()
     * should also be called.
     */
    void rgba(ref Rgba rgba)
    {
        if (_rgba == rgba)
            return;
        _rgba.color = rgba.color;
        mixin(doChanged);
    }

    /// ditto
    ref Rgba rgba() {return _rgba;}

    /**
     * Sets or gets the brush color when fillKind is uniform.
     */
    @Set void color(uint value)
    {
        if (_rgba == value) return;
        _rgba.color = value;
        if (_fillkind == FillKind.uniform)
            mixin(doChanged);
    }

    /// ditto
    @Get uint color() {return _rgba.color;}

    /**
     * Sets or gets the gradient kind when the fillKind is a gradient.
     */
    @Set void gradientKind(GradientKind value)
    {
        if (_gradientKind == value) return;
        _gradientKind = value;
        if (fillKind == FillKind.gradient)
            mixin(doChanged);
    }

    /// ditto
    @Get GradientKind gradientKind() {return _gradientKind;}

    /**
     * Sets or gets the adjustment of the fill when fillKind is a gradient
     * or a bitmap.
     */
    @Set void fillAdjustment(FillAdjustment value)
    {
        if (_fillAdjust == value)
            return;
        _fillAdjust = value;
        if (fillKind == FillKind.gradient || fillKind == FillKind.bitmap)
            mixin(doChanged);
    }

    /// ditto
    @Get FillAdjustment fillAdjustment() {return _fillAdjust;}

    /**
     * Returns the bitmpa used to fill when fillKind is equal
     * to FillKind.bitmap.
     */
    Bitmap bitmap() {return _bitmap;}

    /**
     * Gives a pointer to the gradient Data.
     *
     * Stops can be modified from here.
     */
    @Set void gradient(Gradient value) {_gradient = value;}

    /// ditto
    @Get Gradient gradient() {return _gradient;}

    /**
     * Signal fired when the brush has changed.
     *
     * It usually indicates that a repaint is needed.
     */
    @Set void onChanged(Event value) {_onChanged = value;}

    /// ditto
    @Get Event onChanged(){return _onChanged;}

    /**
     * Manually triggers and forces the onChanged event.
     */
    void changed() {_changedCount = 0; mixin(doChanged);}

    /**
     * Prevents the onChanged event to be fired while several properties
     * are modified. Must be followed by a call to endChange.
     */
    void beginChange() {++_changedCount;}

    /// ditto
    void endChange()
    {
        --_changedCount;
        mixin(doChanged);
    }
}

unittest
{
    static assert(!MustAddGcRange!Brush);
}

