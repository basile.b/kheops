#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype
/**
 * This module contains a widgetset that uses a style system based on
 * the visitor pattern.
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.udcontrols;

import
    std.conv, std.utf, std.ascii, std.stdio;
import
    iz.memory, iz.properties, iz.containers;
import
    kheops.types, kheops.bitmap, kheops.brush, kheops.canvas, kheops.colors,
    kheops.font, kheops.gradient, kheops.pen, kheops.control, kheops.pathdata,
    kheops.helpers.keys;


/**
 * The base of an UniformlyDrawable control handles the relationship with
 * the UniformlyDrawableVisitor that draws the control.
 * The states a control can have are packed in a ControlStates, which the
 * visitor uses to determine how to draw the control.
 */
class UniformlyDrawable: Control
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    ControlStates _controlStates;
    bool _customDraw;
    @NoGc static __gshared UniformlyDrawableVisitor _udVisitor;

protected:

    StateChangeEvent _onCheckedChanged;
    StateChangeEvent _onSelectedChanged;
    StateChangeEvent _onMultiSelectedChanged;
    StateChangeEvent _onDownChanged;
    StateChangeEvent _onDisabledChanged;

    // Control overrides ------------------------------------------------------+
    override void paint(ref Canvas canvas)
    {
        bmp.clear();
        _needRepaint = false;
        if (!_customDraw)
            callVisitor();
        doPaint(canvas);
    }

    override void unsetFocus()
    {
        needRepaint = true;
        repaint();
    }

    override void setFocus()
    {
        needRepaint = true;
        repaint();
    }
    // ----

    // UniformlyDrawable methods ----------------------------------------------+

    /// Virtual method called when the selected state has changed.
    void procSelected(bool value){}

    /// Virtual method called when the multiSelected state has changed.
    void procMultiSelected(bool value){}

    /// Virtual method called when the down state has changed.
    void procDown(bool value){}

    /// Virtual method called when the disabled state has changed.
    void procDisabled(bool value){}

    /// Virtual method called when the checked state has changed.
    void procChecked(bool value){}

    /// To be mixed in any $(D callVisitor) override.
    enum callVisitorImpl =
    q{
        if (_udVisitor)
        {
            if (!isCustomUD)
                _udVisitor.visit(this, bmp.canvas);
            else
                _udVisitor.visit(cast(UniformlyDrawable) this, bmp.canvas);
        }
    };

    /**
     * Calls the visitor in order to be redrawn.
     *
     * Should be called when $(D procSelected()), $(D procMultiSelected())
     * and the other methods related to the state modification are called.
     */
    void callVisitor()
    {
        mixin(callVisitorImpl);
    }
    // ----

public:

    this()
    {
        super();
        collectPublications!(UniformlyDrawable);
        isCustomUD = false;
        wantMouse = true;
        focusable = true;
    }

    /**
     * Indicates wether the control is not part of the factory widget set.
     * When set in the constructor, the control calls the visitor in a
     * specific method that allows custom draws.
     */
    immutable bool isCustomUD;

    /**
     * Returns: the ID of a control that's not included in the widget set and
     * that should be used by a visitor when it visits an unspecialized
     * UniformlyDrawable.
     */
    ubyte customControlID(){return 0;}

    /**
     * Sets or gets the $(D UniformlyDrawableVisitor) used to draw the control.
     * It is common to the whole widgetset.
     */
    final void uniformDrawer(UniformlyDrawableVisitor value)
    {
        _udVisitor = value;
        repaint();
    }

    /// ditto
    final UniformlyDrawableVisitor uniformDrawer() {return _udVisitor;}

    /**
     * Sets or gets if the control state includes selected.
     * Depending on the derived control, this state migh be used or not.
     */
    @Set void selected(bool value)
    {
        if ((ControlState.selected in _controlStates) == value)
            return;
        _controlStates[ControlState.selected] = value;
        procSelected(value);
    }

    /// ditto
    @Get bool selected() {return ControlState.selected in _controlStates;}

    /**
     * Sets or gets if the control state includes multiSelected.
     * Depending on the derived control, this state migh be used or not.
     */
    @Set void multiSelected(bool value)
    {
        if ((ControlState.multiSelected in _controlStates) == value)
            return;
        _controlStates[ControlState.multiSelected] = value;
        procMultiSelected(value);
    }

    /// ditto
    @Get bool multiSelected() {return ControlState.multiSelected in _controlStates;}

    /**
     * Sets or gets if the control state includes down.
     * Depending on the derived control, this state migh be used or not.
     */
    @Set void down(bool value)
    {
        if ((ControlState.down in _controlStates) == value)
            return;
        _controlStates[ControlState.down] = value;
        procDown(value);
    }

    /// ditto
    @Get bool down() {return ControlState.down in _controlStates;}

    /**
     * Sets or gets if the control state includes disabled.
     * Depending on the derived control, this state migh be used or not.
     */
    @Set void disabled(bool value)
    {
        if ((ControlState.disabled in _controlStates) == value)
            return;
        _controlStates[ControlState.disabled] = value;
        procDisabled(value);
    }

    /// ditto
    @Get bool disabled() {return ControlState.disabled in _controlStates;}

    /**
     * Sets or gets if the control state includes disabled.
     * Depending on the derived control, this state migh be used or not.
     */
    @Set void checked(bool value)
    {
        if ((ControlState.checked in _controlStates) == value)
            return;
        _controlStates[ControlState.checked] = value;
        procChecked(value);
    }

    /// ditto
    @Get bool checked() {return ControlState.checked in _controlStates;}

    /**
     * Sets or gets if the control is only drawn customly.
     *
     * When the value is set to $(D true), the visitor is never called and
     * only the $(D onPaint) event can be used to render its visual.
     */
    @Set void customDraw(bool value)
    {
        if (_customDraw == value)
            return;
        _customDraw = value;
        repaint();
    }

    /// ditto
    @Get bool customDraw() {return _customDraw;}

    /**
     * Allows to get drawn by the visitor even if $(D customDraw).
     */
    final void themedPaint() {callVisitor();}

    /// Sets or gets the event that called when the checked state has changed.
    @Set void onCheckedChanged(StateChangeEvent value)
    {_onCheckedChanged = value;}

    /// ditto
    @Get StateChangeEvent onCheckedChanged() {return _onCheckedChanged;}

    /// Sets or gets the event that called when the selected state has changed.
    @Set void onSelectedChanged(StateChangeEvent value)
    {_onSelectedChanged = value;}

    /// ditto
    @Get StateChangeEvent onSelectedChanged() {return _onSelectedChanged;}

    /// Sets or gets the event that called when the multiselected state has changed.
    @Set void onMultiSelectedChanged(StateChangeEvent value)
    {_onMultiSelectedChanged = value;}

    /// ditto
    @Get StateChangeEvent onMultiSelectedChanged() {return _onMultiSelectedChanged;}

    /// Sets or gets the event that called when the down state has changed.
    @Set void onDownChanged(StateChangeEvent value)
    {_onDownChanged = value;}

    /// ditto
    @Get StateChangeEvent onDownChanged() {return _onDownChanged;}

    /// Sets or gets the event that called when the disabled state has changed.
    @Set void onDisabledChanged(StateChangeEvent value)
    {_onDisabledChanged = value;}

    /// ditto
    @Get StateChangeEvent onDisabledChanged() {return _onDisabledChanged;}
}

/**
 * Represents a theme, able to draw any control from the uniformly drawable
 * widgetset. Once an instance is assigned to an $(D UniformlyDrawable) it's
 * automatically used.
 */
class UniformlyDrawableVisitor
{

    mixin inheritedDtor;

public:

    /// Draws an UniformlyDrawable that verifies $(D isCustomUD).
    abstract void visit(UniformlyDrawable udc, ref Canvas canvas);
    /// Draws a UDButton.
    abstract void visit(UDButton udc, ref Canvas canvas);
    /// Draws a UDBitButton.
    abstract void visit(UDBitButton udc, ref Canvas canvas);
    /// Draws a UDPathButton.
    abstract void visit(UDPathButton udc, ref Canvas canvas);
    /// Draws a UDCheckBox.
    abstract void visit(UDCheckBox udc, ref Canvas canvas);
    /// Draws a UDGroupBox.
    abstract void visit(UDGroupBox udc, ref Canvas canvas);
    /// Draws a UDPathCheckBox.
    abstract void visit(UDPathCheckBox udc, ref Canvas canvas);
    /// Draws a UDCustomEditField.
    abstract void visit(UDEditField udc, ref Canvas canvas);
}

class SimpleDrawableVisitor: UniformlyDrawableVisitor
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Pen     _penFrame;
    Brush   _brushFocused;
    Brush   _brushFrame;
    Brush   _brushFill1;
    Brush   _brushFill2;
    Brush   _brushFont;
    Font    _captionFont;
    double   _elementHeight = 22.0;
    ptrdiff_t _updateCount;

    CustomControl _root;

    void tryRepaint()
    {
        if (_root)
            _root.invalidate(true);
    }

    void fillDrawArea(UniformlyDrawable ud, ref Canvas canvas)
    {
    }

    void strokeFocused(UniformlyDrawable ud, ref Canvas canvas)
    {
        if (!ud.focused)
        {
            canvas.fillBrush = _brushFill1;
            canvas.rectangle(ud.sizeRect);
            canvas.fill!true;
        }
        else
        {
            canvas.strokeBrush = _brushFocused;
            canvas.rectangle(ud.sizeRect);
            canvas.stroke!true;
        }
    }

public:

    /**
     * Constructs a new instance.
     *
     * Params:
     *      root = The root control, optional. When set and if a property changes
     *      then the visitor is able to redraw the controls.
     */
    this(CustomControl udRoot = null)
    {
        _penFrame = construct!Pen;
        _brushFrame = construct!Brush;
        _brushFill1 = construct!Brush;
        _brushFill2 = construct!Brush;
        _brushFont  = construct!Brush;
        _brushFocused=construct!Brush;
        _captionFont= construct!Font;

        _brushFont.color  = ColorConstant.black;
        _brushFrame.color = ColorConstant.black;
        _brushFill1.color = ColorConstant.white;
        _brushFill2.color = ColorConstant.lightsteelblue;
        _brushFocused.color = ColorConstant.deepskyblue;
        _penFrame.width   = 1.0f;

        _captionFont.size = 13;

        collectPublications!SimpleDrawableVisitor;

        root(udRoot);
    }

    ~this()
    {
        destructEach(_penFrame, _brushFrame, _brushFill1, _brushFill2,
            _captionFont, _brushFocused);
        callInheritedDtor();
    }

    /// Begins a bulk modifications of the theme properties.
    void beginUpdate()
    {
        ++_updateCount;
    }

    /// Ends a bulk modifications of the theme properties and tries to repaint.
    void endUpdate()
    {
        --_updateCount;
        if (_updateCount <= 0)
        {
            tryRepaint();
            _updateCount = 0;
        }
    }

    /**
     * Sets or gets the root control. When set and if a property changes
     * then the visitor is able to redraw the controls.
     */
    @Set void root(CustomControl value)
    {
        _root = value;
        tryRepaint();
    }

    /// ditto
    @Get CustomControl root() {return _root;}

    /// Sets of gets the height of the elements shapes.
    @Set void elementHeight(double value)
    {
        if (_elementHeight == value)
            return;
        _elementHeight = value;
        beginUpdate();
        endUpdate();
    }
    /// ditto
    @Get double elementHeight() {return _elementHeight;}

    /// Draws an UniformlyDrawable that verifies $(D isCustomUD).
    override void visit(UniformlyDrawable udc, ref Canvas canvas)
    {
        /+/* Dispatch to an handler dedicated to the udc custom ID */
        switch (udc.customControlID)
        {
            default: break;
        }
        +/
    }

    /// Draws a UDButton.
    override void visit(UDButton udc, ref Canvas canvas)
    {
        Rect rc = udc.drawRect();
        strokeFocused(udc, canvas);

        canvas.pen = _penFrame;
        canvas.strokeBrush = _brushFrame;
        canvas.rectangle(rc);
        canvas.fillBrush = udc.down() ? _brushFill2 : _brushFill1;
        canvas.fill!false;
        canvas.stroke!true;

        if (udc.caption() != "")
        {
            canvas.font = _captionFont;
            canvas.fillBrush = _brushFont;
            canvas.text(rc, udc.caption[], Justify.center);
        }

        canvas.restoreAllDefaults;
    }

    /// Draws a UDBitButton.
    override void visit(UDBitButton udc, ref Canvas canvas)
    {
        strokeFocused(udc, canvas);
    }

    /// Draws a UDPathButton.
    override void visit(UDPathButton udc, ref Canvas canvas)
    {
        strokeFocused(udc, canvas);
    }

    /// Draws a UDCheckBox.
    override void visit(UDCheckBox udc, ref Canvas canvas)
    {
        strokeFocused(udc, canvas);
        Rect rc = udc.drawRect();
        double side = (cast(int)_elementHeight) - 0.5;
        rc.height = side;
        rc.width = side;

        canvas.rectangle(rc);
        canvas.fillBrush = _brushFill1;
        if (!udc.checked)
            canvas.strokeBrush = _brushFrame;
        else
            canvas.strokeBrush = _brushFill2;

        canvas.pen = _penFrame;
        canvas.fill!false;
        canvas.stroke!true;

        if (udc.checked)
        {
            rc.left += 3;
            rc.top  += 3;
            rc.width  -= 6;
            rc.height -= 6;
            canvas.rectangle(rc);
            canvas.fillBrush = _brushFill2;
            canvas.fill!true;
        }

        if (udc.caption() != "")
        {
            canvas.font = _captionFont;
            canvas.fillBrush = _brushFont;
            rc = udc.drawRect();
            rc.height = _elementHeight;
            rc.left += rc.height + 2;
            rc.width -= rc.left + 2;
            canvas.text(rc, udc.caption[]);
        }

        canvas.restoreAllDefaults;
    }

    override void visit(UDGroupBox udc, ref Canvas canvas)
    {
        strokeFocused(udc, canvas);
        canvas.pen = _penFrame;
        canvas.font = _captionFont;
        TextExtent te = canvas.textSize(udc.caption[]);

        Rect rect = udc.drawRect;
        rect.left += 2.5;
        rect.width -= 4;
        rect.top += te.height * 0.5 + 0.5;
        rect.height -= te.height;
        canvas.strokeBrush = _brushFrame;
        canvas.fillBrush = _brushFill1;
        canvas.rectangle(rect);
        canvas.fill!false;
        canvas.stroke!true;

        rect.top = 0;
        rect.left = 4;
        rect.width = te.width + 10;
        rect.height = te.height;
        canvas.fillBrush = _brushFill1;
        canvas.rectangle(rect);
        canvas.fill!true;

        canvas.fillBrush = _brushFont;
        canvas.text(8, te.height, udc.caption[]);

        canvas.restoreAllDefaults;
    }

    /// Draws a UDCheckBox.
    override void visit(UDPathCheckBox udc, ref Canvas canvas)
    {
        strokeFocused(udc, canvas);
        canvas.drawPath(udc.pathData);
        canvas.beginPath(0,0);
        canvas.strokeBrush = _brushFrame;
        canvas.fillBrush = udc.checked ? _brushFill2 : _brushFill1;
        canvas.pen = _penFrame;
        canvas.fill!false;
        canvas.stroke!true;

        if (udc.caption() != "")
        {
            Rect rc = udc.drawRect();
            canvas.font = _captionFont;
            canvas.fillBrush = _brushFont;
            rc.height = _elementHeight;
            const double lft = udc._pathData.extents.width;
            rc.left += lft + 2;
            rc.width -= lft + 2;
            canvas.text(rc, udc.caption[]);
        }

        canvas.restoreAllDefaults;
    }

    /// Draws a UDCustomEditField.
    override void visit(UDEditField udc, ref Canvas canvas)
    {
        strokeFocused(udc, canvas);
        canvas.pen = _penFrame;
        canvas.strokeBrush = _brushFrame;
        canvas.font = _captionFont;
        canvas.fillBrush = _brushFill1;

        Rect rc = udc.drawRect;
        canvas.rectangle(rc);
        canvas.fill!false;
        canvas.stroke!true;

        Array!char txt = udc.masked ? udc.maskedText : udc.editText;

        canvas.fillBrush = _brushFont;
        TextExtent szAll = canvas.textSize(txt);
        TextExtent szLft = udc.masked
            ? canvas.textSize(txt[0..udc.caretPosition])
            : canvas.textSize(txt[0..udc.caretPositionInUnits]);

        if (szLft.width >= rc.width)
            rc.left -= szLft.width - rc.width + canvas.maxCharWidth * 0.5;


        double y = (rc.height - canvas.maxTextHeight) * 0.5;

        if (udc.selectionLength)
        {
            TextExtent lft;
            TextExtent rgt;
            if (udc.selStop >= udc.selStart)
            {
                lft = canvas.textSize(txt[0..udc.selStart]);
                rgt = canvas.textSize(txt[udc.selStart..udc.selStop]);
            }
            else
            {
                lft = canvas.textSize(txt[0..udc.selStop]);
                rgt = canvas.textSize(txt[udc.selStop..udc.selStart]);
            }
            canvas.fillBrush = _brushFill2;
            canvas.rectangle(lft.width, y, rgt.width, canvas.maxTextHeight);
            canvas.fill!true;
        }

        canvas.fillBrush = _brushFont;
        canvas.text(rc.left, rc.top + canvas.textBaseLineY(rc.height), txt[]);
        canvas.rectangle(rc.left + szLft.width + 1, y, 2, canvas.maxTextHeight);
        canvas.fill!true;

        canvas.restoreAllDefaults;
    }
}

/**
 * Ancestor of all the controls that redirect their children
 * to a specific sub-container.
 */
class UDRedirectedChildren: UniformlyDrawable
{
    mixin inheritedDtor;

protected:

    UniformlyDrawable _container;
    bool _redirect;

public:

    this()
    {
        super();
        _container = addClientControl!UniformlyDrawable;
        _redirect = true;
    }

    override void addChild(CustomControl child)
    {
        if (_redirect)
        {
            _container.addChild(child);
            _container.tryRealign;
        }
        else super.addChild(child);
    }

    override CustomControl removeChild(size_t index)
    {
        CustomControl result;
        if (_redirect)
        {
            result = _container.removeChild(index);
            _container.tryRealign;
        }
        else result = super.removeChild(index);
        return result;
    }

    override bool removeChild(CustomControl ctrl)
    {
        bool result;
        if (_redirect)
        {
            result = _container.removeChild(ctrl);
            _container.tryRealign;
        }
        else result = super.removeChild(ctrl);
        return result;
    }

    override void insertChild(CustomControl child)
    {
        if (_redirect)
        {
            _container.insertChild(child);
            _container.tryRealign;
        }
        else super.insertChild(child);
    }

    override void insertChild(size_t index, CustomControl child)
    {
        if (_redirect)
        {
            _container.insertChild(index, child);
            _container.tryRealign;
        }
        else super.insertChild(index, child);
    }

    /// Returns: The control that hosts the children.
    UniformlyDrawable container() {return _container;}

    /**
     * Returns: The flag that indicates if a new child has to be added to the
     * container instead of in the control.
     */
    ref bool redirect() {return _redirect;}
}

/**
 * A standard groupBox
 */
class UDGroupBox: UDRedirectedChildren
{
    mixin inheritedDtor;

protected:

    override void callVisitor()
    {
        mixin(callVisitorImpl);
    }

public:

    this()
    {
        super();
        _container.marginAll(3);
        _container.marginTop(18);
    }
}

/**
 * The ancestor of all the buttons.
 *
 * Buttons modify the down state.
 */
class UDCustomButton: UniformlyDrawable
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

protected:

    bool _hold;

    override void procDown(bool value)
    {
        _controlStates[ControlState.down] = value;
        _needRepaint = true;
        repaint;
        if (!_hold)
            return;
        if (_onDownChanged)
            _onDownChanged(this, value);
    }

    override void procMouseDown(double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        super.procMouseDown(x, y, mb, km);
        if (MouseButton.left in mb)
        {
            down(!down());
        }
    }

    override void procMouseUp(double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        super.procMouseUp(x, y, mb, km);
        if (!_hold)
            down(false);
    }

public:

    this()
    {
        super();
        collectPublications!UDCustomButton;
    }

    /**
     * Sets or gets if the button remains pushed.
     */
    @Set void hold(bool value) {_hold = value;}

    /// ditto
    @Get bool hold(){return _hold;}

}

/// A standard button.
class UDButton: UDCustomButton
{

    mixin inheritedDtor;

protected:

    override void callVisitor()
    {
        mixin(callVisitorImpl);
    }
}

/// A button that contains a icon drawn from a path.
class UDPathButton: UDCustomButton
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

protected:

    PathData _pathData;

    static immutable _defaultPathData = "
    m 0.70476768,4.3216409 6.54383532,0
    L 8.9784774,9.2073046 15.134819,0.54104321
    22.036984,0.38517224 7.2175759,22.385172
    0.70476768,4.3216409 Z";

    override void callVisitor()
    {
        mixin(callVisitorImpl);
    }

    override bool isPointInside(double x, double y)
    {
        return _pathData.pointInPath(x - left, y - top);
    }

public:

    this()
    {
        super();
        _pathData = construct!PathData;
        collectPublications!UDPathButton;
        pathText = _defaultPathData;
    }

    ~this()
    {
        destruct(_pathData);
        callInheritedDtor();
    }

    /// Sets or gets the path as a string made of SVG data.
    @Set void pathText(const(char)[] value)
    {
        if (_pathData.svgData[] == value)
            return;
        _pathData.loadFromSvgData(value);
        Margin ex = _pathData.extents;
        _pathData.alignTo(PathAlignment.leftTop, Rect(0,0,30,30));
    }

    /// ditto
    @Get const(char)[] pathText() {return _pathData.svgData[];}

    /// Returns: The path data.
    PathData pathData(){return _pathData;}
}

/// A button that contains a icon drawn Bitmap.
class UDBitButton: UDCustomButton
{

    mixin inheritedDtor;

protected:

    override void callVisitor()
    {
        mixin(callVisitorImpl);
    }
}

/**
 * Ancestor of all the checkboxes.
 */
class UDCustomCheckBox: UniformlyDrawable
{
    mixin inheritedDtor;

private:

    bool _clicking;

protected:

    override void procChecked(bool value)
    {
        _needRepaint = true;
        repaint;
        if (_onCheckedChanged)
            _onCheckedChanged(this, value);
    }

    override void procMouseEnter()
    {
        super.procMouseEnter();
    }

    override void procMouseLeave()
    {
        super.procMouseLeave();
        _clicking = false;
    }

    override void procMouseDown(double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        super.procMouseDown(x, y, mb, km);
        if (focused && (MouseButton.left in mb))
            _clicking = true;
    }

    override void procMouseUp(double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        super.procMouseUp(x, y, mb, km);
        if (focused && (MouseButton.left !in mb) && _clicking)
        {
            checked(!checked());
            _clicking = false;
        }
    }
}

/**
 * A standard checkbox.
 */
class UDCheckBox: UDCustomCheckBox
{

    mixin inheritedDtor;

protected:

    override void callVisitor()
    {
        mixin(callVisitorImpl);
    }
}

/**
 * A checkbox whose the shape is determined by a user-defined path.
 */
class UDPathCheckBox: UDCustomCheckBox
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

protected:

    PathData _pathData;

    static immutable _defaultPathData = "
    m 0.70476768,4.3216409 6.54383532,0
    L 8.9784774,9.2073046 15.134819,0.54104321
    22.036984,0.38517224 7.2175759,22.385172
    0.70476768,4.3216409 Z";

    override void callVisitor()
    {
        mixin(callVisitorImpl);
    }

    override bool isPointInside(double x, double y)
    {
        return _pathData.pointInPath(x - left, y - top);
    }

public:

    this()
    {
        super();
        _pathData = construct!PathData;
        collectPublications!UDPathCheckBox;
        pathText = _defaultPathData;
    }

    ~this()
    {
        destruct(_pathData);
        callInheritedDtor();
    }

    /// Sets or gets the path as a string made of SVG data.
    @Set void pathText(const(char)[] value)
    {
        if (_pathData.svgData[] == value)
            return;
        _pathData.loadFromSvgData(value);
        Margin ex = _pathData.extents;
        _pathData.alignTo(PathAlignment.leftTop, Rect(0,0,30,30));
    }

    /// ditto
    @Get const(char)[] pathText() {return _pathData.svgData[];}

    /// Returns: The path data.
    PathData pathData(){return _pathData;}
}

/**
 * Ancestor of all the editable fields.
 */
class UDCustomEditField: UniformlyDrawable
{

    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

    invariant{assert(_caretPosInUnits > -1); assert(_caretPos > -1);}

private:

    ptrdiff_t _caretPos;
    ptrdiff_t _caretPosInUnits;
    ptrdiff_t _selStart;
    size_t _lengthInUnits;
    bool _selecting;
    char _maskChar = '*';
    bool _masked;
    bool _wantsTab;
    Array!char _editText;
    Array!char _maskedText;

protected:

    final void moveCaretToLeftWordEdge()
    {
        if (_caretPosInUnits == 0)
            return;
        if (masked)
        {
            while (_caretPosInUnits > 0)
                moveCaretLeft();
        }
        else
        {
            const char c = _editText[_caretPosInUnits-1];
            if (c.isWhite)
            {
                while (_editText[_caretPosInUnits-1].isWhite && _caretPosInUnits > 0)
                    moveCaretLeft();
            }
            else
            {
                while (!_editText[_caretPosInUnits-1].isWhite && _caretPosInUnits > 0)
                    moveCaretLeft();
            }
        }
        needRepaint = true;
        repaint;
    }

    final void moveCaretToRightWordEdge()
    {
        if (_caretPosInUnits == _editText.length)
            return;
        if (masked)
        {
            while (_caretPosInUnits + 1 <= _editText.length)
                moveCaretRight();
        }
        else
        {
            const char c = _editText[_caretPosInUnits];
            if (c.isWhite)
            {
                while (_editText[_caretPosInUnits].isWhite &&
                    _caretPosInUnits + 1 <= _editText.length)
                        moveCaretRight();
            }
            else
            {
                while (!_editText[_caretPosInUnits].isWhite &&
                    _caretPosInUnits + 1 <= _editText.length)
                        moveCaretRight();
            }
        }
        needRepaint = true;
        repaint;
    }

    final void moveCaretLeft()
    {
        if (_caretPosInUnits == 0)
            return;
        _caretPosInUnits -= _editText[0.._caretPosInUnits].strideBack();
        _caretPos -= 1;
        if (!_selecting)
            _selStart = _caretPosInUnits;
    }

    final void moveCaretRight()
    {
        if (_caretPosInUnits == _editText.length)
            return;
        const size_t len = (_caretPosInUnits + 4) <= _editText.length ?
            _caretPosInUnits + 4 : _editText.length;
        _caretPosInUnits += _editText[_caretPosInUnits..len].stride();
        _caretPos += 1;
        if (!_selecting)
            _selStart = _caretPosInUnits;
    }

    final void insertChar(dchar value)
    {
        selecting(false);

        const string s = to!string(value);
        _editText = _editText[0.._caretPosInUnits] ~ s ~ _editText[_caretPosInUnits..$];
        _caretPosInUnits += s.length;
        _selStart = _caretPosInUnits;
        _caretPos += 1;
        _needRepaint = true;
        repaint;
    }

    final void replaceSel(const(char)[] value)
    {
        size_t len;
        if (_caretPosInUnits >= _selStart)
        {
            len = std.utf.count(_editText[_selStart .. _caretPosInUnits]);
            _editText = _editText[0.._selStart] ~ value ~ _editText[_caretPosInUnits..$];
            _caretPosInUnits = _selStart;
            _caretPos -= len;
        }
        else
        {
            len = std.utf.count(_editText[_caretPosInUnits.._selStart]);
            _editText = _editText[0.._caretPosInUnits] ~ value ~ _editText[_selStart..$];
        }
        _selStart = _caretPosInUnits;
        selecting(false);

        _needRepaint = true;
        repaint;
    }

    final void deleteChar()
    {
        selecting(false);

        size_t len = _editText[0.._caretPosInUnits].strideBack;
        _editText = _editText[0.._caretPosInUnits-len] ~ _editText[_caretPosInUnits..$];
        _caretPosInUnits -= len;
        _caretPos -= 1;
        _selStart = _caretPosInUnits;
        _needRepaint = true;
        repaint;
    }

    final void deleteSel()
    {
        size_t len;
        if (_caretPosInUnits >= _selStart)
        {
            len = std.utf.count(_editText[_selStart .. _caretPosInUnits]);
            _editText = _editText[0.._selStart] ~ _editText[_caretPosInUnits..$];
            _caretPosInUnits = _selStart;
            _caretPos -= len;
        }
        else
        {
            len = std.utf.count(_editText[_caretPosInUnits.._selStart]);
            _editText = _editText[0.._caretPosInUnits] ~ _editText[_selStart..$];
        }
        _selStart = _caretPosInUnits;
        selecting(false);

        _needRepaint = true;
        repaint;
    }

    override void procKeyDown(dchar key, const ref KeyModifiers km)
    {
        const bool wasSelecting = _selStart != _caretPosInUnits;
        selecting(KeyModifier.shift in km);
        const bool ctrl = KeyModifier.ctrl in km;

        with (VirtualKey) switch (key)
        {
        case '\v', '\r', '\n', '\f':
            break;
        case '\t':
            if (_wantsTab)
                goto default;
            else
                break;
        case VK_LEFT:
            if (ctrl)
                moveCaretToLeftWordEdge();
            else
                caretPosition(_caretPos - 1);
            break;
        case VK_RIGHT:
            if (ctrl)
                moveCaretToRightWordEdge();
            else
                caretPosition(_caretPos + 1);
            break;
        case VK_HOME:
            caretPosition(0);
            break;
        case VK_END:
            caretPosition(_editText.length);
            break;
        case VK_BACKSPACE, VK_DELETE:
            if (wasSelecting)
                deleteSel();
            else if (_caretPos > 0)
                deleteChar();
            break;
        default:
            if (!isVirtualKey(key))
            {
                if ((selecting && selectionLength > 0) || wasSelecting)
                    replaceSel(to!string(key));
                else
                    insertChar(key);
            }
        }

        if (!wantsTab && key == '\t')
            procTabNext;
        if (onKeyDown)
            onKeyDown()(this, key, km);
    }

    override void procMouseDown(double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        super.procMouseDown(x, y, mb, km);
        if (_editText.length)
        {
        }
    }

public:

    this()
    {
        super();
        collectPublications!UDCustomEditField;
    }

    ~this()
    {
        destruct(_editText);
        destruct(_maskedText);
        callInheritedDtor;
    }

    /**
     * Sets or gets the caret position, in code points.
     */
    void caretPosition(ptrdiff_t value)
    {
        if (_caretPos == value)
            return;
        else if (value > _caretPos)
            foreach(immutable i; _caretPos..value)
                moveCaretRight;
        else
            foreach(immutable i; value.._caretPos)
                moveCaretLeft;

        if (!_selecting)
            _selStart = _caretPosInUnits;

        _needRepaint = true;
        repaint;
    }

    /// ditto
    ptrdiff_t caretPosition() {return _caretPos;}

    /// Returns: the caret position in code units.
    ptrdiff_t caretPositionInUnits() {return _caretPosInUnits;}

    /**
     * Sets or gets if a slice is being selected.
     */
    void selecting(bool value)
    {
        if (_selecting == value)
            return;
        _selecting = value;
    }

    /// ditto
    bool selecting() {return _selecting;}

    ptrdiff_t selectionLength()
    {
        ptrdiff_t result;
        if (_caretPosInUnits >= _selStart)
            result = _caretPosInUnits - _selStart;
        else
            result = _selStart - _caretPosInUnits;
        return result;
    }

    ptrdiff_t selStart(){return _selStart;}

    ptrdiff_t selStop(){return _caretPosInUnits;}

    void selectedText(const(char)[] value)
    {
        if (_caretPosInUnits != _selStart)
        {
            ptrdiff_t st = _caretPosInUnits >= _selStart ? _selStart : _caretPosInUnits;
            replaceSel(value);
            _selStart = st;
            _caretPosInUnits = st + value.length;
            _caretPos = std.utf.count(_editText[0.._caretPosInUnits]);
            needRepaint = true;
            repaint;
        }
    }

    /**
     * Sets or gets if the text is masked.
     */
    @Set void masked(bool value)
    {
        if (_masked == value)
            return;
        _masked = value;
        needRepaint = true;
        repaint;
    }

    /// ditto
    @Get bool masked() {return _masked;}

    /**
     * Sets or gets if tabs are filtered.
     */
    @Set void wantsTab(bool value) {_wantsTab = true;}

    /// ditto
    @Get bool wantsTab() {return _wantsTab;}

    /**
     * Sets or gets the character used to mask the text
     * when secret is true.
     */
    @Set void maskChar(char value)
    {
        if (_maskChar == value)
            return;
        _maskChar = value;
        if (_masked)
        {
            needRepaint = true;
            repaint;
        }
    }

    /// ditto
    @Get char maskChar() {return _maskChar;}

    /**
     * Sets or get the editable text.
     */
    @Set void editText(const(char)[] value)
    {
        _editText = value;
        caretPosition(size_t.max);
    }

    /// ditto
    @Get const(char)[] editText(){return _editText[];}

    /// Returns: the masked text.
    const(char)[] maskedText()
    {
        const size_t len = count(_editText[]);
        if (len != _maskedText.length)
        {
            _maskedText.length = len;
            _maskedText[0..$] = _maskChar;
        }
        return _maskedText[];
    }
}

/**
 * A standard edit.
 */
class UDEditField: UDCustomEditField
{

    mixin inheritedDtor;

protected:

    override void callVisitor()
    {
        mixin(callVisitorImpl);
    }
}

unittest
{
    static assert(!MustAddGcRange!UniformlyDrawable);
    static assert(!MustAddGcRange!UniformlyDrawableVisitor);
}

