/**
 * Pen
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.pen;

import
    std.math;
import
    iz.types, iz.properties, iz.memory, iz.containers;
import
    kheops.types;

package
{
    const double[2] dashingDot1  = [1.0, 1.0];
    const double[2] dashingDot2  = [1.0, 2.0];
    const double[2] dashingDot4  = [1.0, 4.0];
    const double[2] dashingDash2 = [2.0, 2.0];
    const double[2] dashingDash4 = [4.0, 4.0];
    const double[4] dashingDashDot1 = [4.0, 1.0, 1.0, 1.0];
    const double[4] dashingDashDot2 = [4.0, 2.0, 1.0, 2.0];
    const double[4] dashingDashDot4 = [4.0, 4.0, 1.0, 4.0];
}

static this()
{
    classesRepository.registerFactoryClass!Pen;
}

/**
 *
 */
class Pen: PropertyPublisher
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    ptrdiff_t _changedCount;
    Array!double _customDashing;
    double _width = 1.0;
    Dash _dash;
    Join _join;
    LineCap _cap;
    Event _onChanged;

    enum doChanged =
    q{
        if (_changedCount > 0)
            return;
        if (_onChanged)
            _onChanged(this);
        _changedCount = 0;
    };

public:

    ///
    this()
    {
        collectPublications!Pen;
    }

    ~this()
    {
        destruct(_customDashing);
        callInheritedDtor;
    }

    /**
     * Copies the data from another Pen.
     */
    void copyFrom(Pen value)
    {
        beginChange;
        _width = value._width;
        _dash = value._dash;
        _join = value._join;
        _cap = value._cap;
        _customDashing = value._customDashing.dup;
        endChange;
    }

    /**
     * Sets or gets the pen width.
     */
    @Set void width(double value)
    {
        if (_width.isClose(value))
            return;
        _width = value;
        mixin(doChanged);
    }

    /// ditto
    @Get double width(){return _width;}

    /**
     * Sets or gets the dashing kind.
     */
    @Set void dash(Dash value)
    {
        if (_dash == value)
            return;
        _dash = value;
        mixin(doChanged);
    }

    /// ditto
    @Get Dash dash(){return _dash;}

    /**
     * Sets or gets the custom dashing pattern.
     *
     * A pattern is made of a count of on pixels followed by a count of
     * off pixels. Note to modify the array returned by the getter is a
     * noop, it's only provided for reading.
     */
    @Set void customDashing(double[] value)
    {
        if (_customDashing == value)
            return;
        _customDashing = value.dup;
        mixin(doChanged);
    }

    /// ditto
    @Get double[] customDashing() {return _customDashing[];}

    /**
     * Sets or gets the line join.
     */
    @Set void join(Join value)
    {
        if (_join == value)
            return;
        _join = value;
        mixin(doChanged);
    }

    /// ditto
    @Get Join join(){return _join;}

    /**
     * Sets or gets the pen line cap.
     */
    @Set void cap(LineCap value)
    {
        if (_cap == value)
            return;
        _cap = value;
        mixin(doChanged);
    }

    /// ditto
    @Get LineCap cap(){return _cap;}

    /**
     * Signal fired when the pen has changed.
     *
     * It usually indicates that a repaint is needed.
     */
    @Set void onChanged(Event value) {_onChanged = value;}

    /// ditto
    @Get Event onChanged(){return _onChanged;}

    /**
     * Manually triggers and forces the onChanged event.
     */
    void changed() {_changedCount = 0; mixin(doChanged);}

    /**
     * Prevents the onChanged event to be fired while several properties
     * are modified. Must be followed by a call to endChange.
     */
    void beginChange(){++_changedCount;}

    /// ditto
    void endChange()
    {
        --_changedCount;
        mixin(doChanged);
    }
}

unittest
{
    static assert(!MustAddGcRange!Pen);
}

