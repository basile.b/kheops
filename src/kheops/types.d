/**
* Common Kheops types
*
* Authors: Basile B.
*
* License:
*  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
*/
module kheops.types;

import
    std.traits, std.math;
import
    iz.enumset, iz.containers;
version(linux) import
    x11.keysymdef;

// Graphic types --------------------------------------------------------------+

/// A point
struct PointOf(T)
if (isNumeric!T)
{
    /// The x coordinate.
    T x = 0;
    /// The y coordinate.
    T y = 0;

    void opOpAssign(string op, T : typeof(this))(auto ref const(T) value)
    {
        mixin("x" ~ op ~ "=value.x;");
        mixin("y" ~ op ~ "=value.y;");
    }
}

/// Point of $(D double) coordinates.
alias Point = PointOf!double;

/// Point of $(D int) coordinates.
alias PointI32 = PointOf!int;

//__gshared HashMap_AB!(string, TypeInfo_Class) classesRepository;
__gshared TypeInfo_Class[string] classesRepository;

/// Like a Point but used when x and y represents a size.
struct SizeOf(T)
if (isNumeric!T)
{
    /// The width.
    T w = 0;
    /// The height.
    T h = 0;
}

/// Size of $(D double) coordinates.
alias Size = SizeOf!double;

/// Size of $(D int) coordinates.
alias SizeI32 = SizeOf!int;

// An GC-free array of Point.
//alias Points(T) = Array!(Point!T);

alias Points = Point[];

/// A rectangle
struct RectOf(T)
if (isNumeric!T)
{
    /// The x coordinate.
    T left = 0;
    /// The y coordinate.
    T top = 0;
    /// The width.
    T width = 80;
    /// The height.
    T height = 50;
    /// The x2 coordinate.
    T right() const {return left + width;}
    /// The y2 coordinate.
    T bottom() const {return top + height;}
}

/// Rect of $(D double) coordinates.
alias Rect = RectOf!double;

/// Rect of $(D int) coordinates.
alias RectI32 = RectOf!int;

/// A margin
struct MarginOf(T)
if (isNumeric!T)
{
    /// left margin
    T left = 0;
    /// top margin.
    T top = 0;
    /// right margin.
    T right = 0;
    /// bottom margin.
    T bottom = 0;
    /// width
    T width() const {return right - left;}
    /// height
    T height() const {return bottom - top;}

    /// Sets left, top, right and bottom at once.
    void all(T value)
    {
        left = value;
        top = value;
        right = value;
        bottom = value;
    }

    /**
     * When left, top, right and bottom values are apprimatively equal return
     * the value otherwise NaN.
     */
    T all()
    {
        if (left.isClose(top) &&
            top.isClose(right) &&
            right.isClose(bottom))
            return left;
        else
            return T.init;
    }
}

/// Margin of $(D double) values.
alias Margin = MarginOf!double;

struct TextExtent
{
    double width;
    double height;
    double bearingX;
    double bearingY;
}

/**
 * Enumerates the possible ways a shape is filled.
 */
enum FillKind: ubyte
{
    none,       /// The shape is transparent
    uniform,    /// The shape is filled with an uniform color.
    gradient,   /// The shape is filled with a gradient.
    bitmap      /// The shape is filled with an image
}

/**
 * Enumerates the possible gradient kinds.
 */
enum GradientKind: ubyte
{
    horizontal, /// The gradient stops are on the horizontal axis.
    vertical,   /// The gradient stops are on the vertical axis.
    radial      /// The gradient stops are projected on a circle.
}

/**
 * Enumerates the possible way a fill is adjusted.
 */
enum FillAdjustment: ubyte
{
    none,
    repeat,
    reflect,
    pad
}

/**
 * Enumerates possible dash patterns.
 */
enum Dash: ubyte
{
    none,   /// Plain line.
    custom, /// Use a user defined pattern.
    dot1,
    dot2,
    dot4,
    dash2,
    dash4,
    dashdot1,
    dashdot2,
    dashdot4
}

/// Enumerates the possible line joins.
enum Join: ubyte
{
    miter,
    round,
    bevel
}

/// Enumerates the possible line cap.
enum LineCap: ubyte
{
    butt,
    round,
    square
}

/**
 * Enumerates the alignment kinds a control can have.
 */
enum Alignment: ubyte
{
    none,   /// The control has a fixed position and a fixed size.
    left,   /// The control is aligned to the left (width and left, top and bottom are constrained).
    right,  /// The control is aligned to the right (width and top and bottom are constrained).
    top,    /// The control is aligned to the top (height and left and width are constrained).
    bottom, /// The control is aligned to the bottom (height and left and width are constrained).
    client  /// The control occupies the remaining space in its client.
}


/**
 * Enumerates the font styles
 */
enum FontStyle: ubyte
{
    bold,
    italic
}

/// Set of font styles.
alias FontStyles = EnumSet!(FontStyle, Set8);

/**
 * Enumerates the different corner kinds.
 */
enum CornerKind: ubyte
{
    none,   /// The corner is a right angle.
    round,  /// The corner is a circle quad.
    line    /// The corner is a straight line.
}

enum EllipseKind: ubyte
{
    strict,
    scaled
}

enum LineKind: ubyte
{
    topToTop,
    bottomToBottom,
    leftToLeft,
    righToRight,
    topLeftToBottomRight,
    BottomLeftToTopRight
}

enum Axis: ubyte
{
    horizontal,
    vertical
}

enum Justify: ubyte
{
    left,
    center,
    right
}

enum Stretch: ubyte
{
    none,
    scaled,
    unscaled
}

/// Enumerates the ways a path can be positioned in a rectangle.
enum PathAlignment: ubyte
{
    /// No change.
    none,
    /// Centers.
    center,
    /// Snaps to the left edge and center Y.
    leftCenter,
    /// Snaps to the top left corner.
    leftTop,
    /// Snaps to the right edge and center Y.
    rightCenter,
    /// Centers X and snaps to the top edge.
    centerTop,
    /// Centers X and snaps to the bottom edge.
    centerBottom,
    // Snaps to the left then scales to the smallest edge
    //fitScaled,
    /// Snaps to the left then scales to each edge.
    fitUnscaled,
}
// ----

// Window types ---------------------------------------------------------------+

enum WindowState
{
    normal,
    minimized,
    maximized
}

enum WindowType: ubyte
{
    dialog,
    normal,
    splash,
    tool
}


enum WindowCloseKind
{
    close,      /// the program does not terminate, window is destroyed.
    hide,       /// the program does not terminate, window is just hidden.
    terminate   /// the program terminates, window is destroyed.
}

alias WindowCloseEvent = void delegate(Object notifier,
    WindowCloseKind closeKind);

/**
 * Prototype of the event called when a window could be closed.
 *
 * Params:
 *      notifier = The OsWindow that could be closed.
 *      closeKind = What's gonna happen if the window is closed.
 *      accept = When set to false, the window is not closed.
 */
alias WindowCloseQueryEvent = void delegate(Object notifier,
    WindowCloseKind closeKind, ref bool accept);

// ----

// Mouse types ----------------------------------------------------------------+

/// Enumerates the possible mouse buttons
enum MouseButton: ubyte
{
    left,
    middle,
    right,
    x1,
    x2
}

/// Set of MouseButton
alias MouseButtons = EnumSet!(MouseButton, Set8);

/// Prototype of the event assignable when a mouse button is pressed.
alias MouseDownEvent = void delegate(Object notifier, double x, double y,
    const ref MouseButtons mb, const ref KeyModifiers km);

alias MouseUpEvent = MouseDownEvent;

/// Prototype of the event assignable when the mouse is moving.
alias MouseMoveEvent = void delegate(Object notifier, double x, double y,
    const ref KeyModifiers km);

/// Prototype of the event assignable when the mouse wheel is used.
alias MouseWheelEvent = void delegate(Object notifier, byte delta,
    const ref KeyModifiers km);

// ----

// Keyboard types -------------------------------------------------------------+

/// Enumerate the possible key modifiers.
enum KeyModifier: ubyte
{
    /// The Ctrl key.
    ctrl,
    /// The shift (Maj) key.
    shift,
    /// The alt key.
    alt,
    /// The Meta key
    meta
}

/// Set of KeyModifier
alias KeyModifiers = EnumSet!(KeyModifier, Set8);

/// Prototype of the event called when a Key is pressed.
alias KeyDownEvent = void delegate(Object notifier, dchar key,
    const ref KeyModifiers km);

/// Prototype of the event called when a Key is released
alias KeyUpEvent = KeyDownEvent;

/**
 * Prototype of the event called when a shortcut happens.
 *
 * Note that the shortcut can be build using key and km as parameter
 * of iz.helpers.key.Shortcut.
 */
alias ShortcutEvent = KeyDownEvent;


version(linux)
{
    enum VirtualKey: int
    {

        VK_NONE     = 0,

        VK_LEFT     = XK_Left,
        VK_RIGHT    = XK_Right,
        VK_UP       = XK_Up,
        VK_DOWN     = XK_Down,

        VK_ALT_L    = XK_Alt_L,
        VK_ALT_R    = XK_Alt_R,
        VK_CTRL_L   = XK_Control_L,
        VK_CTRL_R   = XK_Control_R,
        VK_SHIFT_L  = XK_Shift_L,
        VK_SHIFT_R  = XK_Shift_R,
        VK_META_L   = XK_Super_L,
        VK_META_R   = XK_Super_R,

        VK_HOME     = XK_Home,
        VK_BEGIN    = XK_Begin,
        VK_END      = XK_End,
        VK_PAGE_UP  = XK_Page_Up,
        VK_PAGE_DOWN= XK_Page_Down,
        VK_PRIOR    = XK_Prior,
        VK_NEXT     = XK_Next,
        VK_DELETE   = XK_Delete,

        VK_ENTER    = XK_Return,
        VK_BACKSPACE= XK_BackSpace,

        VK_F1       = XK_F1,
        VK_F2       = XK_F2,
        VK_F3       = XK_F3,
        VK_F4       = XK_F4,
        VK_F5       = XK_F5,
        VK_F6       = XK_F6,
        VK_F7       = XK_F7,
        VK_F8       = XK_F8,
        VK_F9       = XK_F9,
        VK_F10      = XK_F10,
        VK_F11      = XK_F11,
        VK_F12      = XK_F12,
    }
}

// ----

// Misc types -----------------------------------------------------------------+
alias Event = void delegate(Object notifier);

/**
 * Interface for an object able to manage design-time features.
 */
interface Designer
{
    /// Sets or gets the design time status.
    void design(bool value);

    /// ditto
    bool design();
}

/// Enumerates the possible state a control can have.
enum ControlState: ubyte
{
    checked,        /// checkboxes, menu items
    disabled,       /// any
    down,           /// buttons
    selected,       /// items
    multiSelected,  /// items
}

/// Packs the state in a set.
alias ControlStates = EnumSet!(ControlState, Set8);

/// Event use to notify that a state has changed.
alias StateChangeEvent = void delegate(Object, bool);

// ----

