/**
 * Colors
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.colors;

import
    std.bitmanip, std.conv, std.math;
import
    iz.sugar, iz.containers, iz.memory, iz.enumset;
import
    kheops.types;


/**
 * Returns the html representation of a color.
 */
string toHtml(uint color) @safe
{
    version(LittleEndian) color = color.swapEndian;
    return "#" ~ to!string(color, 16);
}

/**
 * Enumeration of named colors with an uint value.
 */
enum ColorConstant: uint
{
    aliceblue            = 0xFFF0F8FF,
    antiquewhite         = 0xFFFAEBD7,
    aqua                 = 0xFF00FFFF,
    aquamarine           = 0xFF7FFFD4,
    azure                = 0xFFF0FFFF,
    beige                = 0xFFF5F5DC,
    bisque               = 0xFFFFE4C4,
    black                = 0xFF000000,
    blanchedalmond       = 0xFFFFEBCD,
    blue                 = 0xFF0000FF,
    blueviolet           = 0xFF8A2BE2,
    brown                = 0xFFA52A2A,
    burlywood            = 0xFFDEB887,
    cadetblue            = 0xFF5F9EA0,
    chartreuse           = 0xFF7FFF00,
    chocolate            = 0xFFD2691E,
    coral                = 0xFFFF7F50,
    cornflowerblue       = 0xFF6495ED,
    cornsilk             = 0xFFFFF8DC,
    crimson              = 0xFFDC143C,
    cyan                 = 0xFF00FFFF,
    darkblue             = 0xFF00008B,
    darkcyan             = 0xFF008B8B,
    darkgoldenrod        = 0xFFB8860B,
    darkgray             = 0xFFA9A9A9,
    darkgreen            = 0xFF006400,
    darkkhaki            = 0xFFBDB76B,
    darkmagenta          = 0xFF8B008B,
    darkolivegreen       = 0xFF556B2F,
    darkorange           = 0xFFFF8C00,
    darkorchid           = 0xFF9932CC,
    darkred              = 0xFF8B0000,
    darksalmon           = 0xFFE9967A,
    darkseagreen         = 0xFF8FBC8F,
    darkslateblue        = 0xFF483D8B,
    darkslategray        = 0xFF2F4F4F,
    darkturquoise        = 0xFF00CED1,
    darkviolet           = 0xFF9400D3,
    deeppink             = 0xFFFF1493,
    deepskyblue          = 0xFF00BFFF,
    dimgray              = 0xFF696969,
    dodgerblue           = 0xFF1E90FF,
    firebrick            = 0xFFB22222,
    floralwhite          = 0xFFFFFAF0,
    forestgreen          = 0xFF228B22,
    fuchsia              = 0xFFFF00FF,
    gainsboro            = 0xFFDCDCDC,
    ghostwhite           = 0xFFF8F8FF,
    gold                 = 0xFFFFD700,
    goldenrod            = 0xFFDAA520,
    gray                 = 0xFF808080,
    green                = 0xFF008000,
    greenyellow          = 0xFFADFF2F,
    honeydew             = 0xFFF0FFF0,
    hotpink              = 0xFFFF69B4,
    indianred            = 0xFFCD5C5C,
    indigo               = 0xFF4B0082,
    ivory                = 0xFFFFFFF0,
    khaki                = 0xFFF0E68C,
    lavender             = 0xFFE6E6FA,
    lavenderblush        = 0xFFFFF0F5,
    lawngreen            = 0xFF7CFC00,
    lemonchiffon         = 0xFFFFFACD,
    lightblue            = 0xFFADD8E6,
    lightcoral           = 0xFFF08080,
    lightcyan            = 0xFFE0FFFF,
    lightgoldenrodyellow = 0xFFFAFAD2,
    lightgray            = 0xFFD3D3D3,
    lightgreen           = 0xFF90EE90,
    lightpink            = 0xFFFFB6C1,
    lightsalmon          = 0xFFFFA07A,
    lightseagreen        = 0xFF20B2AA,
    lightskyblue         = 0xFF87CEFA,
    lightslategray       = 0xFF778899,
    lightsteelblue       = 0xFFB0C4DE,
    lightyellow          = 0xFFFFFFE0,
    lime                 = 0xFF00FF00,
    limegreen            = 0xFF32CD32,
    linen                = 0xFFFAF0E6,
    magenta              = 0xFFFF00FF,
    maroon               = 0xFF800000,
    mediumaquamarine     = 0xFF66CDAA,
    mediumblue           = 0xFF0000CD,
    mediumorchid         = 0xFFBA55D3,
    mediumpurple         = 0xFF9370DB,
    mediumseagreen       = 0xFF3CB371,
    mediumslateblue      = 0xFF7B68EE,
    mediumspringgreen    = 0xFF00FA9A,
    mediumturquoise      = 0xFF48D1CC,
    mediumvioletred      = 0xFFC71585,
    midnightblue         = 0xFF191970,
    mintcream            = 0xFFF5FFFA,
    mistyrose            = 0xFFFFE4E1,
    moccasin             = 0xFFFFE4B5,
    navajowhite          = 0xFFFFDEAD,
    navy                 = 0xFF000080,
    oldlace              = 0xFFFDF5E6,
    olive                = 0xFF808000,
    olivedrab            = 0xFF6B8E23,
    orange               = 0xFFFFA500,
    orangered            = 0xFFFF4500,
    orchid               = 0xFFDA70D6,
    palegoldenrod        = 0xFFEEE8AA,
    palegreen            = 0xFF98FB98,
    paleturquoise        = 0xFFAFEEEE,
    palevioletred        = 0xFFDB7093,
    papayawhip           = 0xFFFFEFD5,
    peachpuff            = 0xFFFFDAB9,
    peru                 = 0xFFCD853F,
    pink                 = 0xFFFFC0CB,
    plum                 = 0xFFDDA0DD,
    powderblue           = 0xFFB0E0E6,
    purple               = 0xFF800080,
    rebeccapurple        = 0xFF663399,
    red                  = 0xFFFF0000,
    rosybrown            = 0xFFBC8F8F,
    royalblue            = 0xFF4169E1,
    saddlebrown          = 0xFF8B4513,
    salmon               = 0xFFFA8072,
    sandybrown           = 0xFFF4A460,
    seagreen             = 0xFF2E8B57,
    seashell             = 0xFFFFF5EE,
    sienna               = 0xFFA0522D,
    silver               = 0xFFC0C0C0,
    skyblue              = 0xFF87CEEB,
    slateblue            = 0xFF6A5ACD,
    slategray            = 0xFF708090,
    snow                 = 0xFFFFFAFA,
    springgreen          = 0xFF00FF7F,
    steelblue            = 0xFF4682B4,
    tan                  = 0xFFD2B48C,
    teal                 = 0xFF008080,
    thistle              = 0xFFD8BFD8,
    tomato               = 0xFFFF6347,
    turquoise            = 0xFF40E0D0,
    violet               = 0xFFEE82EE,
    wheat                = 0xFFF5DEB3,
    white                = 0xFFFFFFFF,
    whitesmoke           = 0xFFF5F5F5,
    yellow               = 0xFFFFFF00,
    yellowgreen          = 0xFF9ACD32,
}

/**
 * Enumeration of named colors whose value is a string
 * with the html representation.
 */
static struct ColorHtml
{
    static immutable aliceblue = ColorConstant.aliceblue.toHtml;
    static immutable antiquewhite = ColorConstant.antiquewhite.toHtml;
    static immutable aqua = ColorConstant.aqua.toHtml;
    static immutable aquamarine = ColorConstant.aquamarine.toHtml;
    static immutable azure = ColorConstant.azure.toHtml;
    static immutable beige = ColorConstant.beige.toHtml;
    static immutable bisque = ColorConstant.bisque.toHtml;
    static immutable black = ColorConstant.black.toHtml;
    static immutable blanchedalmond = ColorConstant.blanchedalmond.toHtml;
    static immutable blue = ColorConstant.blue.toHtml;
    static immutable blueviolet = ColorConstant.blueviolet.toHtml;
    static immutable brown = ColorConstant.brown.toHtml;
    static immutable burlywood = ColorConstant.burlywood.toHtml;
    static immutable cadetblue = ColorConstant.cadetblue.toHtml;
    static immutable chartreuse = ColorConstant.chartreuse.toHtml;
    static immutable chocolate = ColorConstant.chocolate.toHtml;
    static immutable coral = ColorConstant.coral.toHtml;
    static immutable cornflowerblue = ColorConstant.cornflowerblue.toHtml;
    static immutable cornsilk = ColorConstant.cornsilk.toHtml;
    static immutable crimson = ColorConstant.crimson.toHtml;
    static immutable cyan = ColorConstant.cyan.toHtml;
    static immutable darkblue = ColorConstant.darkblue.toHtml;
    static immutable darkcyan = ColorConstant.darkcyan.toHtml;
    static immutable darkgoldenrod = ColorConstant.darkgoldenrod.toHtml;
    static immutable darkgray = ColorConstant.darkgray.toHtml;
    static immutable darkgreen = ColorConstant.darkgreen.toHtml;
    static immutable darkkhaki = ColorConstant.darkkhaki.toHtml;
    static immutable darkmagenta = ColorConstant.darkmagenta.toHtml;
    static immutable darkolivegreen = ColorConstant.darkolivegreen.toHtml;
    static immutable darkorange = ColorConstant.darkorange.toHtml;
    static immutable darkorchid = ColorConstant.darkorchid.toHtml;
    static immutable darkred = ColorConstant.darkred.toHtml;
    static immutable darksalmon = ColorConstant.darksalmon.toHtml;
    static immutable darkseagreen = ColorConstant.darkseagreen.toHtml;
    static immutable darkslateblue = ColorConstant.darkslateblue.toHtml;
    static immutable darkslategray = ColorConstant.darkslategray.toHtml;
    static immutable darkturquoise = ColorConstant.darkturquoise.toHtml;
    static immutable darkviolet = ColorConstant.darkviolet.toHtml;
    static immutable deeppink = ColorConstant.deeppink.toHtml;
    static immutable deepskyblue = ColorConstant.deepskyblue.toHtml;
    static immutable dimgray = ColorConstant.dimgray.toHtml;
    static immutable dodgerblue = ColorConstant.dodgerblue.toHtml;
    static immutable firebrick = ColorConstant.firebrick.toHtml;
    static immutable floralwhite = ColorConstant.floralwhite.toHtml;
    static immutable forestgreen = ColorConstant.forestgreen.toHtml;
    static immutable fuchsia = ColorConstant.fuchsia.toHtml;
    static immutable gainsboro = ColorConstant.gainsboro.toHtml;
    static immutable ghostwhite = ColorConstant.ghostwhite.toHtml;
    static immutable gold = ColorConstant.gold.toHtml;
    static immutable goldenrod = ColorConstant.goldenrod.toHtml;
    static immutable gray = ColorConstant.gray.toHtml;
    static immutable green = ColorConstant.green.toHtml;
    static immutable greenyellow = ColorConstant.greenyellow.toHtml;
    static immutable honeydew = ColorConstant.honeydew.toHtml;
    static immutable hotpink = ColorConstant.hotpink.toHtml;
    static immutable indianred = ColorConstant.indianred.toHtml;
    static immutable indigo = ColorConstant.indigo.toHtml;
    static immutable ivory = ColorConstant.ivory.toHtml;
    static immutable khaki = ColorConstant.khaki.toHtml;
    static immutable lavender = ColorConstant.lavender.toHtml;
    static immutable lavenderblush = ColorConstant.lavenderblush.toHtml;
    static immutable lawngreen = ColorConstant.lawngreen.toHtml;
    static immutable lemonchiffon = ColorConstant.lemonchiffon.toHtml;
    static immutable lightblue = ColorConstant.lightblue.toHtml;
    static immutable lightcoral = ColorConstant.lightcoral.toHtml;
    static immutable lightcyan = ColorConstant.lightcyan.toHtml;
    static immutable lightgoldenrodyellow = ColorConstant.lightgoldenrodyellow.toHtml;
    static immutable lightgray = ColorConstant.lightgray.toHtml;
    static immutable lightgreen = ColorConstant.lightgreen.toHtml;
    static immutable lightpink = ColorConstant.lightpink.toHtml;
    static immutable lightsalmon = ColorConstant.lightsalmon.toHtml;
    static immutable lightseagreen = ColorConstant.lightseagreen.toHtml;
    static immutable lightskyblue = ColorConstant.lightskyblue.toHtml;
    static immutable lightslategray = ColorConstant.lightslategray.toHtml;
    static immutable lightsteelblue = ColorConstant.lightsteelblue.toHtml;
    static immutable lightyellow = ColorConstant.lightyellow.toHtml;
    static immutable lime = ColorConstant.lime.toHtml;
    static immutable limegreen = ColorConstant.limegreen.toHtml;
    static immutable linen = ColorConstant.linen.toHtml;
    static immutable magenta = ColorConstant.magenta.toHtml;
    static immutable maroon = ColorConstant.maroon.toHtml;
    static immutable mediumaquamarine = ColorConstant.mediumaquamarine.toHtml;
    static immutable mediumblue = ColorConstant.mediumblue.toHtml;
    static immutable mediumorchid = ColorConstant.mediumorchid.toHtml;
    static immutable mediumpurple = ColorConstant.mediumpurple.toHtml;
    static immutable mediumseagreen = ColorConstant.mediumseagreen.toHtml;
    static immutable mediumslateblue = ColorConstant.mediumslateblue.toHtml;
    static immutable mediumspringgreen = ColorConstant.mediumspringgreen.toHtml;
    static immutable mediumturquoise = ColorConstant.mediumturquoise.toHtml;
    static immutable mediumvioletred = ColorConstant.mediumvioletred.toHtml;
    static immutable midnightblue = ColorConstant.midnightblue.toHtml;
    static immutable mintcream = ColorConstant.mintcream.toHtml;
    static immutable mistyrose = ColorConstant.mistyrose.toHtml;
    static immutable moccasin = ColorConstant.moccasin.toHtml;
    static immutable navajowhite = ColorConstant.navajowhite.toHtml;
    static immutable navy = ColorConstant.navy.toHtml;
    static immutable oldlace = ColorConstant.oldlace.toHtml;
    static immutable olive = ColorConstant.olive.toHtml;
    static immutable olivedrab = ColorConstant.olivedrab.toHtml;
    static immutable orange = ColorConstant.orange.toHtml;
    static immutable orangered = ColorConstant.orangered.toHtml;
    static immutable orchid = ColorConstant.orchid.toHtml;
    static immutable palegoldenrod = ColorConstant.palegoldenrod.toHtml;
    static immutable palegreen = ColorConstant.palegreen.toHtml;
    static immutable paleturquoise = ColorConstant.paleturquoise.toHtml;
    static immutable palevioletred = ColorConstant.palevioletred.toHtml;
    static immutable papayawhip = ColorConstant.papayawhip.toHtml;
    static immutable peachpuff = ColorConstant.peachpuff.toHtml;
    static immutable peru = ColorConstant.peru.toHtml;
    static immutable pink = ColorConstant.pink.toHtml;
    static immutable plum = ColorConstant.plum.toHtml;
    static immutable powderblue = ColorConstant.powderblue.toHtml;
    static immutable purple = ColorConstant.purple.toHtml;
    static immutable rebeccapurple = ColorConstant.rebeccapurple.toHtml;
    static immutable red = ColorConstant.red.toHtml;
    static immutable rosybrown = ColorConstant.rosybrown.toHtml;
    static immutable royalblue = ColorConstant.royalblue.toHtml;
    static immutable saddlebrown = ColorConstant.saddlebrown.toHtml;
    static immutable salmon = ColorConstant.salmon.toHtml;
    static immutable sandybrown = ColorConstant.sandybrown.toHtml;
    static immutable seagreen = ColorConstant.seagreen.toHtml;
    static immutable seashell = ColorConstant.seashell.toHtml;
    static immutable sienna = ColorConstant.sienna.toHtml;
    static immutable silver = ColorConstant.silver.toHtml;
    static immutable skyblue = ColorConstant.skyblue.toHtml;
    static immutable slateblue = ColorConstant.slateblue.toHtml;
    static immutable slategray = ColorConstant.slategray.toHtml;
    static immutable snow = ColorConstant.snow.toHtml;
    static immutable springgreen = ColorConstant.springgreen.toHtml;
    static immutable steelblue = ColorConstant.steelblue.toHtml;
    static immutable tan = ColorConstant.tan.toHtml;
    static immutable teal = ColorConstant.teal.toHtml;
    static immutable thistle = ColorConstant.thistle.toHtml;
    static immutable tomato = ColorConstant.tomato.toHtml;
    static immutable turquoise = ColorConstant.turquoise.toHtml;
    static immutable violet = ColorConstant.violet.toHtml;
    static immutable wheat = ColorConstant.wheat.toHtml;
    static immutable white = ColorConstant.white.toHtml;
    static immutable whitesmoke = ColorConstant.whitesmoke.toHtml;
    static immutable yellow = ColorConstant.yellow.toHtml;
    static immutable yellowgreen = ColorConstant.yellowgreen.toHtml;
}

__gshared enum defaultColor = ColorConstant.gray;
__gshared enum defaultFillColor = ColorConstant.gray;
__gshared enum defaultStrokeColor = ColorConstant.black;


/**
 * The struct Rgba represents a color as R, G, B, and A components.
 *
 * Internally it caches and keeps in sync each representation,
 * either as uint, float components or ubytes components.
 */
struct Rgba
{

private: @safe:

    enum b2f = 1.0f / ubyte.max;

    union Color
    {
        struct Component{ubyte b, g, r, a;}
        uint all;
        Component component;
    }

    Rgba.Color _color = Color(defaultColor);

    float bf = (0xFF & defaultColor) * b2f;
    float gf = ((0x0000FF00 & defaultColor) >> 8) * b2f;
    float rf = ((0x00FF0000 & defaultColor) >> 16) * b2f;
    float af = ((0xFF000000 & defaultColor) >> 24) * b2f;

    void updateFpValue(char c)()
    {
        static if (c == 'x' || c == 'r') rf = _color.component.r * b2f;
        static if (c == 'x' || c == 'g') gf = _color.component.g * b2f;
        static if (c == 'x' || c == 'b') bf = _color.component.b * b2f;
        static if (c == 'x' || c == 'a') af = _color.component.a * b2f;
    }

public: @safe:

    /**
     * Constructs from a ColorConstant.
     */
    this(ColorConstant value)
    {
        opAssign(value);
    }

    /**
     * Constructs from each component as ubyte.
     */
    this(ubyte r, ubyte g, ubyte b, ubyte a)
    {
        _color.component.r = r;
        _color.component.g = g;
        _color.component.b = b;
        _color.component.a = a;
        updateFpValue!'x';
    }

    /**
     * Constructs from an uint.
     */
    this(uint value)
    {
        opAssign(value);
    }

    /**
     * Constructs from an html color.
     */
    this(string s)
    {
        bool error = true;
        if (s.length == 9 && s[0] == '#')
        {
            error = false;
            uint u = void;
            try u = to!uint(s[1..$], 16);
            catch(ConvException e) error = true;
            if (!error)
            {
                version(LittleEndian) u = u.swapEndian;
                _color.all = u;
                updateFpValue!'x';
            }
        }
        if (error) throw
            new Exception("Invalid string to Rgba convertion: " ~ s);
    }

    /**
     *  Returns the color with the html representation.
     */
    string toString() const
    {
        return _color.all.toHtml;
    }

    /**
     * Returns the color as an uint.
     */
    uint opCast() const
    {
        return _color.all;
    }

    /**
     * Sets the color from an html color.
     */
    void opAssign(string s)
    {
        this = Rgba(s);
    }

    /**
     * Sets the color from an uint or a ColorConstant.
     */
    void opAssign(uint u)
    {
        _color.all = u;
        updateFpValue!'x';
    }

    /**
     * Tests if the color is equal to another Rga, a uint or an html color.
     */
    bool opEquals(R)(const ref R rhs) const
    {
        import std.traits: isImplicitlyConvertible, isSomeString;
        static if (isImplicitlyConvertible!(R, uint))
            return _color.all == rhs;
        else static if (is(R == Rgba))
            return _color.all == rhs._color.all;
        else static if (isSomeString!R)
            return rhs == toString;
        else
            static assert(0, "unsupported argument passed to Rgba.opEquals: "
                ~ R.stringof);
    }

    ///
    size_t toHash() const nothrow @safe
    {
        return _color.all;
    }

    /**
     * Sets or gets the color as an uint.
     */
    uint color(){return _color.all;}
    /// ditto
    void color(uint value){this = Rgba(value);}

    /**
     * Returns the red component as a float.
     */
    float r_fp(){return rf;}

    /**
     * Sets or gets the red component as a ubyte.
     */
    ubyte r(){return _color.component.r;}
    /// ditto
    void r(ubyte value)
    {_color.component.r = value; updateFpValue!'r';}

    /**
     * Returns the green component as a float.
     */
    float g_fp(){return gf;}

    /**
     * Sets or gets the green component as a ubyte.
     */
    ubyte g(){return _color.component.g;}
    /// ditto
    void  g(ubyte value)
    {_color.component.g = value; updateFpValue!'g';}

    /**
     * Returns the blue component as a float.
     */
    float b_fp(){return bf;}

    /**
     * Sets or gets the blue component as a ubyte.
     */
    ubyte b(){return _color.component.b;}
    /// ditto
    void  b(ubyte value)
    {_color.component.b = value; updateFpValue!'b';}

    /**
     * Returns the alpha component as a float.
     */
    float a_fp(){return af;}

    /**
     * Sets or gets the alpha component as a ubyte.
     */
    ubyte a(){return _color.component.a;}
    /// ditto
    void  a(ubyte value)
    {_color.component.a = value; updateFpValue!'a';}

    /**
     * Returns the color as a tuple of float.
     *
     * This can be used as an optimization when all the components as float
     * has to be used in a function call, using myColor.rgbaList[0..$].
     */
    auto rgbaList()
    {
        import std.typecons: tuple;
        return tuple(rf, gf, bf, af);
    }
}

unittest
{
    Rgba c0 = Rgba(ColorConstant.gray);
    assert(c0.a == 0xFF);
    assert(approxEqual(c0.a_fp, 1.0f));
    assert(c0.r == 0x80);
    assert(approxEqual(c0.r_fp, 0.5f));

    Rgba c1 = Rgba(0xAABBCCFF);
    assert(c1.r == 0xBB);
    assert(c1.g == 0xCC);
    assert(c1.b == 0xFF);
    assert(c1.a == 0xAA);

    Rgba c2 = Rgba(c1.toString);
    assert(c2 == c1);
    assert(cast(uint) c2 == 0xAABBCCFF);

    Rgba c3 = Rgba("#FFEEDDCC");
    assert(c3.r == 0xDD);
    assert(c3.g == 0xEE);
    assert(c3.b == 0xFF);
    assert(c3.a == 0xCC);

    auto a1 = &c3;
    c3.color = ColorConstant.aliceblue;
    assert( &c3 == a1);
    c3 = Rgba(ColorConstant.aliceblue);
    assert( &c3 == a1);

    Rgba c4 = defaultColor;
    Rgba c5 = defaultColor;
    assert(c4.toString == c5.toString);
    assert(c4.r_fp.approxEqual(c5.r_fp));
    assert(c4.g_fp.approxEqual(c5.g_fp));
    assert(c4.b_fp.approxEqual(c5.b_fp));
    assert(c4.a_fp.approxEqual(c5.a_fp));

    assert(Rgba(ColorConstant.green) == ColorHtml.green);
}

/// Describes a color.
enum ColorProperty
{
    dark,
    clear,
    warm,
    cold,
}

/// Set of ColorProperty.
alias ColorProperties = EnumSet!(ColorProperty, Set8);

/**
 * Randomizes a color.
 *
 * Params:
 *      props = A set of ColorProperty.
 *
 * Returns:
 *      A randomized color but conform with the properties.
 */
uint randomizedColor(T : ColorProperties)(const auto ref T props) @safe
{
    import std.random: uniform;

    ubyte[2] minmaxR;
    ubyte[2] minmaxG;
    ubyte[2] minmaxB;

    with (ColorProperty)
    {
        minmaxR[0] = 0x00;
        minmaxG[0] = 0x00;
        minmaxB[0] = 0x00;
        minmaxR[1] = 0xFF;
        minmaxG[1] = 0xFF;
        minmaxB[1] = 0xFF;

        if (dark in props)
        {
            minmaxR[0] = 0x20;
            minmaxG[0] = 0x20;
            minmaxB[0] = 0x20;
            minmaxR[1] = 0x7F;
            minmaxG[1] = 0x7F;
            minmaxB[1] = 0x7F;
        }
        else if (clear in props)
        {
            minmaxR[0] = 0x7F;
            minmaxG[0] = 0x7F;
            minmaxB[0] = 0x7F;
            minmaxR[1] = 0xFF;
            minmaxG[1] = 0xFF;
            minmaxB[1] = 0xFF;
        }

        if (cold in props)
        {
            minmaxR[1] -= 0x30;
            minmaxB[0] += 0x30;
        }
        else if (warm in props)
        {
            minmaxR[0] += 0x40;
            minmaxB[1] -= 0x40;
        }
    }

    uint result = 0xFF000000;
    result += uniform(minmaxB[0], minmaxB[1]);
    result += uniform(minmaxG[0], minmaxG[1]) << 8;
    result += uniform(minmaxR[0], minmaxR[1]) << 16;

    return result;
}

alias gradientRange = GradientRange;

/**
 * Iterator allowing to morph from a color to another.
 */
struct GradientRange
{
private:

    immutable size_t _steps;
    immutable ubyte[2] _rRange;
    immutable ubyte[2] _gRange;
    immutable ubyte[2] _bRange;
    immutable ubyte[2] _aRange;
    immutable float _aInc;
    immutable float _rInc;
    immutable float _gInc;
    immutable float _bInc;
    ubyte[4] _front;
    size_t _step = -1;

public:

    /**
     * Constructs the range.
     *
     * Params:
     *      rRange = The first and last value of the red channel.
     *      gRange = The first and last value of the green channel.
     *      bRange = The first and last value of the blue channel.
     *      aRange = The first and last value of the alpha channel, by default
     *          always fully opaque.
     */
    this(size_t steps, ubyte[2] rRange, ubyte[2] gRange, ubyte[2]bRange,
        ubyte[2] aRange = [0xFF,0xFF]) pure nothrow @safe @nogc
    {
        _steps = steps;
        _aRange = aRange;
        _rRange = rRange;
        _gRange = gRange;
        _bRange = bRange;
        _rInc = float(_rRange[1] - _rRange[0]) / _steps;
        _gInc = float(_gRange[1] - _gRange[0]) / _steps;
        _bInc = float(_bRange[1] - _bRange[0]) / _steps;
        _aInc = float(_aRange[1] - _aRange[0]) / _steps;
        popFront();
    }

    /// InputRange primitive
    void popFront() pure nothrow @safe @nogc
    {
        ++_step;
        _front[3] = cast(ubyte) (_aRange[0] + cast(int)(_aInc * _step));
        _front[2] = cast(ubyte) (_rRange[0] + cast(int)(_rInc * _step));
        _front[1] = cast(ubyte) (_gRange[0] + cast(int)(_gInc * _step));
        _front[0] = cast(ubyte) (_bRange[0] + cast(int)(_bInc * _step));
    }

    /// InputRange primitive
    uint front() const pure nothrow @trusted @nogc
    {
        return *cast(uint*) _front.ptr;
    }

    /// InputRange primitive
    bool empty() const pure nothrow @safe @nogc
    {
        return _step == _steps;
    }

    ///
    size_t opDollar() const pure nothrow @safe @nogc
    {
        return _steps + 1;
    }

    ///
    alias length = opDollar;

    ///
    uint opIndex(size_t index) const pure nothrow @trusted @nogc
    {
        ubyte[4] result;
        result[3] = cast(ubyte) (_aRange[0] + cast(int)(_aInc * index));
        result[2] = cast(ubyte) (_rRange[0] + cast(int)(_rInc * index));
        result[1] = cast(ubyte) (_gRange[0] + cast(int)(_gInc * index));
        result[0] = cast(ubyte) (_bRange[0] + cast(int)(_bInc * index));
        return *cast(uint*) result.ptr;
    }
}
///
@safe @nogc pure nothrow unittest
{
    import std.range : popFrontN;

    auto r = gradientRange(12, [0x30,0xFF], [0x20,0xFF], [0x10,0xFF], [0xFF,0xFF]);
    assert(r.length == 13);
    assert(r.front() == 0xFF302010);
    r.popFrontN(12);
    assert(r.front() == 0xFFFFFFFF);
    assert(r[$-1] == 0xFFFFFFFF);
}

@safe @nogc pure nothrow unittest
{
    import std.range : popFrontN;

    auto r = gradientRange(24, [0x30,0xFF], [0xFF,0x20], [0xCC,0xAA], [0xFF,0xFF]);
    assert(r.front() == 0xFF30FFCC);
    r.popFrontN(24);
    assert(r.empty);
    assert(r.front() == 0xFFFF20AA);
}

unittest
{
    import iz.memory: MustAddGcRange;
    static assert(!MustAddGcRange!Rgba);
}

