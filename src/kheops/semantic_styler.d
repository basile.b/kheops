/**
 * Semantic styler.
 * Implements a styling system based on the publications name.
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */

module kheops.semantic_styler;

import
    std.stdio;
import
    iz.memory, iz.properties;
import
    kheops.control, kheops.types, kheops.primitives, kheops.colors;

class SemanticStyler: Control, Stylist
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    @NoGc RectangleEn _background;
    @NoGc RectangleEn _foreground;
    @NoGc RectangleEn _backgroundFrame;
    @NoGc RectangleEn _foregroundFrame;
    @NoGc RectangleEn _backgroundFrameMask;
    @NoGc TextEn      _text;

public:

    this()
    {
        super();
        _background = addControl!RectangleEn(Rect(0,0,1,40), Alignment.top);
        _foreground = addControl!RectangleEn(Rect(0,0,1,40), Alignment.top);
        _backgroundFrame = addControl!RectangleEn(Rect(0,0,1,40), Alignment.top);
        _foregroundFrame = addControl!RectangleEn(Rect(0,0,1,40), Alignment.top);
        _backgroundFrameMask = addControl!RectangleEn(Rect(0,0,1,40), Alignment.top);
        _text = addControl!TextEn(Rect(0,0,1,40), Alignment.top);
        collectPublications!SemanticStyler();
    }

    void applyStyleFrom(Control value)
    {
        value.beginRealign;
        iterateTreeItems!(applicator)(value, this);
        value.endRealign;
        value.repaint;
    }

    /// Sets or gets the rectangle used as model for the "background" properties
    @Set void background(RectangleEn) {}
    /// ditto
    @Get RectangleEn background() {return _background;}

    /// Sets or gets the rectangle used as model for the "foreground" properties
    @Set void foreground(RectangleEn) {}
    /// ditto
    @Get RectangleEn foreground() {return _foreground;}

    /// Sets or gets the rectangle used as model for the "backgroundFrame" properties
    @Set void backgroundFrame(RectangleEn) {}
    /// ditto
    @Get RectangleEn backgroundFrame() {return _backgroundFrame;}

    /// Sets or gets the rectangle used as model for the "foregroundFrame" properties
    @Set void foregroundFrame(RectangleEn) {}
    /// ditto
    @Get RectangleEn foregroundFrame() {return _foregroundFrame;}

    /// Sets or gets the rectangle used as model for the "backgroundFrameMask" properties
    @Set void backgroundFrameMask(RectangleEn) {}
    /// ditto
    @Get RectangleEn backgroundFrameMask() {return _backgroundFrameMask;}

    /// Sets or gets the rectangle used as model for the "text" properties
    @Set void text(TextEn) {}
    /// ditto
    @Get TextEn text() {return _text;}
}

private enum applicator = (CustomControl cc, SemanticStyler styler)
{
    if (Stylized s = cast(Stylized) cc)
    {
        s.beginStyling;
        bindElement(styler.background, cc, "background");
        bindElement(styler.foreground, cc, "foreground");
        bindElement(styler.backgroundFrame, cc, "backgroundFrame");
        bindElement(styler.foregroundFrame, cc, "foregroundFrame");
        bindElement(styler.backgroundFrameMask, cc, "backgroundFrameMaks");
        bindElement(styler.text, cc, "text");
        s.endStyling;
    }
};

private void bindElement(CustomControl source, CustomControl target, string name)
{
    if (void* p = target.publicationFromName(name))
    {
        PropDescriptor!Object* d = cast(PropDescriptor!Object*) p;
        if (Object o = d.get())
            bindPublications!true(source, o);
    }
}

