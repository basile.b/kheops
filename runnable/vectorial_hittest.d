#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "vectorial_hittest"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module vectorial_hittest;

import
    core.thread, std.stdio;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.pen, kheops.primitives, kheops.layouts,
    kheops.helpers.polygons, kheops.pathdata;

OsWindow win;

struct Handler
{
    void handleDown(Object notifier, double x, double y,
        const ref MouseButtons mb, const ref KeyModifiers km)
    {
        if (Primitive p = cast(Primitive) notifier)
        {
            p.fill.color = ColorConstant.bisque - 0x202020;
            p.toFront;
            win.repaint;
        }
    }

    void handleUp(Object notifier, double x, double y,
        const ref MouseButtons mb, const ref KeyModifiers km)
    {
        if (Primitive p = cast(Primitive) notifier)
        {
            p.fill.color = ColorConstant.orange;
            win.repaint;
        }
    }

    void handleEnter(Object notifier)
    {
        if (Primitive p = cast(Primitive) notifier)
        {
            p.fill.color = ColorConstant.orange;
            win.repaint;
        }
    }

    void handleLeave(Object notifier)
    {
        if (Primitive p = cast(Primitive) notifier)
        {
            p.fill.color = ColorConstant.bisque;
            win.repaint;
        }
    }
}

void main()
{
    win = construct!OsWindow;
    win.caption = "Test for inside-ness in complex and transformed shapes + design-time features";

    win.beginRealign;
    win.left   = 1;
    win.top    = 1;
    win.width  = 610;
    win.height = 410;

    Handler h;

    Circle circ = win.addControl!Circle(Rect(10,10,180,180));
    circ.fill.color = ColorConstant.bisque;
    circ.radius = 90;
    circ.wantMouse = true;
    circ.focusable = true;
    circ.vectorialHitTest = true;
    circ.onMouseDown = &h.handleDown;
    circ.onMouseUp = &h.handleUp;
    circ.onMouseEnter = &h.handleEnter;
    circ.onMouseLeave = &h.handleLeave;

    Rectangle rec = win.addControl!Rectangle(Rect(220,20,160,180));
    rec.fill.color = ColorConstant.bisque;
    rec.wantMouse = true;
    rec.focusable = true;
    rec.rotationAngle = -80.5;
    rec.clipChildren = true;

    Rectangle rec1 = rec.addControl!Rectangle(Rect(40,40,60,60));
    rec1.fill.color = ColorConstant.bisque;
    rec1.wantMouse = true;
    rec1.focusable = true;
    rec1.onMouseDown = &h.handleDown;
    rec1.onMouseUp = &h.handleUp;
    rec1.rotationAngle = -22.5;
    rec1.onMouseEnter = &h.handleEnter;
    rec1.onMouseLeave = &h.handleLeave;

    Polygon poly1 = win.addControl!Polygon(Rect(400,10,180,180));
    poly1.fill.color = ColorConstant.bisque;
    poly1.wantMouse = true;
    poly1.focusable = true;
    poly1.vectorialHitTest = true;
    poly1.onMouseDown = &h.handleDown;
    poly1.onMouseUp = &h.handleUp;
    poly1.points = star(Rect(0,0,180,180), 7 , 20);
    poly1.rotationAngle = 33;
    poly1.onMouseEnter = &h.handleEnter;
    poly1.onMouseLeave = &h.handleLeave;

    Polygon poly2 = win.addControl!Polygon(Rect(10,200,180,180));
    poly2.fill.color = ColorConstant.bisque;
    poly2.wantMouse = true;
    poly2.focusable = true;
    poly2.vectorialHitTest = true;
    poly2.onMouseDown = &h.handleDown;
    poly2.onMouseUp = &h.handleUp;
    poly2.points = randomizePolygon(Rect(0,0,180,180), 12, 0.7);
    poly2.polygonPosition = PathAlignment.center;
    poly2.onMouseEnter = &h.handleEnter;
    poly2.onMouseLeave = &h.handleLeave;

    Pie pie1 = win.addControl!Pie(Rect(200,200,180,180));
    pie1.radius = 90;
    pie1.angleBegin = 0;
    pie1.angleEnd = 270;
    pie1.kind = EllipseKind.scaled;
    pie1.fill.color = ColorConstant.bisque;
    pie1.wantMouse = true;
    pie1.focusable = true;
    pie1.vectorialHitTest = true;
    pie1.onMouseDown = &h.handleDown;
    pie1.onMouseUp = &h.handleUp;
    pie1.rotationAngle = - 45;
    pie1.onMouseEnter = &h.handleEnter;
    pie1.onMouseLeave = &h.handleLeave;

    Path p = win.addControl!Path(Rect(400,200,100,200));
    p.fill.color = ColorConstant.bisque;
    p.wantMouse = true;
    p.focusable = true;
    p.vectorialHitTest = true;
    p.onMouseDown = &h.handleDown;
    p.onMouseUp = &h.handleUp;
    p.onMouseEnter = &h.handleEnter;
    p.onMouseLeave = &h.handleLeave;
    p.data.loadFromSvgData(
    "M 140.08008 1.296875
     C 118.03272 1.1192767 96.219389 17.223311 95.148438 64.304688
     C 89.0092 -77.36652 -98.185978 78.349535 79.130859 81.130859
     C -85.776935 83.599742 63.416232 240.25329 90.232422 110.92383
     C 86.030487 151.9067 96.058505 168.0274 138.12305 186.80859
     C 142.39964 187.60897 149.48205 187.03688 147.34375 182.00586
     C 91.881565 154.67827 94.791123 148.34554 98.541016 88.146484
     C 86.693575 266.76962 284.06891 67.043413 115.10547 79.564453
     C 228.14328 65.596863 183.66117 1.6479342 140.08008 1.296875 Z"
    );
    foreach(pt; p.data.pointsIterator)
        pt.x += 50;
    p.pathPosition = PathAlignment.fitUnscaled;

    win.endRealign;
    win.design = true;
    kheopsRun;
}

