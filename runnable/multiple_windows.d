#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "multiple_windows"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module multiple_windows;

import
    core.thread, std.stdio;
import
    iz.memory;
import
    kheops.control, kheops.types;

OsWindow win1, win2, win3, win4;

void main(string[] args)
{
    Handler handler;

    win1 = construct!OsWindow;
    win2 = construct!OsWindow;
    win3 = construct!OsWindow;
    win4 = construct!OsWindow;

    win1.caption = "window 1 - log mouse & keys events";
    win2.caption = "window 2 - log mouse & keys events";
    win3.caption = "window 3 - log mouse & keys events";
    win4.caption = "window 4 - log mouse & keys events";

    win1.left   = 40;
    win1.top    = 40;
    win1.width  = 250;
    win1.height = 180;

    win2.left   = 300;
    win2.top    = 40;
    win2.width  = 250;
    win2.height = 180;

    win3.left   = 40;
    win3.top    = 300;
    win3.width  = 250;
    win3.height = 180;

    win4.left   = 300;
    win4.top    = 300;
    win4.width  = 250;
    win4.height = 180;

    void setupEvents(OsWindow win)
    {
        win.onClick = &handler.logCall!(typeof(win.onClick()),"onClick");
        win.onDoubleClick = &handler.logCall!(typeof(win.onDoubleClick()),"onDoubleClick");
        win.onMouseDown = &handler.logCall!(typeof(win.onMouseDown()),"onMouseDown");
        win.onMouseUp = &handler.logCall!(typeof(win.onMouseUp()),"onMouseUp");
        win.onMouseMove = &handler.logCall!(typeof(win.onMouseMove()),"onMouseMove");
        win.onMouseLeave = &handler.logCall!(typeof(win.onMouseLeave()),"onMouseLeave");
        win.onMouseEnter = &handler.logCall!(typeof(win.onMouseEnter()),"onMouseEnter");
        win.onMouseWheel = &handler.logCall!(typeof(win.onMouseWheel()),"onMouseWheel");

        win.onKeyDown = &handler.logCall!(typeof(win.onKeyDown()),"onKeyDown");
        win.onKeyUp = &handler.logCall!(typeof(win.onKeyUp()),"onKeyUp");
        win.onShortcut = &handler.logCall!(typeof(win.onShortcut()),"onShortcut");

        win.onCloseQuery = &handler.logCall!(typeof(win.onCloseQuery()),"onCloseQuery");
    }

    setupEvents(win1);
    setupEvents(win2);
    setupEvents(win3);
    setupEvents(win4);

    kheopsRun;
}

struct Handler
{
    enum sep = " - ";

    import std.traits: Parameters;
    import std.meta: aliasSeqOf;
    import std.range: iota;
    void logCall(A, string fun)(Parameters!A a)
    {
        win1.wantPaste();
        write(fun);
        write(sep);
        enum len = Parameters!A.length;
        foreach(i; aliasSeqOf!(iota(0, len)))
        {
            static if (is(typeof(a[i]) == dchar))
            {
                import std.uni, std.ascii;
                if (!std.ascii.isWhite(a[i]))
                    write(a[i]);
                else
                    write('#', int(a[i]));
            }
            else write(a[i]);
            if (i != len-1)
                write(sep);
        }
        write("\n");
        stdout.flush;
    }
}

