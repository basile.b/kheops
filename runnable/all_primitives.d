#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs
module all_primitives;

import
    std.stdio;
import
    iz.memory, iz.serializer;
import
    kheops.types, kheops.primitives, kheops.colors,
    kheops.canvas, kheops.brush, kheops.control, kheops.controls.menus;

struct Handler
{
    double trgX, trgY;
    bool down;
    Control ctrl;

    void doMouseDown(Object notifier, double x, double y, const ref MouseButtons mbs,
        const ref KeyModifiers km)
    {
        if (MouseButton.left !in mbs || ctrl)
            return;
        ctrl = cast(Control) notifier;
        trgX = x;
        trgY = y;
        down = true;
        ctrl.siblingIndex = ctrl.siblingCount-1;
        ctrl.realign;
        ctrl.repaint;
    }

    void doMouseUp(Object notifier, double x, double y, const ref MouseButtons mbs,
        const ref KeyModifiers km)
    {
        down = false;
        if (!ctrl) return;
        if (notifier is ctrl)
        {
            double newX = ctrl.left + x - trgX;
            double newY = ctrl.top + y - trgY;
            ctrl.position(newX, newY);
            if (cast(TextEn)ctrl)
            {
                fileToPublisher("kprim.txt", ctrl);
                ctrl.repaint;
            }
        }
        ctrl = null;
    }

    void doMouseMove(Object notifier, double x, double y, const ref KeyModifiers km)
    {
        if (!down || !ctrl)
            return;

        Control ctrld = cast(Control) notifier;
        if (ctrld !is ctrl)
            return;

        double newX = ctrl.left + x - trgX;
        double newY = ctrl.top + y - trgY;
        ctrl.position(newX, newY);
        ctrl.repaint;
    }

    void doClick(Object notifier)
    {
        writeln(__PRETTY_FUNCTION__);
    }

    void doDoubleClick(Object notifier)
    {
        writeln(__PRETTY_FUNCTION__);
    }
}


void main(string[] args)
{
    OsWindow win = construct!OsWindow;
    Handler handler;

    MenuItem i;
    MenuWindow ctxt = win.addControl!MenuWindow(Rect(10,10,70,70));
    i = ctxt.addItem("menu item 0"); i.published = true;
    i = ctxt.addItem("menu item 1"); i.published = true;
    i = ctxt.addItem("menu item 2");i.published = true;
    i = ctxt.addItem("menu item 3"); i.published = true;
    i = ctxt.addItem("-"); i.published = true;
    i = ctxt.addItem("menu item 4"); i.published = true;
    i = ctxt.addItem("menu item 5"); i.published = true;

    //assert(i.parent.dynamicPublications.count == 7);

    Rectangle rect1 = win.addControl!Rectangle(Rect(10,10,70,70));
    rect1.fill.color = ColorConstant.beige;
    rect1.onMouseMove = &handler.doMouseMove;
    rect1.wantMouse = true;
    rect1.onMouseDown = &handler.doMouseDown;
    rect1.onMouseUp = &handler.doMouseUp;
    rect1.contextMenu = ctxt;
    rect1.rotationAngle = 180;
    rect1.pen.width = 16;

    RectangleEn rect2 = win.addControl!RectangleEn(Rect(90,10,70,70));
    rect2.radius = 20;
    rect2.cornerAll = CornerKind.line;
    rect2.fill.color = ColorConstant.beige;
    rect2.onMouseMove = &handler.doMouseMove;
    rect2.wantMouse = true;
    rect2.onMouseDown = &handler.doMouseDown;
    rect2.onMouseUp = &handler.doMouseUp;
    rect2.contextMenu = ctxt;
    rect2.pen.width = 8;

    RectangleEn rect3 = win.addControl!RectangleEn(Rect(170,10,70,70));
    rect3.radius = 12;
    rect3.cornerAll = CornerKind.round;
    rect3.fill.color = ColorConstant.beige;
    rect3.onMouseMove = &handler.doMouseMove;
    rect3.wantMouse = true;
    rect3.onMouseDown = &handler.doMouseDown;
    rect3.onMouseUp = &handler.doMouseUp;
    rect3.contextMenu = ctxt;
    rect3.pen.width = 8;

    Rectangle rect4 = win.addControl!Rectangle(Rect(250,10,70,70));
    rect4.fill.color = ColorConstant.beige;
    rect4.onMouseMove = &handler.doMouseMove;
    rect4.wantMouse = true;
    rect4.onMouseDown = &handler.doMouseDown;
    rect4.onMouseUp = &handler.doMouseUp;
    rect4.contextMenu = ctxt;
    rect4.pen.width = 5;

    Circle circ1 = win.addControl!Circle(Rect(10,100,70,70));
    circ1.kind = EllipseKind.scaled;
    circ1.radius = 35;
    circ1.fill.color = ColorConstant.lightgreen;
    circ1.onMouseMove = &handler.doMouseMove;
    circ1.wantMouse = true;
    circ1.onMouseDown = &handler.doMouseDown;
    circ1.onMouseUp = &handler.doMouseUp;
    circ1.pen.width = 18;

    Ellipse ell1 = win.addControl!Ellipse(Rect(90,100,70,70));
    ell1.radiusX = 12;
    ell1.radiusY = 33;
    ell1.kind = EllipseKind.strict;
    ell1.fill.color = ColorConstant.lightgreen;
    ell1.onMouseMove = &handler.doMouseMove;
    ell1.wantMouse = true;
    ell1.onMouseDown = &handler.doMouseDown;
    ell1.onMouseUp = &handler.doMouseUp;
    ell1.onClick = &handler.doClick;
    ell1.onDoubleClick = &handler.doDoubleClick;
    ell1.focusable = true;
    ell1.pen.width = 5;

    Pie pie1 = win.addControl!Pie(Rect(150,100,70,70));
    pie1.radius = 500;
    pie1.angleBegin = 0;
    pie1.angleEnd = 270;
    pie1.kind = EllipseKind.scaled;
    pie1.fill.color = ColorConstant.lightgreen;
    pie1.onMouseMove = &handler.doMouseMove;
    pie1.wantMouse = true;
    pie1.onMouseDown = &handler.doMouseDown;
    pie1.onMouseUp = &handler.doMouseUp;
    pie1.pen.width = 4;

    TextEn text1 = win.addControl!TextEn(Rect(10, 180, 200, 32));
    text1.font.name = "FreeMono";
    text1.font.size = 32;
    text1.font.style = FontStyles(FontStyle.bold, FontStyle.italic);
    text1.caption = "Reset";
    text1.fill.color = ColorConstant.lemonchiffon;
    text1.stroke.color = ColorConstant.blueviolet;
    text1.onMouseMove = &handler.doMouseMove;
    text1.wantMouse = true;
    text1.onMouseDown = &handler.doMouseDown;
    text1.onMouseUp = &handler.doMouseUp;
    text1.onClick = &handler.doClick;
    text1.onDoubleClick = &handler.doDoubleClick;
    text1.focusable = true;
    text1.pen.width = 1;

    publisherToFile(text1, "kprim.txt");
    publisherToFile(ctxt, "kmenu.txt");

    kheopsRun;
}

