#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g

/+dub.sdl:
name "actions_window"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module actions_window;

import
    core.thread, std.stdio;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.actions, kheops.helpers.keys,
    kheops.primitives, kheops.colors;

OsWindow win;
Text text;

struct Handler
{
    int c;

    void doAction(string n)(Object) @system
    {
        text.caption = __PRETTY_FUNCTION__;
        text.needRepaint = true;
        text.repaint;
        writeln(__PRETTY_FUNCTION__); stdout.flush;
    }

    void doMax(Object notifier)
    {
        writeln(__PRETTY_FUNCTION__); stdout.flush;
        win.windowState(WindowState.max);
    }

    void doRestore(Object notifier)
    {
        writeln(__PRETTY_FUNCTION__); stdout.flush;
        win.windowState(WindowState.normal);
    }

    void doMinimize(Object notifier)
    {
        writeln(__PRETTY_FUNCTION__); stdout.flush;
        win.windowState(WindowState.minimized);
    }

    void doQuit(Object) @system
    {
        writeln(__PRETTY_FUNCTION__); stdout.flush;
        win.close;
    }

    void doCloseQuery(Object notifier, WindowCloseKind, ref bool accept)
    {
        writeln(__PRETTY_FUNCTION__); stdout.flush;
        accept = c++ > 0;
        if (!accept)
            (cast(OsWindow) notifier).caption = "next time mayyyybe ?";
    }

    void doPaste(Object)
    {
        win.wantPaste;
        writeln(__PRETTY_FUNCTION__); stdout.flush;
    }
}

void main()
{
    win = construct!OsWindow;

    win.beginRealign;
    win.left   = 100;
    win.top    = 100;
    win.width  = 400;
    win.height = 250;

    //auto r = win.addControl!Rectangle(Rect!double(0,0,1,1), Alignment.client);
    text = win.addControl!Text(Rect(0,0,1,1), Alignment.client);
    text.font.size = 12;
    text.justify = Justify.left;
    text.fill.color = ColorConstant.black;
    text.stroke.color = ColorConstant.black;
    text.pen.width = 1;
    text.visible = true;
    win.endRealign;

    win.caption = "actions: CTRL+W | CTRL+X | CTRL+ALT+C | SHIFT+Q(uit)";

    Handler h;
    Action a;

    win.onCloseQuery = &h.doCloseQuery;

    a = win.actionList.addItem;
    a.caption = "action 1";
    a.shortcut =  Shortcut('W', KeyModifiers(KeyModifier.ctrl));
    a.onExecute = &h.doAction!"action 1";
    assert(a.shortcut.saveToText == "Ctrl+W");

    a = win.actionList.addItem;
    a.caption = "action 2";
    a.shortcut =  Shortcut('x', KeyModifiers(KeyModifier.ctrl));
    a.onExecute = &h.doAction!"action 2";

    a = win.actionList.addItem;
    a.caption = "action 3";
    a.shortcut =  Shortcut('c', KeyModifiers(KeyModifier.ctrl, KeyModifier.alt));
    a.onExecute = &h.doAction!"action 3";

    a = win.actionList.addItem;
    a.caption = "quit";
    a.shortcut =  Shortcut('q', KeyModifiers(KeyModifier.shift));
    a.onExecute = &h.doQuit;

    a = win.actionList.addItem;
    a.caption = "maximize";
    a.shortcut =  Shortcut('m', KeyModifiers(KeyModifier.ctrl));
    a.onExecute = &h.doMax;

    a = win.actionList.addItem;
    a.caption = "restore";
    a.shortcut =  Shortcut('r', KeyModifiers(KeyModifier.ctrl));
    a.onExecute = &h.doRestore;

    a = win.actionList.addItem;
    a.caption = "minimize";
    a.shortcut =  Shortcut('s', KeyModifiers(KeyModifier.ctrl));
    a.onExecute = &h.doMinimize;

    a = win.actionList.addItem;
    a.caption = "paste";
    a.shortcut =  Shortcut('v', KeyModifiers(KeyModifier.ctrl));
    a.onExecute = &h.doPaste;

    auto r1 = &h.doAction!"action 1";
    auto r2 = &h.doAction!"action 2";
    auto r3 = &h.doAction!"action 3";
    auto r4 = &h.doQuit;
    auto r5 = &h.doMax;
    auto r6 = &h.doRestore;
    auto r7 = &h.doMinimize;
    auto r8 = &h.doPaste;

    ReferenceMan.storeReference(&r1, "handler1");
    ReferenceMan.storeReference(&r2, "handler2");
    ReferenceMan.storeReference(&r3, "handler3");
    ReferenceMan.storeReference(&r4, "handleQuit");
    ReferenceMan.storeReference(&r5, "handleMaximize");
    ReferenceMan.storeReference(&r6, "handleRestore");
    ReferenceMan.storeReference(&r7, "handleMinimize");
    ReferenceMan.storeReference(&r8, "handlePaste");

    publisherToFile(win, "win.txt");
    win.actionList.clear;
    fileToPublisher("win.txt", win);
    assert(win.actionList.count == 8, "serialization broken ?");

    kheopsRun;
}
