#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "scroll_layout"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module scroll_layout;

import
    core.thread, std.stdio, std.path;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas, kheops.layouts,
    kheops.colors, kheops.primitives;


OsWindow win;
Handler handler;
ScrollableLayout sl;

struct Handler
{
    void handleWheel(Object notifier, byte delta, const ref KeyModifiers km)
    {
        if (axis == Axis.vertical)
            sl.scrollY = sl.scrollY - delta * 5;
        else
            sl.scrollX = sl.scrollX - delta * 5;
        sl.repaint;
    }

    void handleRealigned(Object notifier)
    {
        if (axis == Axis.vertical)
        {
            sl.contentWidth = sl.width();
            sl.contentHeight = 1600;
        }
        else
        {
            sl.contentWidth = 1600;
            sl.contentHeight = sl.height();
        }
    }

    void populateScrollbox(Axis ax)
    {
        axis = ax;
        sl.content.deleteChildren;
        const size_t count = 1600 / 40;
        auto r = gradientRange(count, [0x80,0x0],[0xFF,0x20],[0xFF,0x80]);
        for (auto i = 0; !r.empty; i++, r.popFront())
        {
            RectangleEn a;
            final switch (axis)
            {
                case Axis.vertical: a = sl.addControl!RectangleEn(Rect(0,40*i,400,40), Alignment.top); break;
                case Axis.horizontal: a = sl.addControl!RectangleEn(Rect(40*i,0,40,400), Alignment.left); break;
            }
            a.fill.color = r.front();
            a.pen.width = 3;
            a.radius = 6;
            a.wantKeys = false;
            a.wantMouse = false;
        }
        handleRealigned(null);
        sl.scrollX = 0;
        sl.scrollY = 0;
        sl.repaint;
    }

    Axis axis = Axis.vertical;
    void handleClick(Object)
    {
        axis == Axis.vertical
            ? populateScrollbox(Axis.horizontal)
            : populateScrollbox(Axis.vertical);
    }

    void handleKeyDown(Object notifier, dchar key, const ref KeyModifiers km)
    {
        switch(key)
        {
        case VirtualKey.VK_UP:
            sl.scrollY = sl.scrollY - 5;
            break;
        case VirtualKey.VK_DOWN:
            sl.scrollY = sl.scrollY + 5;
            break;
        case VirtualKey.VK_LEFT:
            sl.scrollX = sl.scrollX - 5;
            break;
        case VirtualKey.VK_RIGHT:
            sl.scrollX = sl.scrollX + 5;
            break;
        default:
            break;
        }
        sl.repaint;
    }
}

void main(string[] args)
{
    win = construct!OsWindow;
    win.beginRealign;
    win.left   = 250;
    win.top    = 250;
    win.width  = 400;
    win.height = 400;
    win.onKeyDown = &handler.handleKeyDown;
    win.onMouseWheel = &handler.handleWheel;
    win.onClick = &handler.handleClick;
    win.onRealigned = &handler.handleRealigned;

    win.caption = "scroll layout: ARROWS | WHELL | LEFT CLICK";

    sl = win.addControl!ScrollableLayout(Rect(0,0,1,1), Alignment.client);
    sl.wantKeys = false;
    sl.wantMouse = false;
    win.endRealign;

    handler.populateScrollbox(Axis.vertical);

    kheopsRun;
}

