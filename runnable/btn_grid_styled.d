#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype

/+dub.sdl:
name "btn_grid_styled"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module btn_grid_styled;

import
    core.thread, std.stdio;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.pen, kheops.primitives, kheops.layouts;


OsWindow win1;


class Model: Control
{
    @SetGet Text _text;
    @SetGet Rectangle _background;
    @SetGet RectangleEn _foreground;

    mixin PropertyPublisherImpl;

    this()
    {
        _background = addControl!Rectangle(Rect(0,0,1,1), Alignment.client);
        _foreground = addControl!RectangleEn(Rect(0,0,1,1), Alignment.client);
        _text = addControl!Text(Rect(0,0,1,1), Alignment.client);

        _foreground.marginAll = 3;

        _background.fill.color = ColorConstant.beige;
        _foreground.fill.fillKind = FillKind.gradient;
        _foreground.fill.gradientKind = GradientKind.vertical;
        _foreground.radius = 5;

        _text.wantMouse = true;
        _text.font.size = 12;
        _text.font.style = [FontStyle.bold];
        _text.font.name = "DejaVuSansMono";
        _text.justify = Justify.center;
        _text.fill.color = ColorConstant.black;
        _text.stroke.color = ColorConstant.black;
        _text.pen.width = 1;

        collectPublications!Model;
    }
}


class Btn: Control
{

    mixin PropertyPublisherImpl;

private:

    @PropHints(PropHint.dontGet) @SetGet
    Text _text;
    @PropHints(PropHint.dontGet) @SetGet
    Rectangle _background;
    @PropHints(PropHint.dontGet) @SetGet
    RectangleEn _foreground;
    bool _down;
    bool _over;

    enum _stateStr = [["up","down"],[">> up <<",">> down <<"]];

    double[][2] _gradPos = [
        [0, 0.2, 0.8 ,1],
        [0, 0.4, 0.6, 1],
    ];
    uint[][3][2] _gradCols = [[
        [0xAA505050, 0xAA808080, 0xAA808080 ,0xAA505050],
        [0xAA505050, 0xAA208050, 0xAA208050 ,0xAA505050],
        [0xAA505050, 0xAA505050, 0xAA505050 ,0xAA505050],
    ],[
        [0xAA505050, 0xAA208050, 0xAA208050 ,0xAA505050],
        [0xAA505010, 0xAA208050, 0xAA208050 ,0xAA505010],
        [0xAA505010, 0xAA208050, 0xAA208050 ,0xAA505010],
    ]];

    void updateState()
    {
        _foreground.fill.gradient.colors = _gradCols[focused][_down + _over];
        _foreground.fill.gradient.positions = _gradPos[_down];
        caption = _stateStr[focused][_down];
        repaint;
    }

    void mouseDown(Object not, double x, double y, const ref MouseButtons mb, const ref KeyModifiers km)
    {
        if (mb == [MouseButton.left])
        {
            _down = true;
            updateState;
        }
    }

    void mouseUp(Object not, double x, double y, const ref MouseButtons mb, const ref KeyModifiers km)
    {
        _down = false;
        updateState;
    }

    void mouseLeave(Object not)
    {
        _over = false;
        _down = _leftMbDown;
        updateState;
    }

    void mouseEnter(Object not)
    {
        _over = true;
        updateState;
    }

protected:

    override void unsetFocus()
    {
        updateState;
    }

    override void setFocus()
    {
        updateState;
    }

public:

    this()
    {
        collectPublications!Btn;
        wantKeys = true;
    }

    override void procAfterStyling()
    {
        _text = cast(Text) cast(Object) findPublisher(this, "text");
        _foreground = cast(RectangleEn) cast(Object) findPublisher(this, "foreground");
        _background = cast(Rectangle) cast(Object) findPublisher(this, "background");

        if (_text)
        {
           _text.onMouseDown = &mouseDown;
           _text.onMouseUp = &mouseUp;
           _text.onMouseLeave = &mouseLeave;
           _text.onMouseEnter = &mouseEnter;
           updateState;
        }
    }

    @Set override void caption(string value)
    {
        super.caption(value);
        if (_text)
            _text.caption = value;
    }
}

void main()
{
    win1 = construct!OsWindow;

    win1.beginRealign;
    win1.left   = 20;
    win1.top    = 20;
    win1.width  = 900;
    win1.height = 600;

    win1.caption = "draft of the styles using iz.serializer";

    Model m = construct!Model;
    Serializer ser = construct!Serializer;
    MemoryStream str = construct!MemoryStream;
    ser.publisherToStream(m, str);
    destructEach(m, ser);
    scope(exit) destruct(str);

    str.saveToFile("style.txt");

    AxisLayout al0 = win1.addControl!(AxisLayout)(Rect(0,0,1,1), Alignment.client);
    foreach(i; 0 .. 2)
    {
        AxisLayout col = al0.addControl!(AxisLayout)(Rect(0,0,1,1));
        col.axis = Axis.vertical;
        foreach(j; 0 .. 2)
        {
            Control c = col.addControl!Btn(Rect(0,0,1,1));
            c.tabOrder = 2 - j;
            str.position = 0;
            c.applyStyle(str);
        }
    }

    win1.endRealign;
    kheopsRun;
}
