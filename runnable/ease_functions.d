#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "ease_functions"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module ease_functions;

import
    std.stdio;
import
    iz.memory, iz.serializer, iz.math;
import
    kheops.types, kheops.primitives, kheops.colors,
    kheops.canvas, kheops.brush, kheops.control, kheops.controls.menus;

alias ease = VariableParabol.ease;

class Segment: Control
{
    mixin inheritedDtor;
    double _slope = 1.2;
    bool _down;

    Brush fill;

    this()
    {
        fill = construct!Brush;
        fill.fillKind = FillKind.gradient;
        fill.gradientKind = GradientKind.vertical;
        fill.gradient.insertStop(0, 0.0, ColorConstant.green);
        fill.gradient.insertStop(1, 0.5, ColorConstant.lightgreen);
        fill.gradient.insertStop(2, 1.0, ColorConstant.gray);
    }

    ~this()
    {
        destruct(fill);
        callInheritedDtor;
    }


    override void procMouseDown(double x, double y, const ref MouseButtons mbs,
        const ref KeyModifiers km)
    {
        if (MouseButton.left !in mbs)
            return;
        _down = true;
        _needRepaint = true;
        repaint;
    }

    override void procMouseUp(double x, double y, const ref MouseButtons mbs,
        const ref KeyModifiers km)
    {
        _down = false;
        _needRepaint = true;
        repaint;
    }

    override void procMouseMove(double x, double y, const ref KeyModifiers km)
    {
        if (!_down)
            return;
        import std.algorithm;
        y = ((1.0 / height) * (height-y)).max(0.0).min(1.0);
        _slope = ease.control(y);
        _needRepaint = true;
        repaint;
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        const double inc = 1.0 / width;
        canvas.beginPath(0,height);
        foreach(immutable i; 0..cast(int)width)
        {
            double x = inc * i;
            double y = height - ease.fx(x, _slope) * height;
            canvas.line(x * width, y);
        }
        canvas.pen.width = 2.0;
        canvas.pen.customDashing = [4,4];
        canvas.pen.dash = Dash.custom;
        canvas.line(width, height);
        canvas.line(0,height);
        canvas.closePath;
        canvas.fillBrush = fill;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.restoreDefaultFillBrush;

        static if (ease.numControls == 1)
        {
            canvas.circle(width * 0.5, height - ease.controlFx(_slope) * height, 10);
            canvas.fillBrush.color = ColorConstant.black;
            canvas.fill!false;
            canvas.stroke!true;
        }
    }
}

void main(string[] args)
{
    OsWindow win = construct!OsWindow;
    win.beginRealign;
    win.caption = "Plot IZ easing functions";
    Segment s = win.addControl!Segment(Rect(0,0,7,7), Alignment.client);
    win.endRealign;
    kheopsRun;
}

