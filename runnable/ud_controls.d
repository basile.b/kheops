#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "ud_controls"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/
module ud_controls;

import
    std.stdio;
import
    iz.memory;
import
    kheops.types, kheops.bitmap, kheops.brush, kheops.canvas, kheops.colors,
    kheops.font, kheops.gradient, kheops.pen, kheops.control, kheops.layouts,
    kheops.udcontrols, kheops.helpers.keys, kheops.actions;

OsWindow win;
UDEditField ef1;
SimpleDrawableVisitor sdv;

struct Handler
{
    void handleKeyDown(Object, dchar key, const ref KeyModifiers)
    {
        if (key == VirtualKey.VK_UP)
        {
            sdv.elementHeight = sdv.elementHeight + 0.1;
        }
        else if (key == VirtualKey.VK_DOWN)
        {
            sdv.elementHeight = sdv.elementHeight - 0.1;
        }
    }

    void handleDesignChanged(Object, bool value)
    {
        win.design(value);
    }

    void handleSetMasked(Object, bool value)
    {
        ef1.masked = value;
    }

    enum sep = " - ";

    import std.traits: Parameters;
    import std.meta: aliasSeqOf;
    import std.range: iota;
    void logCall(A, string fun)(Parameters!A a)
    {
        write(fun);
        write(sep);
        enum len = Parameters!A.length;
        foreach(i; aliasSeqOf!(iota(0, len)))
        {
            static if (is(typeof(a[i]) == dchar))
            {
                import std.uni, std.ascii;
                if (!std.ascii.isWhite(a[i]))
                    write(a[i]);
                else
                    write('#', int(a[i]));
            }
            else write(a[i]);
            if (i != len-1)
                write(sep);
        }
        write("\n");
        stdout.flush;
    }
}

void main(string[] args)
{
    Handler h;
    win = construct!OsWindow;
    win.onKeyDown = &h.handleKeyDown;
    win.beginRealign;

    sdv = construct!SimpleDrawableVisitor(win);
    UDCheckBox ck1 = win.addControl!UDCheckBox(Rect(10,10,150,26), Alignment.none);
    ck1.caption = "!! design-time !!";
    ck1.uniformDrawer = sdv;
    ck1.onCheckedChanged = &h.handleDesignChanged;

    UDCheckBox ck2 = win.addControl!UDCheckBox(Rect(10,40,150,26), Alignment.none);
    ck2.caption = "checkbox 2";

    UDCheckBox ck3 = win.addControl!UDCheckBox(Rect(10,70,150,26), Alignment.none);
    ck3.caption = "checkbox 3";

    UDPathCheckBox ck4 = win.addControl!UDPathCheckBox(Rect(10,100,150,26), Alignment.none);
    ck4.caption = "path checkbox 1";

    UDPathCheckBox ck5 = win.addControl!UDPathCheckBox(Rect(10,130,150,26), Alignment.none);
    ck5.caption = "path checkbox 2";

    UDButton bt1 = win.addControl!UDButton(Rect(10,160,150,26), Alignment.none);
    bt1.caption = "button 1";

    UDButton bt2 = win.addControl!UDButton(Rect(10,190,150,26), Alignment.none);
    bt2.hold = true;
    bt2.caption = "button 2";

    UDGroupBox grp1 = win.addControl!(UDGroupBox)(Rect(200,10,200,100), Alignment.none);
    grp1.clipChildren = true;
    grp1.caption = "group box 1";
    UDCheckBox grp1ck1 = grp1.addControl!UDCheckBox(Rect(10,10,150,26), Alignment.none);
    grp1ck1.caption = "checkbox 1";
    UDCheckBox grp1ck2 = grp1.addControl!UDCheckBox(Rect(10,40,150,26), Alignment.none);
    grp1ck2.caption = "checkbox 2";

    ef1 = win.addControl!(UDEditField)(Rect(200,120,200,32), Alignment.none);
    ef1.editText = "some text...";
    UDCheckBox secCheck = win.addControl!UDCheckBox(Rect(200,160,200,32), Alignment.none);
    secCheck.caption = "masked with asterisks";
    secCheck.onCheckedChanged = &h.handleSetMasked;

    UDGroupBox grp2 = win.addControl!(UDGroupBox)(Rect(200,200,200,200));
    grp2.caption = "host a non-UD layout";
    grp2.clipChildren = true;
    AxisLayout al = grp2.addClientControl!(AxisLayout)();
    al.axis = Axis.vertical;
    foreach (i; 0..6)
    {
        auto c = al.addClientControl!UDCheckBox();
        c.caption = "themed anyway...";
    }

    ef1.onKeyDown = &h.logCall!(typeof(win.onKeyDown()),"onKeyDown");
    win.endRealign;
    win.enterEventLoop;

    destruct(sdv);
}
