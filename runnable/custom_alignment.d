#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "custom_alignment"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module custom_alignment;

import
    core.thread, std.stdio;
import
    iz.memory;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.pen, kheops.primitives, kheops.layouts;


OsWindow win1;

class TestCtrl: Control
{
    mixin inheritedDtor;

    this()
    {
        with (ColorProperty)
            rndCol = randomizedColor(ColorProperties(cold,dark));
    }

    uint rndCol;
    override void paint(ref Canvas canvas)
    {
        canvas.pen.width = 2.0f;
        canvas.fillBrush.fillKind = FillKind.uniform;
        canvas.fillBrush.color = rndCol;
        canvas.rectangle(0, 0, width, height);
        canvas.fill;
        canvas.stroke!true;
    }

    override void procMouseDown(double x, double y, const ref MouseButtons mb, const ref KeyModifiers km)
    {
        AxisLayout al = cast(AxisLayout) parent;

        if (km == [KeyModifier.ctrl])
        {
            al.addControl!TestCtrl(Rect(0,0,100,100), Alignment.none);
            al.repaint;
            return;
        }
        else if (km == [KeyModifier.shift])
        {
            if (al.removeChild(this)) {}
            al.repaint;
            return;
        }
        else
        {
            // input dispatcher bug was
            // - dispatched to a control
            // - axis changes
            // - dispatching continues to another control on the new axis
            // - axis changes
            // => like if nothing happens
            if (al.axis == Axis.horizontal)
                al.axis = Axis.vertical;
            else
                al.axis = Axis.horizontal;
            repaint;
        }
    }
}


void main(string[] args)
{
    win1 = construct!OsWindow;
    win1.beginRealign;
    win1.left   = 250;
    win1.top    = 250;
    win1.width  = 600;
    win1.height = 400;

    win1.caption = "axis layout: CLICK | CTRL+CLICK | SHIFT+CLICK";

    AxisLayout l0 = win1.addControl!AxisLayout(Rect(0,0,100,100), Alignment.client);
    l0.spacingLeft = 6;
    l0.spacingRight = 6;
    l0.spacingTop = 6;
    l0.spacingBottom = 6;
    l0.axis = Axis.vertical;
    TestCtrl c0 = l0.addControl!TestCtrl(Rect(0,0,100,100));
    TestCtrl c1 = l0.addControl!TestCtrl(Rect(0,0,100,100));
    TestCtrl c2 = l0.addControl!TestCtrl(Rect(0,0,100,100));
    TestCtrl c3 = l0.addControl!TestCtrl(Rect(0,0,100,100));
    win1.endRealign;

    kheopsRun;
}
