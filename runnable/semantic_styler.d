#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "split_layout"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module semantic_styler;

import
    core.thread, std.stdio, std.path;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas, kheops.semantic_styler,
    kheops.colors, kheops.primitives, kheops.layouts, kheops.controls;

OsWindow win;
SplitLayout spl;

void main(string[] args)
{
    win = construct!OsWindow;
    win.beginRealign;

    win.left   = 250;
    win.top    = 250;
    win.width  = 600;
    win.height = 400;
    win.design = true;

    SemanticStyler ss = construct!SemanticStyler;
    scope(exit) destruct(ss);

    Label lbl = win.addControl!Label(Rect(20,20, 256, 30));
    lbl.caption = "Some text";

    GroupBox grp = win.addControl!GroupBox(Rect(20,70, 300, 300));
    grp.caption = "Group box 1";

    Edit edt = win.addControl!Edit(Rect(300,20,200,30));

    ss.background.fill.color = ColorConstant.bisque;
    ss.background.stroke.color = ColorConstant.red;

    ss.backgroundFrame.fill.fillKind = FillKind.none;
    ss.backgroundFrame.stroke.color = ColorConstant.brown;
    ss.backgroundFrame.cornerAll = CornerKind.round;
    ss.backgroundFrame.radius = 8;

    ss.text.fill.color = ColorConstant.red;
    ss.text.stroke.color = ColorConstant.red;
    ss.text.font.size = 22;
    ss.text.font.name = "OpenSans-Italic";

    ss.applyStyleFrom(win);

    win.caption = "Apply semantic styler models to controls";

    win.endRealign;
    kheopsRun;
}
