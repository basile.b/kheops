#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "default_alignment"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module default_alignment;

import
    core.thread, std.stdio, std.path;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.primitives;

OsWindow win1;

struct TestGrad
{
    void handlePaint(Object sender, ref const(Rect) rect, ref Canvas canvas)
    {
        auto _store = canvas.fillBrush.fillKind;
        canvas.fillBrush.beginChange;
        if (canvas.fillBrush.gradient.stopCount == 0)
        {
            canvas.fillBrush.gradient.insertStop(0, 0.0, ColorConstant.blue);
            canvas.fillBrush.gradient.insertStop(1, 0.5, ColorConstant.green);
            canvas.fillBrush.gradient.insertStop(2, 1.0, ColorConstant.red);
        }

        canvas.fillBrush.gradientKind = GradientKind.radial;
        canvas.fillBrush.fillKind = FillKind.gradient;
        canvas.fillBrush.fillAdjustment = FillAdjustment.pad;
        canvas.fillBrush.endChange;

        canvas.rectangle(1, 1, rect.width - 1, rect.height - 1);

        canvas.fill!false;
        canvas.stroke!true;

        /*
        canvas.fillBrush.fillKind = FillKind.uniform;
        canvas.fillBrush.color = ColorConstant.black;
        canvas.font.getStyles = [FontStyle.italic, FontStyle.bold];
        canvas.font.name = "Sans";
        canvas.font.size = 20;
        canvas.drawText("--- C A I R O ---", 5, 25);
        canvas.stroke!true;
        */

        canvas.fillBrush.fillKind = _store;
    }
}

class TestRectangle : RectangleEn
{
    this()
    {
        super();
        cornerAll = CornerKind.round;
        pen.width = 8;
        radius = 25;
        fill.color = randomizedColor(ColorProperties([ColorProperty.cold, ColorProperty.clear]));
    }
}

void main(string[] args)
{
    win1 = construct!OsWindow;
    win1.beginRealign;
    win1.left   = 250;
    win1.top    = 250;
    win1.width  = 600;
    win1.height = 400;

    win1.caption = "nested alignment top/left/right/bottom/client with scaling";

    TestRectangle c0 = win1.addControl!TestRectangle(Rect(0,0,100,100), Alignment.left);
    TestRectangle c1 = win1.addControl!TestRectangle(Rect(0,0,100,100), Alignment.right);
    TestRectangle c2 = win1.addControl!TestRectangle(Rect(0,0,100,100), Alignment.top);
    TestRectangle c3 = win1.addControl!TestRectangle(Rect(0,0,100,100), Alignment.bottom);
    TestRectangle c4 = win1.addControl!TestRectangle(Rect(0,0,100,100), Alignment.client);

    c0.scaled = true;
    c1.scaled = true;
    c2.scaled = true;
    c3.scaled = true;

    c0.paddingAll(3);
    c1.paddingAll(3);
    c2.paddingAll(3);
    c3.paddingAll(3);

    auto c5 = c4.addControl!TestRectangle(Rect(0,0,100,100), Alignment.client);
    c5.marginAll(1);
    c5.paddingAll(3);
    c5.clipChildren = true;

    TestRectangle c52 = c5.addControl!TestRectangle(Rect(0,0,20,20), Alignment.top);
    TestRectangle c53 = c5.addControl!TestRectangle(Rect(0,0,20,20), Alignment.bottom);
    TestRectangle c50 = c5.addControl!TestRectangle(Rect(0,0,10,20), Alignment.left);
    TestRectangle c51 = c5.addControl!TestRectangle(Rect(0,0,10,20), Alignment.right);
    TestRectangle c54 = c5.addControl!TestRectangle(Rect(0,0,20,20), Alignment.client);
    c54.clipChildren = true;

    c50.scaled = true;
    c51.scaled = true;
    c52.scaled = true;
    c53.scaled = true;


    TestRectangle r = c54.addControl!TestRectangle(Rect(0,0,200,200), Alignment.client);
    r.clipChildren = true;
    r.marginAll(40);
    r.paddingAll(3);
    r.radius = 20;

    r.cornerTopLeft     = CornerKind.round;
    r.cornerTopRight    = CornerKind.round;
    r.cornerBottomRight = CornerKind.round;
    r.cornerBottomLeft  = CornerKind.round;

    r.fill.fillKind = FillKind.bitmap;
    r.fill.fillAdjustment = FillAdjustment.reflect;
    r.fill.bitmap.loadFromPng(__FILE__.dirName ~ "/../media/acorn.png");

    import kheops.helpers.polygons;

    TextEn t0 = r.addControl!TextEn(Rect(0,0,400,400), Alignment.client);
    t0.font.style = [FontStyle.bold, FontStyle.italic];
    t0.font.size = 40;
    t0.caption = "nuts box";
    t0.justify = Justify.center;
    t0.pen.width = 2;
    t0.pen.dash = Dash.dash2;
    t0.fill.color = ColorConstant.yellow;

    win1.endRealign;
    //publisherToFile(c55, "kheops.txt");


    kheopsRun;
}
